<!--==========================================================================-->
<!--  LHCb Detector Description                                                 -->
<!--==========================================================================-->
<!--                                                                            -->
<!--  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)  -->
<!--  All rights reserved.                                                      -->
<!--                                                                            -->
<!--   @author  M.Frank                                                         -->
<!--   @date    14/10/2018                                                      -->
<!--                                                                            -->
<!--==========================================================================-->
<!-- XML description of the UT stations                                        -->
<!--==========================================================================-->

<lccdd>

  <detectors>
    <detector id="FT:ID" name="FT" type="LHCb_FT_geo" parent="${FT:parent}" readout="FTHits" vis="FT:Envelope">

      <!-- FT module geometry   -->
      <volume name="lvEndplug" material="FT:PerforatedAluminium" vis="FT:Core1">
        <shape type="Box" name="EndplugBox"
               dx="0.5*FT:EndplugSizeX"     dy="0.5*FT:EndplugSizeY"     dz="0.5*FT:EndplugSizeZ"/>
      </volume>
      <volume name="lvEndpiece" material="FT:Polycarbonate"  vis="FT:Core2">
        <shape type="Box" name="EndpieceBox"
               dx="0.5*FT:EndpieceSizeX"    dy="0.5*FT:EndpieceSizeY"    dz="0.5*FT:EndpieceSizeZ"/>
      </volume>
      <volume name="lvEndpiece1" material="FT:Polycarbonate"  vis="FT:Core2">
        <shape type="Box" name="EndpieceBox1"
               dx="0.5*FT:HoleSizeX"        dy="0.5*FT:EndpieceSizeY"    dz="0.5*FT:EndpieceSizeZ"/>
      </volume>
      <volume name="lvEndpiece3" material="FT:Polycarbonate"  vis="FT:Core2">
        <shape type="Box" name="EndpieceBox3"
               dx="0.5*(FT:EndpieceSizeX-FT:HoleSizeX)" dy="0.5*FT:EndpieceSizeY" dz="0.5*FT:EndpieceSizeZ"/>
      </volume>
      <volume name="lvFibreMat" material="FT:SciFibre" sensitive="1"  vis="FT:Fibre">
        <shape type="Box" name="FibreBox"
               dx="0.5*FT:MatSizeX"         dy="0.5*FT:MatSizeY"         dz="0.5*FT:MatSizeZ"/>
      </volume>
      <volume name="lvSkin" material="FT:Skin"  vis="FT:Support">
        <shape type="Box" name="SkinBox"
               dx="FT:SkinSizeX/2"          dy="FT:SkinSizeY/2"          dz="FT:SkinSizeZ/2"/>
      </volume>
      <volume name="lvSideWall" material="FT:Skin"  vis="FT:Support">
        <shape type="Box" name = "SideWallBox"
               dx="FT:WallThickness/2"      dy="FT:SkinSizeY/2"          dz="FT:ModuleSizeZ/2" />
      </volume>
      <volume name="lvCore" material="FT:Honeycomb" vis="FT:Core">
        <shape type="Box" name="CoreBox"
               dx="0.5*FT:CoreSizeX"        dy="0.5*FT:CoreSizeY"        dz="0.5*FT:CoreSizeZ"/>
        <physvol name="pvEndplug"      logvol="lvEndplug">
          <position  y="0.5*(FT:CoreSizeY-FT:EndplugSizeY)"/>
        </physvol>
        <physvol name="pvEndpiece"     logvol="lvEndpiece">
          <position  y="-0.5*(FT:CoreSizeY-FT:EndpieceSizeY)+FT:DeadSizeY"
                     z="0.5*(FT:CoreSizeZ-FT:EndpieceSizeZ)"/>
        </physvol>
      </volume>

      <!-- FT CFrame geometry   -->
      <volume name="lvCFramePairIBar" material="Iron" vis="FT:CFrame">
	<shape type="Box" name="CFramePairIBar"
               dx="0.5*FT:CFrameIBarX"      dy="0.5*FT:CFrameIBarY"          dz="0.5*FT:CFrameIBarZ"/>
      </volume>
      <volume name="lvCFramePairCarrierTop" material="Iron" vis="FT:CFrame">
	<shape type="Box" name="CFramePairCarrierTop"
               dx="FT:CFrameCarrierTopX"    dy="0.5*FT:CFrameCarrierTopY"    dz="0.5*FT:CFrameCarrierTopZ"/>
      </volume>
      <volume name="lvCFramePairCarrierBottom" material="Iron" vis="FT:CFrame">
	<shape type="Box" name="CFramePairCarrierBottom"
               dx="FT:CFrameCarrierBottomX" dy="0.5*FT:CFrameCarrierBottomY" dz="0.5*FT:CFrameCarrierBottomZ"/>
      </volume>
      <volume name="lvCFramePairModuleMountTop" material="Iron" vis="FT:CFrame">
	<shape type="Box" name="CFramePairModuleMountTop"
               dx="FT:CFrameModuleMountX"     dy="0.5*FT:CFrameModuleMountY" dz="0.5*FT:CFrameModuleMountZTop"/>
      </volume>
      <volume name="lvCFramePairModuleMountBottom" material="Iron" vis="FT:CFrame">
	<shape type="Box" name="CFramePairModuleMountBottom"
               dx="FT:CFrameModuleMountX"   dy="0.5*FT:CFrameModuleMountY"   dz="0.5*FT:CFrameModuleMountZBottom"/>
      </volume>

      <volume name="lvCFramePair" material="Air" vis="FT:Frame">
	<!-- Define box volume for CFrame -->
	<shape type="BooleanShape" operation="subtraction" name="CFramePairVolumeWithHole">
	  <shape type="Box" name="CFrameBox"
		 dx="FT:CFrameSizeX"    dy="0.5*FT:CFrameSizeY"      dz="0.5*FT:CFrameSizeZ+FT:eps"/>
	  <shape type="Tube" name="BeamHole"
		 dz="0.5*FT:CFrameSizeZ+FT:eps" rmin="0.0*mm"     rmax="T:BeamHoleRadius"/>
	  <position y="-0.5*FT:CFrameShiftY"/>
	</shape>

	<physvol name="pvCFramePairIBarLeft" logvol="lvCFramePairIBar">
	  <position x="-(FT:CFrameSizeX-0.5*FT:CFrameIBarX)" y="-0.5*FT:CFrameCarrierTopY"/>
	</physvol>

	<physvol name="pvCFramePairIBarRight" logvol="lvCFramePairIBar">
	  <position x="(FT:CFrameSizeX-0.5*FT:CFrameIBarX)" y="-0.5*FT:CFrameCarrierTopY"/>
	</physvol>

	<physvol name="pvCFramePairCarrierTop" logvol="lvCFramePairCarrierTop">
	  <position y="+0.5*(FT:CFrameSizeY-FT:CFrameCarrierTopY)"/>
	</physvol>
	
	<physvol name="pvCFramePairCarrierBottom" logvol="lvCFramePairCarrierBottom">
	  <position y="-0.5*(FT:CFrameSizeY-FT:CFrameCarrierBottomY)"/>
	</physvol>
	
	<physvol name="pvCFramePairModuleMountTop" logvol="lvCFramePairModuleMountTop">
	  <position y="0.5*FT:CFrameSizeY-FT:CFrameCarrierTopY-0.5*FT:CFrameModuleMountY"/>
	</physvol>
	
	<physvol name="pvCFramePairModuleMountBottom" logvol="lvCFramePairModuleMountBottom">
	  <position y="-0.5*FT:CFrameSizeY+FT:CFrameCarrierBottomY+0.5*FT:CFrameModuleMountY"/>
	</physvol>
      </volume>

      <transformation>
        <position/>
      </transformation>

      <debug>
<!--
        <item name="attach_volume"   value="lvLayer5X1"/>
        <item name="attach_volume"   value="lvLayer5V"/>
        <item name="attach_volume"   value="lvStation5"/>

-->
        <item name="build_passive" value="0"/>
        <item name="build_frames"  value="0"/>
        <item name="debug" value="0"/>
      </debug>

    </detector>
  </detectors>

  <readouts>
    <readout name="FTHits">
      <id>system:8,station:4,layer:5,quarter:5,module:6,mat:3</id>
    </readout>
  </readouts>
</lccdd>
