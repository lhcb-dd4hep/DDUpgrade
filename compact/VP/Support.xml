<!--==========================================================================-->
<!--  LHCb Detector Description                                                 -->
<!--==========================================================================-->
<!--                                                                            -->
<!--  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)  -->
<!--  All rights reserved.                                                      -->
<!--                                                                            -->
<!--   @date    14/10/2018                                                      -->
<!--                                                                            -->
<!--==========================================================================-->
<velo>

  <!-- PlanA Substrate definition -->
  <volume vis="VP:ModuleSupport" name="lvSubstrate" material="Silicon">
    <!-- Consists of parent box with a series of subtractions to get the correct shape-->
    <shape type="BooleanShape" operation="subtraction" name="SubstrateNewSub">
      <shape type="Box" name="SubstrateNewMain"
             dx="0.5*VP:SubstrateNewSizeX"
             dy="0.5*VP:SubstrateNewSizeY"
             dz="0.5*VP:SubstrateThick"/>
      <shape type="Box" name="SubstrateNewTRCornerCut"
             dx="0.5*sqrt(2)*34.3583*mm"
             dy="VP:SubstrateNewSizeY"
             dz="VP:SubstrateThick"/>
      <position x="VP:SubstrateNewSizeX/2"
		y="VP:SubstrateNewSizeY/2"/>
      <rotation z="VP:Rotation"/>
      <shape type="Box" name="SubstrateNewTLCornerCut"
             dx="0.5*sqrt(2)*33.5*mm"
             dy="VP:SubstrateNewSizeY"
             dz="VP:SubstrateThick"/>
      <position x="-VP:SubstrateNewSizeX/2"
		y="VP:SubstrateNewSizeY/2"/>
      <rotation z="-VP:Rotation"/>
      <shape type="Box" name="SubstrateNewBLCornerCut"
             dx="0.5*sqrt(2)*10*mm"
             dy="VP:SubstrateNewSizeY"
             dz="VP:SubstrateThick"/>
      <position x="-VP:SubstrateNewSizeX/2"
		y="-VP:SubstrateNewSizeY/2"/>
      <rotation z="VP:Rotation"/>
      <shape type="Box" name="SubstrateNewBRCornerCut"
             dx="0.5*sqrt(2)*35.4037*mm"
             dy="VP:SubstrateNewSizeY"
             dz="VP:SubstrateThick"/>
      <position x="-VP:SubstrateNewSizeX/2 + 86.8233*mm"
		y="-VP:SubstrateNewSizeY/2"/>
      <rotation z="-VP:Rotation"/>
      <shape type="Box" name="SubstrateNewBottomInnerCut"
             dx="VP:SubstrateNewBottomInnerCutSize"
             dy="VP:SubstrateNewBottomInnerCutSize"
             dz="VP:SubstrateThick"/>
      <position x="VP:SubstrateNewSizeX/2 + VP:SubstrateNewBottomInnerCutSize/sqrt(2) - 24.5455*mm"
		y="VP:SubstrateNewSizeY/2 - 70*mm - (5.005*mm/sqrt(2))"/>
      <rotation z="45*degree"/>
      <shape type="Box" name="SubstrateNewTopInnerCut"
             dx="VP:SubstrateNewTopInnerCutSize"
             dy="VP:SubstrateNewTopInnerCutSize"
             dz="VP:SubstrateThick"/>
      <position x="VP:SubstrateNewSizeX/2 + VP:SubstrateNewTopInnerCutSize/sqrt(2)"
		y="VP:SubstrateNewSizeY/2 - 70*mm + (5.005*mm/sqrt(2))"/>
      <rotation z="45*degree"/>
      <shape type="Tube" name="SubstrateNewInnerCorner"
             dz="VP:SubstrateThick"
             rmax="5.005*mm"
             deltaphi="360*degree"/>
      <position y="-VP:SubstrateNewSizeY/2 + 46.5*mm"
		x="-VP:SubstrateNewSizeX/2 + 82.8052*mm"/>
    </shape>
  </volume>


  <!-- New Hybrid description -->
  <!-- "Front" is the side with the CO2 connector, hence "Back" is the other side -->

  <!-- Kapton and Copper layers -->
  <volume vis="VP:ModuleSupport" name="lvHybridKaptonBack" material="VP:Kapton">
    <shape type="BooleanShape" operation="subtraction" name="KaptonBackSub">
      <shape type="BooleanShape" operation="intersection" name="KaptonBackInter">
        <shape type="Box"    name="KaptonBackInterBox1"
               dx="0.5*VP:NewHybridSizeX"
               dy="0.5*VP:NewHybridSizeY"
               dz="0.5*VP:NewHybridKaptonThick"/>
        <shape type="Box"    name="KaptonBackInterBox2"
               dx="0.5*VP:NewHybridBackInterBox2SizeX"
               dy="0.5*VP:NewHybridBackInterBox2SizeY"
               dz="0.5*VP:NewHybridKaptonThick"/>
        <position x="VP:NewHybridBackInterBox2CentreX"
                  y="VP:NewHybridBackInterBox2CentreY"/>
        <rotation z="-VP:Rotation"/>
      </shape>
      <shape type="Box"    name="KaptonBackSubBox1"
             dx="0.5*VP:NewHybridBackSubBox1XSize"
             dy="0.5*VP:NewHybridBackSubBox1YSize"
             dz="0.5*VP:NewHybridKaptonThick*2"/>
      <position x="-VP:NewHybridSizeX/2+VP:NewHybridTopLeftCornerSize+VP:NewHybridBackTopEdgeSize+VP:NewHybridBackSubBox1XSize/2"
		y="VP:NewHybridSizeY/2"/>
      <shape type="Box"    name="KaptonBackSubBox2"
             dx="0.5*VP:NewHybridBackSubBox2XSize"
             dy="0.5*VP:NewHybridBackSubBox2YSize"
             dz="0.5*VP:NewHybridKaptonThick*2"/>
      <position x="VP:NewHybridSizeX/2"
		y="VP:NewHybridSizeY/2"/>
    </shape>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvHybridKaptonFront" material="VP:Kapton">
    <shape type="BooleanShape" operation="subtraction" name="KaptonFrontSub">
      <shape type="BooleanShape" operation="intersection" name="KaptonFrontInter">
        <shape type="Box"    name="KaptonFrontInterBox1"
               dx="0.5*VP:NewHybridSizeX"
               dy="0.5*VP:NewHybridSizeY"
               dz="0.5*VP:NewHybridKaptonThick"/>
        <shape type="Box"    name="KaptonFrontInterBox2"
               dx="0.5*VP:NewHybridFrontInterBox2SizeX"
               dy="0.5*VP:NewHybridFrontInterBox2SizeY"
               dz="0.5*VP:NewHybridKaptonThick"/>
        <position x="VP:NewHybridFrontInterBox2CentreX"
                  y="VP:NewHybridFrontInterBox2CentreY"/>
        <rotation z="-VP:Rotation"/>
      </shape>
      <shape type="BooleanShape" operation="subtraction" name="KaptonFrontSub2">
        <shape type="Box"    name="KaptonFrontSubBox1"
               dx="0.5*VP:NewHybridFrontSubBox1XSize"
               dy="0.5*VP:NewHybridFrontSubBox1YSize"
               dz="0.5*VP:NewHybridKaptonThick*2"/>
        <shape type="Box"    name="KaptonFrontSubBox2"
               dx="0.5*VP:NewHybridFrontSubBox2XSize"
               dy="0.5*VP:NewHybridFrontSubBox2YSize"
               dz="0.5*VP:NewHybridKaptonThick*4"/>
        <position x="VP:NewHybridFrontSubBox1XSize/2"
                  y="-VP:NewHybridFrontSubBox1YSize/2"/>
        <rotation z="-VP:Rotation"/>
      </shape>
      <position x="-VP:NewHybridSizeX/2+VP:NewHybridTopLeftCornerSize+VP:NewHybridFrontTopEdgeSize+VP:NewHybridFrontSubBox1XSize/2"
		y="VP:NewHybridSizeY/2"/>
      <shape type="Box"    name="KaptonFrontSubBox3"
             dx="0.5*VP:NewHybridFrontSubBox3XSize"
             dy="0.5*VP:NewHybridFrontSubBox3YSize"
             dz="0.5*VP:NewHybridKaptonThick*2"/>
      <position x="VP:NewHybridSizeX/2"
		y="VP:NewHybridSizeY/2"/>
    </shape>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvHybridCopperBack" material="Copper">
    <shape type="BooleanShape" operation="subtraction" name="CopperBackSub">
      <shape type="BooleanShape" operation="intersection" name="CopperBackInter">
        <shape type="Box"    name="CopperBackInterBox1"
               dx="0.5*VP:NewHybridSizeX"
               dy="0.5*VP:NewHybridSizeY"
               dz="0.5*VP:NewHybridCopperThick"/>
        <shape type="Box"    name="CopperBackInterBox2"
               dx="0.5*VP:NewHybridBackInterBox2SizeX"
               dy="0.5*VP:NewHybridBackInterBox2SizeY"
               dz="0.5*VP:NewHybridCopperThick"/>
        <position x="VP:NewHybridBackInterBox2CentreX"
                  y="VP:NewHybridBackInterBox2CentreY"/>
        <rotation z="-VP:Rotation"/>
      </shape>
      <shape type="Box"    name="CopperBackSubBox1"
             dx="0.5*VP:NewHybridBackSubBox1XSize"
             dy="0.5*VP:NewHybridBackSubBox1YSize"
             dz="0.5*VP:NewHybridCopperThick*2"/>
      <position x="-VP:NewHybridSizeX/2+VP:NewHybridTopLeftCornerSize+VP:NewHybridBackTopEdgeSize+VP:NewHybridBackSubBox1XSize/2"
		y="VP:NewHybridSizeY/2"/>
      <shape type="Box"    name="CopperBackSubBox2"
             dx="0.5*VP:NewHybridBackSubBox2XSize"
             dy="0.5*VP:NewHybridBackSubBox2YSize"
             dz="0.5*VP:NewHybridCopperThick*2"/>
      <position x="VP:NewHybridSizeX/2"
		y="VP:NewHybridSizeY/2"/>
    </shape>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvHybridCopperFront" material="Copper">
    <shape type="BooleanShape" operation="subtraction" name="CopperFrontSub">
      <shape type="BooleanShape" operation="intersection" name="CopperFrontInter">
        <shape type="Box"    name="CopperFrontInterBox1"
               dx="0.5*VP:NewHybridSizeX"
               dy="0.5*VP:NewHybridSizeY"
               dz="0.5*VP:NewHybridCopperThick"/>
        <shape type="Box"    name="CopperFrontInterBox2"
               dx="0.5*VP:NewHybridFrontInterBox2SizeX"
               dy="0.5*VP:NewHybridFrontInterBox2SizeY"
               dz="0.5*VP:NewHybridCopperThick"/>
        <position x="VP:NewHybridFrontInterBox2CentreX"
                  y="VP:NewHybridFrontInterBox2CentreY"/>
        <rotation z="-VP:Rotation"/>
      </shape>
      <shape type="BooleanShape" operation="subtraction" name="CopperFrontSub2">
        <shape type="Box"    name="CopperFrontSubBox1"
               dx="0.5*VP:NewHybridFrontSubBox1XSize"
               dy="0.5*VP:NewHybridFrontSubBox1YSize"
               dz="0.5*VP:NewHybridCopperThick*2"/>
        <shape type="Box"    name="CopperFrontSubBox2"
               dx="0.5*VP:NewHybridFrontSubBox2XSize"
               dy="0.5*VP:NewHybridFrontSubBox2YSize"
               dz="0.5*VP:NewHybridCopperThick*4"/>
        <position x="VP:NewHybridFrontSubBox1XSize/2"
                  y="-VP:NewHybridFrontSubBox1YSize/2"/>
        <rotation z="-VP:Rotation"/>
      </shape>
      <position x="-VP:NewHybridSizeX/2+VP:NewHybridTopLeftCornerSize+VP:NewHybridFrontTopEdgeSize+VP:NewHybridFrontSubBox1XSize/2"
		y="VP:NewHybridSizeY/2"/>
      <shape type="Box"    name="CopperFrontSubBox3"
             dx="0.5*VP:NewHybridFrontSubBox3XSize"
             dy="0.5*VP:NewHybridFrontSubBox3YSize"
             dz="0.5*VP:NewHybridCopperThick*2"/>
      <position x="VP:NewHybridSizeX/2"
		y="VP:NewHybridSizeY/2"/>
    </shape>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvHybridKaptonBottomStrip" material="VP:Kapton">
    <shape type="Box" name="KaptonBottomStripBox"
           dx="0.5*VP:NewHybridBottomStripSizeX"
           dy="0.5*VP:NewHybridBottomStripSizeY"
           dz="0.5*VP:NewHybridKaptonThick"/>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvHybridCopperBottomStrip" material="Copper">
    <shape type="Box" name="CopperBottomStripBox"
           dx="0.5*VP:NewHybridBottomStripSizeX"
           dy="0.5*VP:NewHybridBottomStripSizeY"
           dz="0.5*VP:NewHybridCopperThick"/>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvHybridBottomStrip" material="Vacuum">
    <shape type="Box" name="BottomStripBox"
           dx="0.5*VP:NewHybridBottomStripSizeX"
           dy="0.5*VP:NewHybridBottomStripSizeY"
           dz="0.5*VP:NewHybridThick"/>
    <physvol name="pvHybridBottomStripBtmKapton"
             volume="lvHybridKaptonBottomStrip">
      <position z="(VP:NewHybridThick-VP:NewHybridKaptonThick)/2"/>
    </physvol>
    <physvol name="pvHybridBottomStripTopKapton"
             volume="lvHybridKaptonBottomStrip">
      <position z="-(VP:NewHybridThick-VP:NewHybridKaptonThick)/2"/>
    </physvol>
    <physvol name="pvHybridBottomStripCopper"
             volume="lvHybridCopperBottomStrip">
    </physvol>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvHybridKaptonSideLeg" material="VP:Kapton">
    <shape type="Box" name="KaptonSideLegBox"
           dx="0.5*VP:NewHybridSideLegSizeX"
           dy="0.5*VP:NewHybridSideLegSizeY"
           dz="0.5*VP:NewHybridKaptonThick"/>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvHybridCopperSideLeg" material="Copper">
    <shape type="Box" name="CopperSideLegBox"
           dx="0.5*VP:NewHybridSideLegSizeX"
           dy="0.5*VP:NewHybridSideLegSizeY"
           dz="0.5*VP:NewHybridCopperThick"/>
  </volume>

  <volume vis="VP:Assembly" name="lvHybridSideLeg">
    <physvol name="pvSideLegCopper"
             volume="lvHybridCopperSideLeg">
    </physvol>
    <physvol name="pvSideLegBtmKapton"
             volume="lvHybridKaptonSideLeg">
      <position z="(VP:NewHybridCopperThick+VP:NewHybridKaptonThick)/2"/>
    </physvol>
    <physvol name="pvSideLegTopKapton"
             volume="lvHybridKaptonSideLeg">
      <position z="-(VP:NewHybridCopperThick+VP:NewHybridKaptonThick)/2"/>
    </physvol>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvDataConnectorLCP" material="VP:LCP">
    <shape type="BooleanShape" operation="subtraction" name="DataConnectorLCPSub">
      <shape type="Box" name="VP:DataConnectorLCPMain"
             dx="0.5*VP:DataConnectorX"
             dy="0.5*VP:DataConnectorY"
             dz="0.5*VP:DataConnectorThick"/>
      <shape type="Box" name="DataConnectorLCPCut1"
             dx="0.5*VP:DataConnectorContactX*1.02"
             dy="0.5*VP:DataConnectorY*1.1"
             dz="0.5*(2*VP:DataConnectorContactThick + 0.02*VP:DataConnectorContactX)"/>
      <position x="0.4*mm + VP:DataConnectorContactX/2"
		z="VP:DataConnectorThick/2"/>
      <shape type="Box" name="DataConnectorLCPCut2"
             dx="0.5*VP:DataConnectorContactX*1.02"
             dy="0.5*VP:DataConnectorY*1.1"
             dz="0.5*(2*VP:DataConnectorContactThick + 0.02*VP:DataConnectorContactX)"/>
      <position x="-(0.4*mm + VP:DataConnectorContactX/2)"
		z="VP:DataConnectorThick/2"/>
    </shape>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvDataConnectorContact" material="VP:BerylliumCopper">
    <shape type="Box" name="DataConnectorContact"
           dx="0.5*VP:DataConnectorContactX"
           dy="0.5*VP:DataConnectorY"
           dz="0.5*VP:DataConnectorContactThick"/>
  </volume>

  <!-- Data connectors, located on the end of the hybrid legs -->
  <volume vis="VP:Assembly" name="lvDataConnector" material="Vacuum">
    <shape type="Box" name="DataConnector"
           dx="0.5*VP:DataConnectorX"
           dy="0.5*VP:DataConnectorY"
           dz="0.5*VP:DataConnectorThick"/>
    <physvol volume="lvDataConnectorLCP" name="pvDataConnectorMain">
    </physvol>
    <physvol volume="lvDataConnectorContact" name="pvDataConnectorContact_0">
      <position x="0.4*mm + VP:DataConnectorContactX/2"
		z="(VP:DataConnectorThick - VP:DataConnectorContactThick)/2"/>
    </physvol>
    <physvol volume="lvDataConnectorContact" name="pvDataConnectorContact_1">
      <position x="-(0.4*mm + VP:DataConnectorContactX/2)"
		z="(VP:DataConnectorThick - VP:DataConnectorContactThick)/2"/>
    </physvol>
  </volume>

  <!-- GBT -->
  <volume vis="VP:ModuleGBT" name="lvGBT" material="Silicon">
    <shape type="Box" name="GBTBox"
           dx="0.5*VP:GBTSizeX"
           dy="0.5*VP:GBTSizeY"
           dz="0.5*VP:GBTThick"/>
  </volume>

  <!-- The complete hybrid -->
  <volume vis="Red" name="lvHybrid">
    <!-- The substrate volume must be first in the list - it defines the placement wrt IP -->
    <physvol name="pvSubstrate"             volume="lvSubstrate">
      <transformation>
        <position x="VP:SubstrateNewSizeX/2 - VP:SubstrateNewIPXDistFromBL"
                  y="VP:SubstrateNewSizeY/2 - VP:SubstrateNewIPYDistFromBL"/>
        <rotation/>
        <position/>
        <rotation x="180*degree"/>
      </transformation>
    </physvol>

    <!-- Then come the layers of the hybrid -->
    <physvol name="pvBottomKaptonFront"
             volume="lvHybridKaptonFront">
      <transformation>
        <position x="-VP:NewHybridSizeX/2+VP:NewHybridXOverhang"
                  y="-VP:NewHybridSizeY/2+VP:NewHybridYOverhang"
                  z="-(VP:SubstrateThick/2+VP:NewHybridKaptonThick/2)"/>
        <rotation/>
        <position/>
        <rotation z="-VP:Rotation"/>
      </transformation>
    </physvol>
    <physvol name="pvCopperFront"
             volume="lvHybridCopperFront">
      <transformation>
        <position x="-VP:NewHybridSizeX/2+VP:NewHybridXOverhang"
                  y="-VP:NewHybridSizeY/2+VP:NewHybridYOverhang"
                  z="-(VP:SubstrateThick/2+VP:NewHybridKaptonThick+VP:NewHybridCopperThick/2)"/>
        <rotation/>
        <position/>
        <rotation z="-VP:Rotation"/>
      </transformation>
    </physvol>
    <physvol name="pvTopKaptonFront"
             volume="lvHybridKaptonFront">
      <transformation>
        <position x="-VP:NewHybridSizeX/2+VP:NewHybridXOverhang"
                  y="-VP:NewHybridSizeY/2+VP:NewHybridYOverhang"
                  z="-(VP:SubstrateThick/2+3*VP:NewHybridKaptonThick/2+VP:NewHybridCopperThick)"/>
        <rotation/>
        <position/>
        <rotation z="-VP:Rotation"/>
      </transformation>
    </physvol>
    <physvol name="pvBottomKaptonBack"
             volume="lvHybridKaptonBack">
      <transformation>
        <position x="-VP:NewHybridSizeX/2+VP:NewHybridXOverhang"
                  y="-VP:NewHybridSizeY/2+VP:NewHybridYOverhang"
                  z="(VP:SubstrateThick/2+VP:NewHybridKaptonThick/2)"/>
        <rotation/>
        <position/>
        <rotation z="-VP:Rotation"/>
      </transformation>
    </physvol>
    <physvol name="pvCopperBack"
             volume="lvHybridCopperBack">
      <transformation>
        <position x="-VP:NewHybridSizeX/2+VP:NewHybridXOverhang"
                  y="-VP:NewHybridSizeY/2+VP:NewHybridYOverhang"
                  z="(VP:SubstrateThick/2+VP:NewHybridKaptonThick+VP:NewHybridCopperThick/2)"/>
        <rotation/>
        <position/>
        <rotation z="-VP:Rotation"/>
      </transformation>
    </physvol>
    <physvol name="pvTopKaptonBack"
             volume="lvHybridKaptonBack">
      <transformation>
        <position x="-VP:NewHybridSizeX/2+VP:NewHybridXOverhang"
                  y="-VP:NewHybridSizeY/2+VP:NewHybridYOverhang"
                  z="(VP:SubstrateThick/2+3*VP:NewHybridKaptonThick/2+VP:NewHybridCopperThick)"/>
        <rotation/>
        <position/>
        <rotation z="-VP:Rotation"/>
      </transformation>
    </physvol>

    <!-- The legs and their data connectors -->

    <!-- The strips at the bottom of the hybrid just above the legs -->
    <physvol name="pvBottomStripBack"
             volume="lvHybridBottomStrip">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridBottomStripSizeX/2)"
		z="(VP:SubstrateThick/2+VP:NewHybridThick/2)"/>
    </physvol>
    <physvol name="pvBottomStripFront"
             volume="lvHybridBottomStrip">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridBottomStripSizeX/2)"
		z="-(VP:SubstrateThick/2+VP:NewHybridThick/2)"/>
    </physvol>

    <!-- Back Left -->
    <physvol name="pvSideLegLeftBack"
             volume="lvHybridSideLeg">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridBottomStripSizeX+VP:NewHybridSideLegSizeX/2)"
		y="(VP:NewHybridBottomStripSizeY-VP:NewHybridSideLegSizeY)/2"
		z="(VP:SubstrateThick/2+VP:NewHybridThick/2)"/>
    </physvol>
    <!-- Back Right -->
    <physvol name="pvSideLegRightBack"
             volume="lvHybridSideLeg">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridBottomStripSizeX+VP:NewHybridSideLegSizeX/2)"
		y="-(VP:NewHybridBottomStripSizeY-VP:NewHybridSideLegSizeY)/2"
		z="(VP:SubstrateThick/2+VP:NewHybridThick/2)"/>
    </physvol>

    <!-- Front Left -->
    <physvol name="pvSideLegLeftFront"
             volume="lvHybridSideLeg">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridBottomStripSizeX+VP:NewHybridSideLegSizeX/2)"
		y="(VP:NewHybridBottomStripSizeY-VP:NewHybridSideLegSizeY)/2"
		z="-(VP:SubstrateThick/2+VP:NewHybridThick/2)"/>
      <rotation x="180*degree"/>
    </physvol>
    <!-- Front Right -->
    <physvol name="pvSideLegRightFront"
             volume="lvHybridSideLeg">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridBottomStripSizeX+VP:NewHybridSideLegSizeX/2)"
		y="-(VP:NewHybridBottomStripSizeY-VP:NewHybridSideLegSizeY)/2"
		z="-(VP:SubstrateThick/2+VP:NewHybridThick/2)"/>
      <rotation x="180*degree"/>
    </physvol>

    <!-- Back Connectors -->
    <physvol volume="lvDataConnector"
             name="pvDataConnectorLeftBack">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridSideLegSizeX-VP:DataConnectorX/2)"
		y="(VP:NewHybridBottomStripSizeY-VP:NewHybridSideLegSizeY)/2"
		z="(VP:SubstrateThick/2+VP:NewHybridThick+VP:DataConnectorThick/2)"/>
    </physvol>
    <physvol volume="lvDataConnector"
             name="pvDataConnectorRightBack">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridSideLegSizeX-VP:DataConnectorX/2)"
		y="-(VP:NewHybridBottomStripSizeY-VP:NewHybridSideLegSizeY)/2"
		z="(VP:SubstrateThick/2+VP:NewHybridThick+VP:DataConnectorThick/2)"/>
    </physvol>

    <!-- Front Connectors -->
    <physvol volume="lvDataConnector"
             name="pvDataConnectorLeftFront">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridSideLegSizeX-VP:DataConnectorX/2)"
		y="(VP:NewHybridBottomStripSizeY-VP:NewHybridSideLegSizeY)/2"
		z="-(VP:SubstrateThick/2+VP:NewHybridThick+VP:DataConnectorThick/2)"/>
      <rotation x="180*degree"/>
    </physvol>
    <physvol volume="lvDataConnector"
             name="pvDataConnectorRightFront">
      <position x="-(VP:NewHybridBaseDistFromIP+VP:NewHybridSideLegSizeX-VP:DataConnectorX/2)"
		y="-(VP:NewHybridBottomStripSizeY-VP:NewHybridSideLegSizeY)/2"
		z="-(VP:SubstrateThick/2+VP:NewHybridThick+VP:DataConnectorThick/2)"/>
      <rotation x="180*degree"/>
    </physvol>
  </volume>

  <!-- End of New Hybrid description -->

  <!-- PlanA CO2 manifold description -->
  <volume vis="VP:ModuleSupport" name="lvManifold" material="VP:Invar">
    <shape type="BooleanShape" operation="subtraction" name="CooolingConnectorBoxSub">
      <!-- Create a parent box -->
      <shape type="Box" name="CoolingConnectorBox"
             dx="0.5*VP:CoolingConnectorSizeX"
             dy="0.5*VP:CoolingConnectorSizeY"
             dz="0.5*VP:CoolingConnectorThickness"/>
      <!-- Round off the corners -->
      <shape type="Tube" name="CoolingConnectorCorner1"
             dz="VP:CoolingConnectorThickness"
             rmin="VP:CoolingConnectorCornerRadius"
             rmax="2*VP:CoolingConnectorCornerRadius"
             startphi="0*degree"
             deltaphi="90*degree"/>
      <position x="VP:CoolingConnectorSizeX/2-VP:CoolingConnectorCornerRadius"
		y="VP:CoolingConnectorSizeY/2-VP:CoolingConnectorCornerRadius"/>
      <shape type="Tube" name="CoolingConnectorCorner2"
             dz="VP:CoolingConnectorThickness"
             rmin="VP:CoolingConnectorCornerRadius"
             rmax="2*VP:CoolingConnectorCornerRadius"
             startphi="90*degree"
             deltaphi="90*degree"/>
      <position x="-(VP:CoolingConnectorSizeX/2-VP:CoolingConnectorCornerRadius)"
		y="VP:CoolingConnectorSizeY/2-VP:CoolingConnectorCornerRadius"/>
      <shape type="Tube" name="CoolingConnectorCorner3"
             dz="VP:CoolingConnectorThickness"
             rmin="VP:CoolingConnectorCornerRadius"
             rmax="2*VP:CoolingConnectorCornerRadius"
             startphi="180*degree"
             deltaphi="90*degree"/>
      <position x="-(VP:CoolingConnectorSizeX/2-VP:CoolingConnectorCornerRadius)"
		y="-(VP:CoolingConnectorSizeY/2-VP:CoolingConnectorCornerRadius)"/>
      <shape type="Tube" name="CoolingConnectorCorner4"
             dz="VP:CoolingConnectorThickness"
             rmin="VP:CoolingConnectorCornerRadius"
             rmax="2*VP:CoolingConnectorCornerRadius"
             startphi="270*degree"
             deltaphi="90*degree"/>
      <position x="VP:CoolingConnectorSizeX/2-VP:CoolingConnectorCornerRadius"
		y="-(VP:CoolingConnectorSizeY/2-VP:CoolingConnectorCornerRadius)"/>
      <!-- Make the two slit-like indents in the middle of CoolingConnector-->
      <!--First Indent-->
      <shape type="BooleanShape" operation="subtraction" name="CoolingConnectorIndentSlit1Sub">
        <shape type="Box" name="CoolingConnectorIndentSlit1Box"
               dx="0.5*VP:CoolingConnectorIndentSizeX"
               dy="0.5*VP:CoolingConnectorIndentSizeY"
               dz="VP:CoolingConnectorIndentDepth"/>
        <shape type="Tube" name="CoolingConnectorIndentSlit1Tubs1"
               dz="0.5*4*VP:CoolingConnectorIndentDepth"
               rmin="VP:CoolingConnectorIndentRadius"
               rmax="2*VP:CoolingConnectorIndentRadius"
               startphi="-90*degree"
               deltaphi="180*degree"/>
        <position x="VP:CoolingConnectorIndentSizeX/2-VP:CoolingConnectorIndentRadius"/>
        <shape type="Tube" name="CoolingConnectorIndentSlit1Tubs2"
               dz="0.5*4*VP:CoolingConnectorIndentDepth"
               rmin="VP:CoolingConnectorIndentRadius"
               rmax="2*VP:CoolingConnectorIndentRadius"
               startphi="90*degree"
               deltaphi="180*degree"/>
        <position x="-(VP:CoolingConnectorIndentSizeX/2-VP:CoolingConnectorIndentRadius)"/>
      </shape>
      <position x="(VP:CoolingConnectorIndentSizeX - VP:CoolingConnectorSizeX)/2 + VP:CoolingConnectorIndentX1"
		y="(VP:CoolingConnectorSizeY - VP:CoolingConnectorIndentSizeY)/2 - VP:CoolingConnectorIndentY1"
		z="VP:CoolingConnectorThickness/2"/>
      <!--Second Indent-->
      <shape type="BooleanShape" operation="subtraction" name="CoolingConnectorIndentSlit2Sub">
        <shape type="Box" name="CoolingConnectorIndentSlit2Box"
               dx="0.5*VP:CoolingConnectorIndentSizeX"
               dy="0.5*VP:CoolingConnectorIndentSizeY"
               dz="VP:CoolingConnectorIndentDepth"/>
        <shape type="Tube" name="VP:CoolingConnectorIndentSlit2Tubs1"
               dz="0.5*4*VP:CoolingConnectorIndentDepth"
               rmin="VP:CoolingConnectorIndentRadius"
               rmax="2*VP:CoolingConnectorIndentRadius"
               startphi="-90*degree"
               deltaphi="180*degree"/>
        <position x="VP:CoolingConnectorIndentSizeX/2-VP:CoolingConnectorIndentRadius"/>
        <shape type="Tube" name="CoolingConnectorIndentSlit2Tubs2"
               dz="0.5*4*VP:CoolingConnectorIndentDepth"
               rmin="VP:CoolingConnectorIndentRadius"
               rmax="2*VP:CoolingConnectorIndentRadius"
               startphi="90*degree"
               deltaphi="180*degree"/>
        <position x="-(VP:CoolingConnectorIndentSizeX/2-VP:CoolingConnectorIndentRadius)"/>
      </shape>
      <position x="(VP:CoolingConnectorIndentSizeX - VP:CoolingConnectorSizeX)/2 + VP:CoolingConnectorIndentX3"
		y="(VP:CoolingConnectorSizeY - VP:CoolingConnectorIndentSizeY)/2 - VP:CoolingConnectorIndentY1"
		z="VP:CoolingConnectorThickness/2"/>
      <!-- Define 2 pairs of small and big tubes to create the CO2 inlets-->
      <!--First Pair-->
      <shape type="Tube" name="CoolingConnectorBigTube1"
             rmax="VP:CoolingConnectorBigTubeRadius"
             dz="VP:CoolingConnectorBigTubeZ"/>
      <position x="VP:CoolingConnectorTubeCentreFromEdge-VP:CoolingConnectorSizeX/2"
		y="-VP:CoolingConnectorSizeY/2"/>
      <rotation x="90*degree"/>
      <shape type="Tube" name="CoolingConnectorSmallTube1"
             rmax="VP:CoolingConnectorSmallTubeRadius"
             dz="VP:CoolingConnectorSmallTubeZ"/>
      <position x="VP:CoolingConnectorTubeCentreFromEdge-VP:CoolingConnectorSizeX/2"
		y="-VP:CoolingConnectorSizeY/2"/>
      <rotation x="90*degree"/>
      <!--Second Pair-->
      <shape type="Tube" name="CoolingConnectorBigTube2"
             rmax="VP:CoolingConnectorBigTubeRadius"
             dz="VP:CoolingConnectorBigTubeZ"/>
      <position x="-(VP:CoolingConnectorTubeCentreFromEdge-VP:CoolingConnectorSizeX/2)"
		y="-VP:CoolingConnectorSizeY/2"/>
      <rotation x="90*degree"/>
      <shape type="Tube" name="CoolingConnectorSmallTube2"
             rmax="VP:CoolingConnectorSmallTubeRadius"
             dz="VP:CoolingConnectorSmallTubeZ"/>
      <position x="-(VP:CoolingConnectorTubeCentreFromEdge-VP:CoolingConnectorSizeX/2)"
		y="-VP:CoolingConnectorSizeY/2"/>
      <rotation x="90*degree"/>
    </shape>
  </volume>
  <!-- End of CO2 Manifold description -->

  <!-- Start of CO2 Delivery Pipe -->
  <!-- Delivery Pipe Metal -->
  <volume vis="VP:ModuleSupport" name="lvDeliveryPipeMetal" material="VP:StainlessSteel">
    <shape type="Tube" name="DeliveryPipeMetal"
           rmin="VP:DeliveryPipeInnerRadius"
           rmax="VP:DeliveryPipeOuterRadius"
           dz="0.5*VP:DeliveryPipeLength"/>
  </volume>

  <!-- Delivery Pipe CO2 -->
  <volume vis="VP:ModuleSupport" name="lvDeliveryPipeCO2" material="VP:Coolant">
    <shape type="Tube" name="DeliveryPipeCO2"
           rmin="0*mm"
           rmax="VP:DeliveryPipeInnerRadius-VP:VPEpsilon"
           dz="0.5*VP:DeliveryPipeLength"/>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvDeliveryPipe" material="Vacuum">
    <shape type="Tube" name="DeliveryPipeMotherVolume"
           rmin="VP:DeliveryPipeInnerRadius"
           rmax="VP:DeliveryPipeOuterRadius"
           dz="0.5*VP:DeliveryPipeLength"/>
    <physvol volume="lvDeliveryPipeMetal"
             name="pvDeliveryPipeMetal">
    </physvol>
    <physvol volume="lvDeliveryPipeCO2"
             name="pvDeliveryPipeCO2">
    </physvol>
  </volume>
  <!-- End of CO2 Delivery Pipe-->


  <!-- Start of the Nikhef support description -->

  <!-- Complete support design -->
  <volume vis="VP:ModuleSupport" name="lvSupportPipe" material="VP:CarbonFibre">
    <shape type="BooleanShape" operation="subtraction" name="lvSupportPipeSub" >
      <shape type="Tube" name="SupportPipeTubs"
             dz="VP:SupportPipeLength/2"
             rmin="VP:SupportPipeInnerRadius"
             rmax="VP:SupportPipeOuterRadius"/>
      <shape type="Box" name="lvSupportPipeBoxCut"
             dx="VP:SupportPipeGrooveDepth"
             dy="VP:SupportPipeGrooveLength"
             dz="0.5*VP:SupportPipeGrooveLength"/>
      <position x="VP:SupportPipeOuterRadius"
		z="VP:SupportPipeLength/2 + VP:SupportPipeGrooveLength/2 -17.5*mm"/>
    </shape>
  </volume>

  <volume vis="VP:ModuleSupport" name="lvSupportMidPlate" material="VP:Borofloat">
    <shape type="BooleanShape" operation="subtraction" name="SupportMidPlateSub" >
      <shape type="Box" name="SupportMidPlateSubBox1"
             dx="0.5*VP:SupportMidPlateSizeX"
             dy="0.5*VP:SupportMidPlateSizeY"
             dz="0.5*VP:SupportMidPlateThick"/>
      <shape type="BooleanShape" operation="subtraction" name="SupportMidPlateSub2">
        <shape type="Box" name="SupportMidPlateSubBox2"
               dx="VP:SupportMidPlateCornerSizeX"
               dy="VP:SupportMidPlateCornerSizeY"
               dz="VP:SupportMidPlateThick"/>
        <shape type="Tube" name="SupportMidPlateSubTubs1"
               dz="2*VP:SupportMidPlateThick"
               rmin="VP:SupportMidPlateCornerRadius"
               rmax="2*VP:SupportMidPlateCornerRadius"
               startphi="90*degree"
               deltaphi="90*degree"/>
        <position x="-VP:SupportMidPlateCornerSizeX + VP:SupportMidPlateCornerRadius" y="VP:SupportMidPlateCornerSizeY - VP:SupportMidPlateCornerRadius"/>
      </shape>
      <position x="VP:SupportMidPlateSizeX/2" y="-VP:SupportMidPlateSizeY/2"/>
      <shape type="BooleanShape" operation="subtraction" name="SupportMidPlateSub3">
        <shape type="Box" name="SupportMidPlateSubBox3"
               dx="VP:SupportMidPlateCornerSizeX"
               dy="VP:SupportMidPlateCornerSizeY"
               dz="VP:SupportMidPlateThick"/>
        <shape type="Tube" name="SupportMidPlateSubTubs2"
               dz="2*VP:SupportMidPlateThick"
               rmin="VP:SupportMidPlateCornerRadius"
               rmax="2*VP:SupportMidPlateCornerRadius"
               startphi="180*degree"
               deltaphi="90*degree"/>
        <position x="-VP:SupportMidPlateCornerSizeX + VP:SupportMidPlateCornerRadius" y="-VP:SupportMidPlateCornerSizeY + VP:SupportMidPlateCornerRadius"/>
      </shape>
      <position x="VP:SupportMidPlateSizeX/2" y="VP:SupportMidPlateSizeY/2"/>
    </shape>
  </volume>
  <volume vis="VP:Assembly" name="lvNikhefSupport">
    <physvol volume="lvSupportPipe"
             name="pvPipeBottom">
      <position x="-VP:SupportPipeLength/2"
		y="-(VP:SupportMidPlateSizeY/2 - VP:SupportPipeOuterRadius)"/>
      <rotation y="90*degree"/>
    </physvol>
    <physvol volume="lvSupportPipe"
             name="pvPipeTop">
      <position x="-VP:SupportPipeLength/2"
		y="(VP:SupportMidPlateSizeY/2 - VP:SupportPipeOuterRadius)"/>
      <rotation y="90*degree"/>
    </physvol>
    <physvol volume="lvSupportMidPlate"
             name="pvSupportMidPlate">
      <position x="VP:SupportMidPlateSizeX/2 - 17.25*mm"
		z="-VP:SupportPipeOuterRadius - VP:SupportMidPlateThick/2 + (VP:SupportPipeGrooveDepth - 0.05*mm)"/>
    </physvol>
  </volume>


  <!-- Total support -->
  <volume vis="VP" name="lvSupport">
    <!-- The carbon fibre and borofloat hurdle -->
    <physvol volume="lvNikhefSupport"             name="pvNikhefSupport">
      <position x="-(VP:SubstrateNewIPXDistFromBL + 1.5*mm)"/>
    </physvol>

    <!-- The CO2 manifold -->
    <physvol name="pvCoolingConnector"            volume="lvManifold">
      <position x="-(VP:SubstrateNewIPXDistFromBL - VP:CoolingConnectorDistFromSubstrateBase - VP:CoolingConnectorSizeY/2)"
		z="-(VP:SubstrateThick + VP:CoolingConnectorThickness)/2"/>
      <rotation z="-90*degree"/>
    </physvol>

    <!-- The CO2 delivery pipes -->
    <physvol volume="lvDeliveryPipe"              name="pvDeliveryPipe1">
      <position x="-(VP:SubstrateNewIPXDistFromBL - VP:CoolingConnectorDistFromSubstrateBase + VP:DeliveryPipeLength/2 - VP:CoolingConnectorBigTubeZ)"
		y="VP:CoolingConnectorSizeX/2 - VP:CoolingConnectorTubeCentreFromEdge"
		z="-(VP:SubstrateThick + VP:CoolingConnectorThickness)/2"/>
      <rotation y="90*degree"/>
    </physvol>
    <physvol volume="lvDeliveryPipe"              name="pvDeliveryPipe2">
      <position x="-(VP:SubstrateNewIPXDistFromBL - VP:CoolingConnectorDistFromSubstrateBase + VP:DeliveryPipeLength/2 - VP:CoolingConnectorBigTubeZ)"
		y="-(VP:CoolingConnectorSizeX/2 - VP:CoolingConnectorTubeCentreFromEdge)"
		z="-(VP:SubstrateThick + VP:CoolingConnectorThickness)/2"/>
      <rotation y="90*degree"/>
    </physvol>
  </volume>

</velo>

