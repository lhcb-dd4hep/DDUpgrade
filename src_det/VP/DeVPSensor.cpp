//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================

// Framework include files
#include "Detector/VP/DeVPSensor.h"
#include "DD4hep/Primitives.h"
#include "DD4hep/Printout.h"

namespace gaudi  {
  const DeVPSensorElement::itemkey_type DeVPSensorElement::key_info    = dd4hep::ConditionKey::itemCode("StripInfo");
  const DeVPSensorElement::itemkey_type DeVPSensorElement::key_noise   = dd4hep::ConditionKey::itemCode("StripNoise");
  const DeVPSensorElement::itemkey_type DeVPSensorElement::key_readout = dd4hep::ConditionKey::itemCode("StripReadout");
}

/// Printout method to stdout
void gaudi::detail::DeVPSensorStaticObject::print(int indent, int flg)  const   {

  this->DeStaticObject::print(indent, flg);
  if ( flg & DePrint::SPECIFIC )  {
    printout(INFO,"DeVPSensorStatic",
             "%s+  >> Module:%d Sensor:%d %s %d Chips Rows:%d Cols:%d",
             DE::indent(indent).c_str(),
             module, sensorNumber, (de_user&VP::LEFT) ? "Left" : "Right", nChips, nCols, nRows);
  }
  if ( flg & DePrint::DETAIL )  {
    printout(INFO,"DeVPSensorStatic",
             "%s+  >> Thickness:%g ChipSize:%g Dist:%g Pix-Size:%g Dist:%g",
             DE::indent(indent).c_str(),
             thickness, chipSize, interChipDist, pixelSize, interChipPixelSize);
    printout(INFO,"DeVPSensorStatic",
             "%s+  >> SizeX: %g SizeY: %g local:%ld pitch:%ld",
             DE::indent(indent).c_str(), sizeX, sizeY, local_x.size(), x_pitch.size());
  }
}

/// Printout method to stdout
void gaudi::detail::DeVPSensorObject::print(int indent, int flg)  const   {
  DeIOVObject::print(indent, flg);
  printout(INFO,"DeVPSensor", "%s+  >> Info: %s Noise:%s Readout:%s",
           DE::indent(indent).c_str(),
           yes_no(info.isValid()),
           yes_no(noise.isValid()),
           yes_no(readout.isValid()));
}

