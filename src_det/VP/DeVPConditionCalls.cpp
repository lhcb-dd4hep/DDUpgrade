//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
// Framework include files
#include "Detector/VP/DeVPConditionCalls.h"
#include "DD4hep/DetectorProcessor.h"
#include "XML/XML.h"

using namespace std;
using namespace gaudi;
using namespace dd4hep;

namespace {
  template <typename T>
  void collect_sensors(T gen, DeConditionCall::Context& ctxt)  {
    typename T::Object* elt = gen.ptr();
    ConditionKey::KeyMaker km(elt->hash);
    unsigned int flag = elt->de_user;
    for ( const auto& c : elt->detector.children() )   {
      if ( ::strncmp(c.first.c_str(),"RF",2) == 0 )  continue;
      if ( ::strncmp(c.first.c_str(),"Support",4) == 0 )  continue;
      typename T::base_t::Object::Children::value_type daughter =
        ctxt.condition(ConditionKey::KeyMaker(c.second.key(),km.values.item_key).hash);
      if ( (daughter->de_user & VP::SENSOR) )  {
        typename T::Sensors::value_type sens(daughter);
        elt->children.push_back(sens);
        elt->sensors.push_back(sens);
        sens->assembly = gen;
        continue;
      }
      T g = daughter;
      string path = g->detector.path();
      g->parent = elt;
      elt->children.push_back(g);
      for( auto sens : g->sensors )  {
        elt->sensors.push_back(sens);
        if      ( flag & VP::MODULE   ) sens->module   = gen;
        else if ( flag & VP::ASSEMBLY ) sens->assembly = gen;
        else if ( flag & VP::SIDE     ) sens->side     = gen;
        else if ( flag & VP::TOP      ) sens->velo     = gen;
      }
    }
  }
}

/// Interface to client Callback in order to update the condition
template <typename T> dd4hep::Condition DeVPConditionCall<T>::operator()(const ConditionKey& key, Context& ctxt)  {
  DetElement de = detectorElement(key.hash);
  auto*      s  = new typename T::base_t::Object();
  s->initialize(de, ctxt);
  s->de_user |= T::base_t::Object::typeID;
  s->data.bindExtern(s);
  return s;
}

/// Interface to client callback for resolving references or to use data from other conditions
template <typename T> void DeVPConditionCall<T>::resolve(Condition c, Context& ctxt)    {
  DeStatic s(c);
  s->resolve(s->detector, ctxt);
}

/// Interface to client Callback in order to update the condition
template <> Condition
DeVPConditionCall<DeVPStatic>::operator()(const ConditionKey& key, Context& ctxt)  {
  DetElement          de = detectorElement(key.hash);
  DeVPStatic::Object* s = new DeVPStatic::Object();
  s->initialize(de, ctxt);
  s->de_user |= VP::TOP;
  s->sensitiveVolumeCut = _toDouble("VP:sensitiveVolumeCut");
  s->data.bindExtern(s);
  return DeStatic(s);
}

/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeVPConditionCall<DeVPStatic>::resolve(Condition c, Context& ctxt)  {
  DeVPStatic vp = c;
  DeVPStatic::Object* obj = vp.access();
  obj->de_user |= VP::TOP;
  obj->sensors.reserve(208);
  obj->resolve(obj->detector, ctxt);
  obj->sides.resize(2);
  for ( const auto& child : obj->detector.children() )   {
    bool left = false, right = false;
    if ( 0 == ::strncmp(child.first.c_str(),"VPLeft",6) )
      left = true;
    else if ( 0 == ::strncmp(child.first.c_str(),"VPRight",6) )
      right = true;
    else
      continue;

    ConditionKey::KeyMaker km(child.second.key(),Keys::staticKey);
    DeVPGenericStatic side = ctxt.condition(km.hash);
    if ( left  ) obj->left  = side;
    if ( right ) obj->right = side;
    obj->sides[side->detector.id()] = side;
    for( auto module : side->children )  {
      DeVPGenericStatic genmod = module;
      obj->modules.push_back(module);
      for( auto assembly : genmod->children )  {
        DeVPGenericStatic genassembly = assembly;
        obj->assemblies.push_back(assembly);
        for( auto sensor : genassembly->children )  {
          DeVPSensorStatic sens = sensor;
          obj->sensors.push_back(sensor);
          sens->velo = vp;
        }
      }
    }
  }
}

/// Interface to client Callback in order to update the condition
template <> Condition
DeVPConditionCall<DeVPGenericStatic>::operator()(const ConditionKey& key, Context& ctxt)  {
  DetElement par, de = detectorElement(key.hash);
  DeVPGenericStatic::Object* s = new DeVPGenericStatic::Object();
  s->initialize(de, ctxt);
  s->data.bindExtern(s);
  vector<DetElement> parents;
  for(par = de; par.name() != string("VP"); par=par.parent() )
    parents.push_back(par);
  DetElement half = parents.empty() ? de : parents.back();
  ::strcmp(half.name(),"VPLeft") == 0 ? s->de_user |= VP::LEFT : s->de_user |= VP::RIGHT;
  switch(parents.size())  {
  case 1:  // VP half
    s->de_user |= VP::SIDE;
    break;
  case 2:  // Module with support
    s->de_user |= VP::MODULE;
    break;
  case 3:  // Asembly with sensor ladders (=Sensors)
    s->de_user |= VP::ASSEMBLY;
    break;
  case 4:  // Ladder
    s->de_user |= VP::SENSOR;
    break;
  case 5:  // Sensor: We should never end up here!
    s->de_user |= VP::ASIC;
    break;
  default:
    break;
  }
  return DeStatic(s);
}

template <> void DeVPConditionCall<DeVPGenericStatic>::resolve(Condition c, Context& ctxt)   {
  DeVPGenericStatic gen(c);
  gen->resolve(gen->detector, ctxt);
  //printout(INFO,"VPGeneric","+++ Resolve: %s",gen->detector.path().c_str());
  collect_sensors(gen, ctxt);
}

/// Interface to client Callback in order to update the condition
template <> Condition
DeVPConditionCall<DeVPSensorStatic>::operator()(const ConditionKey& key, Context& ctxt)  {
  DetElement de = detectorElement(key.hash);
  DetElement mod_with_support = de.parent().parent();
  DeVPSensorStatic::Object* s = new DeVPSensorStatic::Object();
  // Hierarchy is Sensor-Ladder-Module-ModuleWithSupport-Half-VP
  string side   = mod_with_support.parent().name();
  s->initialize(de, ctxt);
  s->data.bindExtern(s);
  s->de_user           |= VP::SENSOR;
  side.find("VPLeft") == 0 ? s->de_user |= VP::LEFT : s->de_user |= VP::RIGHT;
  s->sensorNumber       = de.id();
  s->moduleNumber       = mod_with_support.id();
  s->nChips             = _toInt("VP:NChips");
  s->nCols              = _toInt("VP:NColumns");
  s->nRows              = _toInt("VP:NRows");

  s->thickness          = _toDouble("VP:Thickness");
  s->chipSize           = _toDouble("VP:ChipSizeX");
  s->pixelSize          = _toDouble("VP:PixelSize");
  s->interChipDist      = _toDouble("VP:InterChipDist");
  s->interChipPixelSize = _toDouble("VP:InterChipPixelSize");
  s->sizeX              = s->nChips * s->chipSize + (s->nChips - 1) * s->interChipDist;
  s->sizeY              = s->chipSize;
  for (unsigned int col = 0; col < VP::NSensorColumns; ++col) {
    // Calculate the x-coordinate of the pixel centre and the pitch.
    const double x0 = (col / VP::NColumns) * (s->chipSize + s->interChipDist);
    double x = x0 + (col % VP::NColumns + 0.5) * s->pixelSize;
    double pitch = s->pixelSize;
    switch (col) {
    case 256:
    case 512:
      // right of chip border
      x     -= 0.5 * (s->interChipPixelSize - s->pixelSize);
      pitch  = 0.5 * (s->interChipPixelSize + s->pixelSize);
      break;
    case 255:
    case 511:
      // left of chip border
      x     += 0.5 * (s->interChipPixelSize - s->pixelSize);
      pitch  = s->interChipPixelSize;
      break;
    case 254:
    case 510:
      // two left of chip border
      pitch = 0.5 * (s->interChipPixelSize + s->pixelSize);
      break;
    }
    s->local_x[col] = x;
    s->x_pitch[col] = pitch;
  }
  return DeStatic(s);
}

template void DeVPConditionCall<DeVPSensorStatic>::resolve(Condition, Context&);

template Condition DeVPConditionCall<DeVP>::operator()(const ConditionKey&, Context&);

/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeVPConditionCall<DeVP>::resolve(Condition c, Context& ctxt)    {
  DeVP vp = c;
  DeVP::Object* obj = vp.access();
  detail::default_iov_resolve(vp, ctxt);
  obj->de_user |= VP::TOP;
  obj->sensors.reserve(208);
  obj->sides.resize(2);
  //int cnt = 1;
  for ( const auto& child : obj->detector.children() )   {
    bool left = false, right = false;
    if ( 0 == ::strncmp(child.first.c_str(),"VPLeft",6) )
      left = true;
    else if ( 0 == ::strncmp(child.first.c_str(),"VPRight",6) )
      right = true;
    else
      continue;
    
    ConditionKey::KeyMaker km(child.second.key(),Keys::deKey);
    DeVPGeneric side = ctxt.condition(km.hash);
    if ( left  ) obj->left = side;
    if ( right ) obj->right = side;
    obj->sides[side->detector.id()] = side;
#if 0
    for( auto sens : side->sensors )  {
      DeVPGeneric sens_side = sens->side;
      cout << "Count:" << cnt
           << " Side:   " << sens_side->detector.id()
           << " Module: " << sens.moduleNumber()
           << " Sensor: " << sens.sensorNumber() << " " << sens.name() << endl;
      ++cnt;
    }
#endif
    bool dbg = false;
    if ( dbg ) cout << "Side: " << side->detector.path() << endl;
    for( auto module : side->children )  {
      DeVPGeneric genmod = module;
      obj->modules.push_back(module);
      if ( dbg ) cout << "    Module: " << genmod->detector.path() << endl;
      for( auto assembly : genmod->children )  {
        DeVPGeneric genassembly = assembly;
        obj->assemblies.push_back(assembly);
        if ( dbg ) cout   << "        Assembly: " << genassembly->detector.path() << endl;
        for( auto sensor : genassembly->children )  {
          DeVPSensor sens = sensor;
          obj->sensors.push_back(sensor);
          sens->velo = vp;
          if ( dbg ) cout << "        Sensor:   " << sensor->detector.path() << endl;
        }
      }
    }
  }
}

template Condition DeVPConditionCall<DeVPSensor>::operator()(const ConditionKey&, Context&);
/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeVPConditionCall<DeVPSensor>::resolve(Condition cond, Context& ctxt)    {
  DeVPSensor sensor(cond);
  detail::default_iov_resolve(sensor, ctxt);
  // We require a valid alignment object for sensors!
  sensor->checkAlignment();
  for(const auto& c : sensor->conditions)   {
    if ( c.first == DeVPSensor::key_info ) sensor->info = c.second;
    else if ( c.first == DeVPSensor::key_noise ) sensor->noise = c.second;
    else if ( c.first == DeVPSensor::key_readout ) sensor->readout = c.second;
  }
  // Check here if values must be valid ?
  if ( !sensor->info.isValid() )  {
    // except(DeVPSensor", "Invalid IOV dependent sensor info!");
  }
  if ( !sensor->noise.isValid() )  {
    // except(DeVPSensor", "Invalid IOV dependent sensor noise!");
  }
  if ( !sensor->readout.isValid() )  {
    // except(DeVPSensor", "Invalid IOV dependent sensor readout!");
  }
}

template Condition DeVPConditionCall<DeVPGeneric>::operator()(const ConditionKey&, Context&);
/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeVPConditionCall<DeVPGeneric>::resolve(Condition c, Context& ctxt)    {
  DeVPGeneric iov(c);
  ctxt.condition(Keys::alignmentsComputedKey);
  iov->resolve(iov->detector, ctxt);
  iov->concrete_static = iov->de_static;
  iov->de_user         = iov->de_static->de_user;
  collect_sensors(iov, ctxt);  
}
