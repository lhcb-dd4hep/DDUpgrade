//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================

// A bit of magic you do not really want to know about....

// Framework include files
#include "Detector/VP/DeVP.h"
#include "DD4hep/detail/Handle.inl"

using namespace gaudi::detail;

DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeVPSensorStaticObject,DeStaticObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeVPGenericStaticObject,DeStaticObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeVPStaticObject,DeVPGenericStaticObject,DeStaticObject,ConditionObject);

DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeVPSensorObject,DeIOVObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeVPGenericObject,DeIOVObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeVPObject,DeVPGenericObject,DeIOVObject,ConditionObject);
