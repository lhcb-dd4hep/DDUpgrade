//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================

// A bit of magic you do not really want to know about....

// Framework include files
#include "Detector/FT/DeFT.h"
#include "DD4hep/detail/Handle.inl"

using namespace gaudi::detail;

DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTMatStaticObject,DeStaticObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTModuleStaticObject,DeStaticObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTQuarterStaticObject,DeStaticObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTLayerStaticObject,DeStaticObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTStationStaticObject,DeStaticObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTStaticObject,DeStaticObject,ConditionObject);

DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTMatObject,DeIOVObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTModuleObject,DeIOVObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTQuarterObject,DeIOVObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTLayerObject,DeIOVObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTStationObject,DeIOVObject,ConditionObject);
DD4HEP_INSTANTIATE_HANDLE_UNNAMED(DeFTObject,DeIOVObject,ConditionObject);

