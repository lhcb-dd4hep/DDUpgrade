//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================

// Framework include files
#include "Detector/FT/DeFTConditionCalls.h"
#include "DD4hep/DetectorProcessor.h"
#include "DD4hep/detail/DetectorInterna.h"
#include "XML/XML.h"

using namespace std;
using namespace gaudi;
using namespace dd4hep;

static PrintLevel print_level = DEBUG;

/// Interface to client Callback in order to update the condition
template <typename T> Condition DeFTConditionCall<T>::operator()(const ConditionKey& key, Context& ctxt)  {
  DetElement de = detectorElement(key.hash);
  auto*      s  = new typename T::base_t::Object();
  s->initialize(de, ctxt);
  s->de_user |= T::base_t::Object::type;
  return s;
}

/// Interface to client callback for resolving references or to use data from other conditions
template <typename T> void DeFTConditionCall<T>::resolve(Condition c, Context& ctxt)    {
  DeStatic s(c);
  s->resolve(s->detector, ctxt);
}

/// Interface to client Callback in order to update the condition
template <> Condition
DeFTConditionCall<DeFTStatic>::operator()(const ConditionKey& key, Context& ctxt)  {
  DetElement          de = detectorElement(key.hash);
  DeFTStatic::Object* s = new DeFTStatic::Object();
  s->initialize(de, ctxt);
  s->de_user          |= FT::TOP;
  s->version           = _toInt("FT:version");
  s->nModulesT1        = _toInt("FT:nModulesT1");
  s->nModulesT2        = _toInt("FT:nModulesT2");
  s->nModulesT3        = _toInt("FT:nModulesT3");
  s->nLayers           = _toInt("FT:nLayers");
  s->nQuarters         = _toInt("FT:nQuarters");
  s->nChannelsInModule = _toInt("FT:nChannelsInModule");
  return DeStatic(s);
}

/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeFTConditionCall<DeFTStatic>::resolve(Condition c, Context& ctxt)    {
  DeFTStatic ft(c);
  ft->resolve(ft->detector, ctxt);
  for ( const auto& i : ft->detector.children() )   {
    DetElement        de = i.second;
    DeFTStationStatic st = ctxt.condition(KeyMaker(de.key(), Keys::staticKey).hash);
    int idx = st->detector.id();
    st->parent = ft.ptr();
    ft->stations[idx] = st;
  }
}

template Condition DeFTConditionCall<DeFTStationStatic>::operator()(const ConditionKey&, Context&);
/// Update the dependent references of the FTStation to its children of type FTLayer
template <> void DeFTConditionCall<DeFTStationStatic>::resolve(Condition c, Context& ctxt)   {
  DeFTStationStatic station(c);
  const auto& children = station->detector.children();
  station->resolve(station->detector, ctxt);
  for ( const auto& i : children )   {
    DetElement       de     = i.second;
    DeFTLayerStatic layer = ctxt.condition(KeyMaker(de.key(), Keys::staticKey).hash);
    int idx = layer->detector.id();
    layer->parent        = station.ptr();
    station->layers[idx] = layer;
  }
}

/// Interface to client Callback in order to update the condition
template Condition
DeFTConditionCall<DeFTLayerStatic>::operator()(const ConditionKey& key, Context& ctxt);
template <> void DeFTConditionCall<DeFTLayerStatic>::resolve(Condition c, Context& ctxt)   {
  DeFTLayerStatic layer(c);
  const auto& children = layer->detector.children();
  layer->resolve(layer->detector, ctxt);
  layer->stereoAngle = _toDouble(string("FT:StereoAngle")+*(layer->detector.name()));
  for ( const auto& i : children )   {
    DetElement        de      = i.second;
    DeFTQuarterStatic quarter = ctxt.condition(KeyMaker(de.key(), Keys::staticKey).hash);
    int idx = quarter->detector.id();
    quarter->parent   = layer.ptr();
    layer->quarters[idx] = quarter;
  }
}

template Condition DeFTConditionCall<DeFTQuarterStatic>::operator()(const ConditionKey&, Context&);
template <> void DeFTConditionCall<DeFTQuarterStatic>::resolve(Condition c, Context& ctxt)   {
  DeFTQuarterStatic quarter(c);
  const auto& children = quarter->detector.children();
  quarter->resolve(quarter->detector, ctxt);
  quarter->modules.resize(children.size());
  for ( const auto& i : children )   {
    DetElement       de     = i.second;
    DeFTModuleStatic module = ctxt.condition(KeyMaker(de.key(), Keys::staticKey).hash);
    int idx = module->detector.id();
    module->parent   = quarter.ptr();
    quarter->modules[idx] = module;
  }
}

template Condition DeFTConditionCall<DeFTModuleStatic>::operator()(const ConditionKey&, Context&);
template <> void DeFTConditionCall<DeFTModuleStatic>::resolve(Condition c, Context& ctxt)   {
  DeFTModuleStatic module(c);
  const auto& children = module->detector.children();
  module->resolve(module->detector, ctxt);
  for ( const auto& i : children )   {
    DetElement    de  = i.second;
    DeFTMatStatic mat = ctxt.condition(KeyMaker(de.key(), Keys::staticKey).hash);
    int idx = mat->detector.id();
    mat->parent = module.ptr();
    module->mats[idx] = mat;
  }
}

// Nothing extra to do. Individual mats cannot be identified as top level elements!
template Condition DeFTConditionCall<DeFTMatStatic>::operator()(const ConditionKey&, Context&);
template void DeFTConditionCall<DeFTMatStatic>::resolve(Condition, Context&);

template Condition DeFTConditionCall<DeFT>::operator()(const ConditionKey&, Context&);
/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeFTConditionCall<DeFT>::resolve(Condition c, Context& ctxt)    {
  DeFT ft(c);
  const auto& children = ft->detector.children();
  detail::default_iov_resolve(ft, ctxt);
  for ( const auto& i : children )   {
    DetElement de     = i.second;
    DeFTStation st    = ctxt.condition(KeyMaker(de.key(), Keys::deKey).hash);
    int idx           = st->detector->id;
    st->parent        = ft;
    ft->stations[idx] = st;
  }
  printout(print_level,"DeFT","Add Subdetector FT [%03d]:  %s", ft->detector.id(), ft->detector.path().c_str());
}

template Condition DeFTConditionCall<DeFTStation>::operator()(const ConditionKey&, Context&);
/// Update the dependent references of the FTStation to its children of type FTLayer
template <> void DeFTConditionCall<DeFTStation>::resolve(Condition c, Context& ctxt)    {
  DeFTStation station(c);
  const auto& children = station->detector.children();
  detail::default_iov_resolve(station, ctxt);
  for ( const auto& i : children )   {
    DetElement de = i.second;
    DeFTLayer  layer = ctxt.condition(KeyMaker(de.key(), Keys::deKey).hash);
    int idx = layer->detector->id;
    layer->parent = station;
    station->layers[idx] = layer;
  }
  printout(print_level,"DeFT","Add Station [%03d]:  %s", station->detector.id(), station->detector.path().c_str());
}

template Condition DeFTConditionCall<DeFTLayer>::operator()(const ConditionKey&, Context&);
/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeFTConditionCall<DeFTLayer>::resolve(Condition c, Context& ctxt)    {
  DeFTLayer layer(c);
  const auto& children = layer->detector.children();
  detail::default_iov_resolve(layer, ctxt);
  for ( const auto& i : children )   {
    DetElement de        = i.second;
    DeFTQuarter quart    = ctxt.condition(KeyMaker(de.key(), Keys::deKey).hash);
    int idx              = quart->detector->id;
    quart->parent        = layer;
    layer->quarters[idx] = quart;
  }
  printout(print_level,"DeFT","Add Layer    [%03d]:  %s", layer->detector.id(),layer->detector.path().c_str());
}

template Condition DeFTConditionCall<DeFTQuarter>::operator()(const ConditionKey&, Context&);
/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeFTConditionCall<DeFTQuarter>::resolve(Condition c, Context& ctxt)    {
  DeFTQuarter quarter(c);
  const auto& children = quarter->detector.children();
  detail::default_iov_resolve(quarter, ctxt);
  quarter->modules.resize(children.size());
  for ( const auto& i : children )   {
    DetElement de         = i.second;
    DeFTModule module     = ctxt.condition(KeyMaker(de.key(), Keys::deKey).hash);
    int idx               = module->detector->id;
    module->parent        = quarter;
    quarter->modules[idx] = module;
  }
  printout(print_level,"DeFT","Add Quarter    [%03d]:  %s", quarter->detector.id(),quarter->detector.path().c_str());
}

template Condition DeFTConditionCall<DeFTModule>::operator()(const ConditionKey&, Context&);
/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeFTConditionCall<DeFTModule>::resolve(Condition c, Context& ctxt)    {
  DeFTModule module(c);
  const auto& children = module->detector.children();
  detail::default_iov_resolve(module, ctxt);
  for ( const auto& i : children )   {
    DetElement de     = i.second;
    DeFTMat mat       = ctxt.condition(KeyMaker(de.key(), Keys::deKey).hash);
    int idx           = mat->detector->id;
    mat->parent       = module;
    if ( idx < 0 || idx > 3 )   {
      printout(ERROR,"DeFT","**************************************************");
    }
    module->mats[idx] = mat;
  }
  printout(print_level,"DeFT","Add Module    [%03d]:  %s", module->detector.id(),module->detector.path().c_str());
}

template Condition DeFTConditionCall<DeFTMat>::operator()(const ConditionKey&, Context&);
/// Interface to client callback for resolving references or to use data from other conditions
template <> void DeFTConditionCall<DeFTMat>::resolve(Condition c, Context& ctxt)    {
  detail::default_iov_resolve(DeFTMat(c), ctxt);
}

