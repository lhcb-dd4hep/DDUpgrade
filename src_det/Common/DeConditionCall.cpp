//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-11-08
//  \version  1.0
//
//==============================================================================

// Framework include files
#include "DD4hep/Printout.h"
#include "Detector/Common/DeConditionCall.h"

using namespace gaudi;
using namespace dd4hep;
using namespace dd4hep::cond;


/// Interface to client Callback in order to update the condition
Condition DeDefaultDeltaCall::operator()(const ConditionKey& /* key */, Context& /* ctxt */)  {
  Condition c("","alignment_delta");
  c.bind<Delta>();
  return c;
}


/// Selector call for single condition
void RequestSelector::select(Condition::key_type key)   {
  // There are more derived conditions than normal numbers. Check these always first.
  auto rd = required.derived().find(key);
  if ( rd != required.derived().end() )   {
    return;
  }
  auto rc = required.conditions().find(key);
  if ( rc != required.conditions().end() )   {
    return;
  }
  auto ed = existing.derived().find(key);
  if ( ed != existing.derived().end() )   {
    ConditionDependency* dep = (*ed).second;
    required.addDependency(dep);
    for ( const auto& e : dep->dependencies )
      select(e.hash);
    return;
  }
  auto ec = existing.conditions().find(key);
  if ( ec != existing.conditions().end() )   {
    required.addLocationInfo(ec->first, ec->second);
    return;
  }
  // Serious error ..... We should never end up here
  except("DeRequestSelector",
         "No conditions recipe found for conditions key: %s",
         ConditionKey(key).toString().c_str());
}

/// Selector call for condition groups
void RequestSelector::select(std::set<Condition::key_type>& needed, bool clr)  {
  if ( clr ) required.clear();
  for (const auto key : needed )   {
    select(key);
  }
}

/// Selector call for condition groups
void RequestSelector::select(std::vector<Condition::key_type>& needed, bool clr)  {
  if ( clr ) required.clear();
  for (const auto key : needed )   {
    select(key);
  }
}

/// Initializing constructor
DeConditionCall::DeConditionCall(std::shared_ptr<ConditionsRepository>& rep)
  : repository(rep.get())
{
} 

/// Default destructor
DeConditionCall::~DeConditionCall()  {
}

/// Access the conditions derivation given the condiitons key using the repository
const ConditionDependency* DeConditionCall::derivationInfo(key_type key) const   {
  const auto itr_info = repository->derived().find(key);
  if ( itr_info == repository->derived().end() )  {
    except("Condiitons","++ No detector element was present of conditions key:%016X", key);
  }
  return (*itr_info).second;
}

/// Access the detector element given the condiitons key using the repository
DetElement DeConditionCall::detectorElement(key_type key)   const   {
  return derivationInfo(key)->detector;
}

