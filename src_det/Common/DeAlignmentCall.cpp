//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================

// Framework include files
#include "Detector/Common/DeAlignmentCall.h"
#include "Detector/Common/DetectorElement.h"
#include "DD4hep/AlignmentsCalculator.h"
#include "TTimeStamp.h"

using namespace dd4hep;

/// Interface to client Callback in order to update the condition
Condition gaudi::DeAlignmentCall::operator()(const ConditionKey& /* key */,
                                             cond::ConditionUpdateContext& ctxt)  {
  typedef align::AlignmentsCalculator Calculator;
  typedef Calculator::OrderedDeltas Deltas;
  Calculator        calc;
  ConditionsHashMap slice;
  TTimeStamp start;
  Condition  cond = Condition(Keys::alignmentsComputedKeyName,"Calculator");
  Deltas& deltas = cond.bind<Deltas>();
  size_t  num_delta = calc.extract_deltas(ctxt, deltas);
  Calculator::Result ares = calc.compute(deltas, slice);
  if ( ctxt.registerMapping(*ctxt.iov, slice.data) == slice.data.size() )  {
    TTimeStamp stop;
    printout(INFO,"Align","Alignments:(D:%ld,C:%ld,M:%ld,*:%ld) Effective IOV:%s  [%7.5f seconds]",
             num_delta, ares.computed, ares.missing, ares.multiply,
             ctxt.iov->str().c_str(),stop.AsDouble()-start.AsDouble());
    return cond;
  }
  except("DeAlignmentCall","Failed to register all alignment conditions!");
  return nullptr;
}
