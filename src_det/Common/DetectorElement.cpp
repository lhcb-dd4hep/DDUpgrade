//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================

// Framework include files
#include "Detector/Common/DetectorElement.h"
#include "DD4hep/Alignments.h"
#include "DD4hep/Printout.h"


namespace gaudi  {
  typedef dd4hep::ConditionKey _K;
  /// Static key name: "DetElement-Info-Static"
  const std::string        Keys::staticKeyName     ("DetElement-Info-Static");
  /// Static key: 32 bit hash of "DetElement-Info-Static". Must be unique for one DetElement
  const Keys::itemkey_type Keys::staticKey = _K::itemCode(Keys::staticKeyName);
  /// Static key name: "DetElement-Info-IOV"
  const std::string        Keys::deKeyName         ("DetElement-Info-IOV");
  /// Static key: 32 bit hash of "DetElement-Info-IOV". Must be unique for one DetElement
  const Keys::itemkey_type Keys::deKey            = _K::itemCode(Keys::deKeyName);
  /// Key name  of a delta condition "alignment_delta".
  const std::string        Keys::deltaName        = dd4hep::align::Keys::deltaName;
  /// Key value of a delta condition "alignment_delta".
  const Keys::itemkey_type Keys::deltaKey         = dd4hep::align::Keys::deltaKey;

  /// Static key name: "alignment"
  const std::string        Keys::alignmentKeyName = dd4hep::align::Keys::alignmentName;
  /// Static key: 32 bit hash of "alignment". Must be unique for one DetElement
  const Keys::itemkey_type Keys::alignmentKey     = dd4hep::align::Keys::alignmentKey;

  /// Static key name: "Alignments-Computed"
  const std::string        Keys::alignmentsComputedKeyName("Alignments-Computed");
  /// Static key: 32 bit hash of "Alignments-Computed". Must be unique for the world
  const Keys::key_type     Keys::alignmentsComputedKey = _K::KeyMaker(0,Keys::alignmentsComputedKeyName).hash;
}

std::string gaudi::DE::indent(int level)   {
  char fmt[128], text[1024];
  ::snprintf(fmt,sizeof(fmt),"%03d %%-%ds",level+1,2*level+1);
  ::snprintf(text,sizeof(text),fmt,"");
  return text;
}

