#include "TTimeStamp.h"

#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "DD4hep/InstanceCount.h"
#include "DD4hep/ConditionDerived.h"
#include "Detector/Common/ConditionService.h"

#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/ConditionDerived.h"

#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsSlice.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsManagerObject.h"


#include "Detector/Common/DetectorElement.h"
#include "Detector/Common/DeConditionCall.h"
#include "Detector/Common/DeAlignmentCall.h"
#include "Conditions/ConditionsRepository.h"

namespace Gaudi
{
typedef ROOT::Math::Rotation3D Rotation3D;    ///< 3D rotation
typedef ROOT::Math::EulerAngles EulerAngles;  ///< 3D Euler Angles
typedef ROOT::Math::Quaternion Quaternion;    ///< 3D Quaternion
typedef ROOT::Math::AxisAngle AxisAngle;      ///< 3D Axis Angle
typedef ROOT::Math::RotationX RotationX;      ///< Rotation about X axis
typedef ROOT::Math::RotationY RotationY;      ///< Rotation about Y axis
typedef ROOT::Math::RotationZ RotationZ;      ///< Rotation about Z axis
typedef ROOT::Math::Transform3D Transform3D;  ///< General 3D transformation (rotation+translation)
typedef ROOT::Math::XYZVector TranslationXYZ; ///< 3D translation

} // namespace Gaudi

using namespace dd4hep;
using namespace std;

void gaudi::DetectorDataService::initialize()
{
  m_manager["PoolType"] = "DD4hep_ConditionsLinearPool";
  m_manager["UserPoolType"] = "DD4hep_ConditionsMapUserPool";
  m_manager["UpdatePoolType"] = "DD4hep_ConditionsLinearUpdatePool";
  m_manager["LoaderType"] = "LHCb_ConditionsLoader";
  m_manager["OutputUnloadedConditions"] = true;
  m_manager["LoadConditions"] = true;

  m_manager.initialize();
  m_iov_typ = m_manager.registerIOVType(0, "run").second;
  if (0 == m_iov_typ)
  {
    except("ConditionsPrepare", "++ Unknown IOV type supplied.");
  }

  cond::ConditionsDataLoader &loader = m_manager.loader();
  loader["ReaderType"] = "LHCb_ConditionsFileReader";
  loader["Directory"] = "file:///workspace/DDUpgrade/Conditions/";
  loader["Match"] = "file:";
  loader["IOVType"] = "run";
  loader.initialize();

  std::set<DetElement> detectors;
  for (const auto &det : m_detectorNames)
  {
    DetElement de = m_description.detector(det);
    const CONTENT &de_conds = *de.extension<CONTENT>();
    m_all_conditions->merge(*de_conds);
    if (de.path() == "/world")
      continue;
    detectors.insert(de);
  }

  for (const auto &de : detectors)
  {
    CONTENT required(new gaudi::ConditionsRepository());
    gaudi::RequestSelector sel(*m_all_conditions, *required);
    Condition::key_type key = ConditionKey(de, gaudi::Keys::deKey).hash;
    TTimeStamp start;
    set<Condition::key_type> needed;
    needed.insert(key);
    sel.select(needed, true);
    TTimeStamp stop;
    printout(INFO, "TEST", "\tSelected %5ld [%5ld,%5ld] conditions requests for %s  [%7.5f seconds]",
             (required->conditions().size() + required->derived().size()),
             required->conditions().size(), required->derived().size(),
             de.path().c_str(), stop.AsDouble() - start.AsDouble());
  }

  /// Selection procedure: This selects all subdetector conditions specified
  CONTENT content(new gaudi::ConditionsRepository());
  gaudi::RequestSelector sel(*m_all_conditions, *content);
  std::set<Condition::key_type> needed;
  for (const auto &de : detectors)
    needed.insert(ConditionKey(de, gaudi::Keys::deKey).hash);
  needed.insert(gaudi::Keys::alignmentsComputedKey);
  sel.select(needed, true);
}

void gaudi::DetectorDataService::finalize()
{
  /// Clear it
  m_manager.clear();
  // Let's do the cleanup
  m_all_conditions.reset();
  m_manager.destroy();
}

shared_ptr<cond::ConditionsSlice> gaudi::DetectorDataService::get_slice(size_t iov)
{

  std::lock_guard<std::mutex> hold(m_cache_mutex);
  shared_ptr<cond::ConditionsSlice> sp = m_cache[iov].lock();
  if (!sp)
    m_cache[iov] = sp = load_slice(iov);
  return sp;
}

shared_ptr<cond::ConditionsSlice> gaudi::DetectorDataService::load_slice(size_t iov)
{

  IOV req_iov(m_iov_typ, iov);
  shared_ptr<cond::ConditionsSlice> slice(new cond::ConditionsSlice(m_manager, m_all_conditions));
  TTimeStamp start;

  /// Load the conditions
  ///cond::ConditionsManager::Result total = manager.prepare(req_iov,*slice);
  cond::ConditionsManager::Result total = m_manager.load(req_iov, *slice);
  TTimeStamp comp;
  total += m_manager.compute(req_iov, *slice);
  TTimeStamp stop;
  cond::ConditionsContent::Conditions &missing = slice->missingConditions();
  for (const auto &m : missing)
  {
    printout(ERROR, "TEST", "Failed to load condition [%016llX]: %s",
             m.first, m.second->toString().c_str());
  }
  printout(INFO, "Statistics",
           "+  Created/Accessed a total of %ld conditions "
           "(S:%6ld,L:%6ld,C:%6ld,M:%ld)  Load:%7.5f sec Compute:%7.5f sec",
           total.total(), total.selected, total.loaded, total.computed, total.missing,
           comp.AsDouble() - start.AsDouble(),
           stop.AsDouble() - comp.AsDouble());
  return slice;
}
