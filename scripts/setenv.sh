
# Basic config to get the DD4hep available on to of the SFT LCG 94
export CMTCONFIG=x86_64-centos7-gcc7-opt
export DD4HEP_VERSION=v01-09-48-geaf0481
#prev: v01-09-47-gf90f8b7
source /cvmfs/sft.cern.ch/lcg/views/LCG_94/${CMTCONFIG}/setup.sh
source /cvmfs/lhcbdev.cern.ch/lib/lcg/releases/LCG_94/DD4hep/${DD4HEP_VERSION}/${CMTCONFIG}/bin/thisdd4hep.sh
