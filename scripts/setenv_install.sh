#!/bin/bash
set -e

filename=$(readlink -f  $BASH_SOURCE)
dir=$(dirname $filename)
SCRIPT_LOC=$(cd  $dir;pwd)

export CMTCONFIG=x86_64-centos7-gcc7-opt
export MYSITEROOT=${SCRIPT_LOC}/../siteroot

if [[ ! -d ../env ]]; then
    virtualenv ../env
    source ../env/bin/activate
    pip install lbinstall
else
    source ../env/bin/activate
fi

RPM=$(lbinstall query LCG_94_DD4hep_v01_09 | grep ${CMTCONFIG//-/_} |  tail -1)
VERSION=$(echo ${RPM} | cut -d_ -f4-7 )
RPM2=$(lbinstall query DD4hep | grep -v LCG_94 | grep ${VERSION} | grep ${CMTCONFIG//-/_} | tail -1)
echo "Found RPM ${RPM} and ${RPM2}"

if [[ ! -d ${MYSITEROOT}/lcg/releases/LCG_94/DD4hep/${VERSION//_/-}/${CMTCONFIG} ]]; then
echo "Install DD4hep ${VERSION//_/-}"
lbinstall install --nodeps --info ${RPM} ${RPM2}
fi

source /cvmfs/sft.cern.ch/lcg/views/LCG_94/${CMTCONFIG}/setup.sh
source ${MYSITEROOT}/lcg/releases/LCG_94/DD4hep/${VERSION//_/-}/${CMTCONFIG}/bin/thisdd4hep.sh

