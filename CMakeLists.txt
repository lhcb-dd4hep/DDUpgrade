#==========================================================================
#  AIDA Detector description implementation 
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see $DD4hepINSTALL/LICENSE.
# For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
#
#==========================================================================
cmake_minimum_required(VERSION 3.3 FATAL_ERROR)

# Added to make sure the LHCb DD4hep HEAD include dirs are before the LCG view 
# one. THis is just neeed for test setup, not mandatory once we hve a clean
# production setup
set(CMAKE_INCLUDE_DIRECTORIES_BEFORE ON)

find_package(DD4hep REQUIRED DDCore DDCond)

#-----------------------------------------------------------------------------------
dd4hep_configure_output ()
dd4hep_package ( DDUpgrade MAJOR 0 MINOR 0 PATCH 1
  USES  [ROOT   REQUIRED COMPONENTS Geom] 
        [DD4hep REQUIRED COMPONENTS DDCore DDCond]
  INCLUDE_DIRS     include
  INSTALL_INCLUDES include/Detector
 )
#
#---DDDB library --------------------------------------------------------------
dd4hep_add_package_library(DDUpgradeLib
  SOURCES src_det/Common/*.cpp
  src_det/FT/*.cpp
  src_det/VP/*.cpp
  src_det/Conditions/*.cpp
  USES    [ROOT    REQUIRED COMPONENTS Geom GenVector]
          [DD4hep REQUIRED COMPONENTS DDCore DDCond]
  )
#---DDCodex plugin library -------------------------------------------------------
dd4hep_add_plugin(DDUpgradePlugins
  SOURCES src/*.cpp
  USES    [ROOT    REQUIRED COMPONENTS Gdml Geom GenVector]
)
#
#
dd4hep_install_dir(include DESTINATION ${CMAKE_INSTALL_PREFIX}/DDUpgrade )
dd4hep_install_dir(compact DESTINATION ${CMAKE_INSTALL_PREFIX}/DDUpgrade )
dd4hep_install_dir(scripts DESTINATION ${CMAKE_INSTALL_PREFIX}/DDUpgrade )
#--------------------------------------------------------------------------
#
dd4hep_configure_scripts ( DDUpgrade DEFAULT_SETUP WITH_TESTS )
#
#---Testing-------------------------------------------------------------------------
#

