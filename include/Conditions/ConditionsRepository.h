//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author  Markus Frank
//  \date    2016-02-02
//  \version 1.0
//
//==========================================================================
#ifndef DD4HEP_GAUDI_CONDITONSREPOSITORY_H
#define DD4HEP_GAUDI_CONDITONSREPOSITORY_H

// Framework include files
#include "DDCond/ConditionsContent.h"
#include "Conditions/ConditionIdentifier.h"

/// Namespace for the Gaudi framework
namespace gaudi {

  /// Forward declarations
  class ConditionIdentifier;
  class RequestSelector;
  
  /// 
  /** 
   *  \author   M.Frank
   *  \version  1.0
   *  \ingroup  DD4HEP_GAUDI
   */
  class ConditionsRepository : public dd4hep::cond::ConditionsContent  {
    friend class gaudi::RequestSelector;
  public:
    std::map<dd4hep::Condition::key_type, ConditionIdentifier*> locations;
  private:
    /// Default assignment operator
    ConditionsRepository& operator=(const ConditionsRepository& copy) = delete;
    /// Copy constructor
    ConditionsRepository(const ConditionsRepository& copy) = delete;

  public:
    /// Default constructor
    ConditionsRepository() = default;
    /// Default destructor.
    virtual ~ConditionsRepository() = default;
    operator ConditionsContent& () { return *this; }
    /// Access to the real condition entries to be loaded (CONST)
    const Conditions& conditions() const  { return m_conditions;   }
    /// Access to the derived condition entries to be computed (CONST)
    const Dependencies& derived() const   { return m_derived;      }
    /// Merge the content of "to_add" into the this content
    void merge(const ConditionsRepository& to_add);
    /// Clear the container. Destroys the contained stuff
    void clear();
    std::pair<dd4hep::Condition::key_type, dd4hep::cond::ConditionsLoadInfo*>
    addLocation(dd4hep::DetElement de,
                dd4hep::Condition::itemkey_type item,
                const std::string& sys_id,
                const std::string& object);
    std::pair<dd4hep::Condition::key_type, dd4hep::cond::ConditionsLoadInfo*>
    addGlobal(dd4hep::DetElement de,
              dd4hep::Condition::itemkey_type item,
              const std::string& sys_id,
              const std::string& object);
    /// Add a new shared conditions dependency
    std::pair<dd4hep::Condition::key_type,dd4hep::cond::ConditionDependency*>
    addDependency(dd4hep::cond::ConditionDependency* dep);
    /// Add a new conditions dependency (Built internally from arguments)
    std::pair<dd4hep::Condition::key_type, dd4hep::cond::ConditionDependency*>
    addDependency(dd4hep::DetElement de,
                  dd4hep::Condition::itemkey_type item,
                  dd4hep::cond::ConditionUpdateCall* callback);
    /// Add a new global conditions dependency (Built internally from arguments)
    std::pair<dd4hep::Condition::key_type, dd4hep::cond::ConditionDependency*>
    addGlobal(dd4hep::DetElement de,
              dd4hep::Condition::itemkey_type item,
              dd4hep::cond::ConditionUpdateCall* callback);
    /// Add a new global conditions dependency (Built internally from arguments)
    std::pair<dd4hep::Condition::key_type, dd4hep::cond::ConditionDependency*>
    addGlobal(dd4hep::cond::ConditionDependency* dep);
  };
}      /* End namespace dd4hep                    */
#endif /* DD4HEP_GAUDI_CONDITONSREPOSITORY_H      */
