//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEVP_H 
#define DETECTOR_DEVP_H 1

// Framework include files
#include "Detector/VP/DeVPSensor.h"
#include "Detector/VP/DeVPGeneric.h"

#define DE_VP_TYPEDEFS(TYP) public:                       \
  DE_CONDITIONS_TYPEDEFS;                                 \
  typedef DeVPSensor##TYP               Sensor;           \
  typedef DeVPGeneric##TYP              Side;             \
  typedef std::vector<DeVPSensor##TYP>  Sensors;          \
  typedef std::vector<DeVPGeneric##TYP> Sides;            \
  typedef std::vector<DeVPGeneric##TYP> Modules;          \
  typedef std::vector<DeVPGeneric##TYP> Assemblies
  
/// Gaudi namespace declaration
namespace gaudi   {

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// VP  detector element data
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeVPStaticObject : public DeVPGenericStaticObject  {
      DE_VP_TYPEDEFS(Static);

    public:
      enum { typeID = 8200 };
      /// Left/right side
      Side           left, right;
      /// The two sides (left/right or A/C)
      Sides          sides;
      /// The modules with support attached
      Modules modules;
      /// The assemblies contained in the VP (sort of artificial: 1-1 correspondance between module-ladder-sensor)
      Assemblies     assemblies;

      double         sensitiveVolumeCut = 0e0;
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeVPStaticObject);
      /// Printout method to stdout
      virtual void print(int indent, int flags)  const;
    };
  }    // End namespace detail


  /// Handle defintiion to an instance of VP  detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeVPStaticElement : public dd4hep::Handle<detail::DeVPStaticObject>   {
    DE_VP_TYPEDEFS(Static);
    typedef Object static_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeVPStaticElement,Base);
    /// Return the number of modules.
    size_t numberModules() const               { return ptr()->modules.size();     }
    /// Return vector of modules.
    const Modules& modules() const             { return ptr()->modules;            }
    /// Return the number of modules.
    size_t numberAssemblies() const            { return ptr()->assemblies.size();  }
    /// Return vector of assemblies.
    const Assemblies& assemblies() const       { return ptr()->assemblies;         }
    /// Return the number of sensors.
    size_t numberSensors() const               { return ptr()->sensors.size();     }
    /// Return vector of sensors.
    const Sensors& sensors() const             { return ptr()->sensors;            }
  };

  /// For the fully enabled object, we have to combine it with the generic stuff
  typedef  DetectorStaticElement<DeVPStaticElement>  DeVPStatic;

  /// Gaudi::detail namespace declaration
  namespace detail   {
  
    /// VP  detector element data
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeVPObject : public DeVPGenericObject  {
      DE_VP_TYPEDEFS();
      typedef DeVPStatic::Object static_t;
      enum { typeID = static_t::typeID };

    public:
      /// Handle to the static information 
      DeVPStatic     vp_static;
      /// Left/right side
      Side           left, right;
      /// The two sides (left/right or A/C)
      Sides          sides;
      /// The modules with support attached
      Modules        modules;
      /// The assemblies contained in the VP (sort of artificial: 1-1 correspondance between module-ladder-sensor)
      Assemblies     assemblies;

    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeVPObject);
      /// Printout method to stdout
      void print(int indent, int flags)  const ;
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of VP  detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeVPElement : public dd4hep::Handle<detail::DeVPObject>   {
    DE_VP_TYPEDEFS();
    typedef Object::static_t static_t;
    typedef Object           iov_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeVPElement,Base);
    /// Access to the static data
    static_t& staticData()  const              { return access()->vp_static;       }
    /// Return the number of modules.
    size_t numberModules() const               { return ptr()->modules.size();     }
    /// Return vector of modules. 
    const Modules& modules() const             { return ptr()->modules;            }
    /// Return the number of modules.
    size_t numberAssemblies() const            { return ptr()->assemblies.size();  }
    /// Return vector of assemblies.
    const Assemblies& assemblies() const       { return ptr()->assemblies;         }
    /// Return the number of sensors.
    size_t numberSensors() const               { return ptr()->sensors.size();     }
    /// Return vector of sensors.
    const Sensors& sensors() const             { return ptr()->sensors;            }
  };

  /// For the fully enabled object, we have to combine it with the generic stuff
  typedef  DetectorElement<DeVPElement>  DeVP;
  
}      // End namespace gaudi
#endif // DETECTOR_DEVP_H
