//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEVPCONDITIONCALLS_H 
#define DETECTOR_DEVPCONDITIONCALLS_H 1

// Framework include files
#include "Detector/VP/DeVP.h"
#include "Detector/Common/DeConditionCall.h"
#include "DD4hep/ConditionDerived.h"

/// Gaudi namespace declaration
namespace gaudi   {

  /// Condition derivation call to build the static top level VP DetElement condition information
  /**
   *
   *  \author  Markus Frank
   *  \date    2018-11-12
   *  \version  1.0
   */
  template <typename T> class DeVPConditionCall : public DeConditionCall  {
  public:
    /// Inherit constructors
    using DeConditionCall::DeConditionCall;
    virtual ~DeVPConditionCall() {}
    /// Interface to client Callback in order to update the condition
    virtual Condition operator()(const ConditionKey& key, Context& context) override final;
    /// Interface to client callback for resolving references or to use data from other conditions
    virtual void resolve(Condition c, Context& context)  override final;
  };
}      // End namespace gaudi
#endif // DETECTOR_DEVPCONDITIONCALLS_H
