//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DECONDITIONCALL_H 
#define DETECTOR_DECONDITIONCALL_H 1

// Framework include files
#include "DD4hep/DetElement.h"
#include "DD4hep/ConditionDerived.h"
#include "Conditions/ConditionsRepository.h"
#include "Detector/Common/DetectorElement.h"

// C/C++ include files
#include <memory>
#include <unordered_map>


#include "Conditions/ConditionsRepository.h"

/// Gaudi namespace declaration
namespace gaudi   {

  /// Selector to determine which conditions are mandatory for event processing
  /**
   *
   *  \author  Markus Frank
   *  \date    2018-11-30
   *  \version  1.0
   */
  class RequestSelector   {
  public:
    const ConditionsRepository& existing;
    ConditionsRepository&       required;
  public:
    /// Inhibit default constructor
    RequestSelector() = delete;
    /// Inhibit move constructor
    RequestSelector(RequestSelector&& copy) = delete;
    /// Inhibit copy constructor
    RequestSelector(const RequestSelector& copy) = delete;
    /// Initializing constructor
    RequestSelector(const ConditionsRepository& e,ConditionsRepository& r)
      : existing(e), required(r) {
    }
    /// Inhibit assignment
    RequestSelector& operator=(const RequestSelector& copy) = delete;
    /// Selector call for single condition
    void select(dd4hep::Condition::key_type key);
    /// Selector call for condition groups
    void select(std::set<dd4hep::Condition::key_type>& needed, bool clr);
    /// Selector call for condition groups
    void select(std::vector<dd4hep::Condition::key_type>& needed, bool clr);
  };

  /// Base class to share common type definitions
  /**
   *
   *  \author  Markus Frank
   *  \date    2018-11-30
   *  \version  1.0
   */
  class DeConditionCallDefs {
  public:
    typedef dd4hep::DetElement                   DetElement;
    typedef dd4hep::Condition                    Condition;
    typedef Condition::key_type                  key_type;
    typedef dd4hep::ConditionKey                 ConditionKey;
    typedef dd4hep::ConditionKey::KeyMaker       KeyMaker;
    typedef dd4hep::cond::ConditionUpdateCall    ConditionUpdateCall;
    typedef dd4hep::cond::ConditionDependency    ConditionDependency;
    typedef dd4hep::cond::ConditionResolver      Resolver;
    typedef dd4hep::cond::ConditionUpdateContext Context;
  };

  
  /// Base class for detector element conditions callbacks
  /**
   *
   *  \author  Markus Frank
   *  \date    2018-11-30
   *  \version  1.0
   */
  class DeConditionCall : public dd4hep::cond::ConditionUpdateCall, public DeConditionCallDefs  {
  public:
    /* Do not use shared pointers here: There is a circular dependency between the 
     * repository and the callback and the net result is that nothing at all is deleted.
     */
    /// Reference to the inventory of subdetector conditions
    ConditionsRepository* repository = 0;
  public:
    /// Default constructor
    DeConditionCall() = delete;
    /// Copy constructor
    DeConditionCall(const DeConditionCall& copy) = delete;
    /// Initializing constructor
    DeConditionCall(std::shared_ptr<ConditionsRepository>& rep);
    /// Default destructor
    virtual ~DeConditionCall();
    /// Access the conditions derivation given the condiitons key using the repository
    const dd4hep::cond::ConditionDependency* derivationInfo(key_type key)   const;
    /// Access the detector element given the condiitons key using the repository
    DetElement detectorElement(key_type key)   const;
  };

  /// Condition derivation call to build the static top level VP DetElement condition information
  /**
   *
   *  \author  Markus Frank
   *  \date    2018-11-12
   *  \version  1.0
   */
  class DeDefaultDeltaCall : public DeConditionCall  {
  public:
    /// Inherit constructors
    using DeConditionCall::DeConditionCall;
    virtual ~DeDefaultDeltaCall() {}
    /// Interface to client Callback in order to update the condition
    virtual Condition operator()(const ConditionKey& key, Context& context) override final;
    /// Interface to client callback for resolving references or to use data from other conditions
    virtual void resolve(Condition, Context&)  override final {}
  };
  
  namespace detail {
    // This is a standard implementation to resolve "simple" detector element callbacks.
    template <typename IOV>
    void default_iov_resolve(const IOV& iov, DeConditionCallDefs::Context& ctxt)    {
      ctxt.condition(Keys::alignmentsComputedKey);
      iov->resolve(iov->detector, ctxt);
      iov->concrete_static = iov->de_static;
      iov->de_user         = iov->de_static->de_user;
    }
  }
  
}      // End namespace gaudi
#endif // DETECTOR_DECONDITIONCALL_H
