#ifndef DETECTOR_COMMON_CONDSERVICE_H
#define DETECTOR_COMMON_CONDSERVICE_H

#include <memory>
#include <map>
#include <vector>

#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsSlice.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsManagerObject.h"
#include "Conditions/ConditionsRepository.h"

/// gaudi namespace declaration
namespace gaudi
{

using CONTENT = std::shared_ptr<gaudi::ConditionsRepository>;

class DetectorDataService
{
public:
  DetectorDataService(dd4hep::Detector &description, std::vector<std::string> detectorNames) : m_description(description),
                                                                                               m_manager(description, "DD4hep_ConditionsManager_Type1"), m_detectorNames(detectorNames) {}

  void initialize();

  void finalize();

  std::shared_ptr<dd4hep::cond::ConditionsSlice> get_slice(size_t iov);

  std::shared_ptr<dd4hep::cond::ConditionsSlice> load_slice(size_t iov);

protected:
  dd4hep::Detector &m_description;
  dd4hep::cond::ConditionsManager m_manager;
  std::vector<std::string> m_detectorNames;
  const dd4hep::IOVType *m_iov_typ = nullptr;
  CONTENT m_all_conditions{new gaudi::ConditionsRepository()};
  std::map<size_t, std::weak_ptr<dd4hep::cond::ConditionsSlice>> m_cache;
  std::mutex m_cache_mutex;
};


const std::type_info &get_condition_type(const dd4hep::Condition &condition)
{
  if (condition.is_bound())
    return condition.typeInfo();
  else
    return typeid(*condition);
}

} // End namespace gaudi
#endif // DETECTOR_COMMON_CONDSERVICE_H
