//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEFTGENERIC_H 
#define DETECTOR_DEFTGENERIC_H 1

// Framework include files
#include "Detector/FT/DeFTMat.h"
#include "Detector/Common/DeStatic.h"
#include "Detector/Common/DeIOV.h"

/// Gaudi namespace declaration
namespace gaudi   {

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// FT  detector element data
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTGenericStaticObject : public DeStaticObject    {
      DE_CONDITIONS_TYPEDEFS;

    public:
      typedef  std::vector<DeFTMatStatic>  Mats;
      Mats  mats;

    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTGenericStaticObject);
      /// Initialization of sub-classes
      virtual void initialize()  override;
      /// Printout method to stdout
      virtual void print(int indent, int flg)  const  override;
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT  detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTGenericStaticElement
    : public dd4hep::Handle<detail::DeFTGenericStaticObject>
  {
    DE_CONDITIONS_TYPEDEFS;
    typedef Object           static_t;
    typedef Object::Mats  Mats;
  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTGenericStaticElement,Base);
    /// Export access to the mats from the detector element
    Object::Mats& mats()  const   {   return access()->mats;    }
  };
  /// For the fully enabled object, we have to combine it with the generic stuff
  typedef  DetectorStaticElement<DeFTGenericStaticElement>  DeFTGenericStatic;

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// FT  detector element data
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTGenericObject : public DeIOVObject    {
      DE_CONDITIONS_TYPEDEFS;

    public:
      typedef  std::vector<DeFTMat>  Mats;
      Mats  mats;

    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTGenericObject);
      /// Initialization of sub-classes
      virtual void initialize()  override;
      /// Printout method to stdout
      virtual void print(int indent, int flg)  const  override;
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT  detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTGenericElement : public dd4hep::Handle<detail::DeFTGenericObject>   {
    DE_CONDITIONS_TYPEDEFS;
    /// These two are needed by the DetectorElement<TYPE> to properly forward requests.
    typedef Object           iov_t;
    typedef Object::static_t static_t;
    typedef Object::Mats     Mats;
    
  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTGenericElement,Base);
    /// Access to the static data
    static_t& staticData()  const    { return access()->de_static;   }
    /// Export access to the mats from the detector element
    Object::Mats& mats()  const      {   return access()->mats;   }
  };
  
  /// For the fully enabled object, we have to combine it with the generic stuff
  typedef  DetectorElement<DeFTGenericElement>  DeFTGeneric;

}      // End namespace gaudi
#endif // DETECTOR_DEFTGENERIC_H
