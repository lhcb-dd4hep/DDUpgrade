//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-11-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEFT_H 
#define DETECTOR_DEFT_H 1

// Framework include files
#include "Detector/FT/DeFTStation.h"

#define DE_FT_TYPEDEFS(TYP) public:                       \
  DE_CONDITIONS_TYPEDEFS                              
  
/// Gaudi namespace declaration
namespace gaudi   {

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// FT  detector element data
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-11-08
     *  \version  1.0
     */
    class DeFTStaticObject : public DeStaticObject  {
      DE_FT_TYPEDEFS(Static);

    public:
      enum { type = FT::LAYER };
      int            version    = 64;        // FT Geometry Version
      int            nModulesT1 = 0;         // Number of modules in T1
      int            nModulesT2 = 0;         // Number of modules in T2
      int            nModulesT3 = 0;         // Number of modules in T3
      int            nLayers    = 0;         // Number of layers per station
      int            nQuarters  = 0;         // Number of quarters per layer
      int            nChannelsInModule = 0;  // Number of channels per SiPM
      std::array<DeFTStationStatic,3> stations;

    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTStaticObject);
    };
  }    // End namespace detail


  /// Handle defintiion to an instance of FT  detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-11-08
   *  \version 1.0
   */
  class DeFTStaticElement : public dd4hep::Handle<detail::DeFTStaticObject>   {
    DE_FT_TYPEDEFS(Static);
    typedef Object static_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTStaticElement,Base);
  };

  /// For the fully enabled object, we have to combine it with the generic stuff
  typedef  DetectorStaticElement<DeFTStaticElement>  DeFTStatic;

  /// Gaudi::detail namespace declaration
  namespace detail   {
  
    /// FT  detector element data
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-11-08
     *  \version  1.0
     */
    class DeFTObject : public DeIOVObject  {
      DE_FT_TYPEDEFS();
      typedef DeFTStatic::Object static_t;

    public:
      enum { type = static_t::type };
      DeFTStatic                concrete_static;
      std::array<DeFTStation,3> stations;
      
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTObject);
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT  detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-11-08
   *  \version 1.0
   */
  class DeFTElement : public dd4hep::Handle<detail::DeFTObject>   {
    DE_FT_TYPEDEFS();
    typedef Object::static_t static_t;
    typedef Object           iov_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTElement,Base);
    /// Access to the static data
    static_t& staticData()  const  { return access()->concrete_static;       }
    /// FT Geometry Version
    int version() const            { return staticData().version;            }
    /// Number of modules in T1
    int nModulesT1() const         { return staticData().nModulesT1;         }
    /// Number of modules in T2
    int nModulesT2() const         { return staticData().nModulesT2;         }
    /// Number of modules in T3
    int nModulesT3() const         { return staticData().nModulesT3;         }
    /// Number of layers per station
    int nLayers() const            { return staticData().nLayers;            }
    /// Number of quarters per layer
    int nQuarters() const          { return staticData().nQuarters;          }
    /// Number of channels per SiPM
    int nChannelsInModule() const  { return staticData().nChannelsInModule;  }
    /// Access to the station T1
    DeFTStation t1()  const        { return access()->stations[0];           }
    /// Access to the station T2
    DeFTStation t2()  const        { return access()->stations[1];           }
    /// Access to the station T3
    DeFTStation t3()  const        { return access()->stations[2];           }
  };
  /// For the fully enabled object, we have to combine it with the generic stuff
  typedef  DetectorElement<DeFTElement>  DeFT;
  
}      // End namespace gaudi
#endif // DETECTOR_DEFT_H
