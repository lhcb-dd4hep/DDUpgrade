//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEFTSTATION_H 
#define DETECTOR_DEFTSTATION_H 1

// C/C++ include files

// Framework include files
#include "Detector/FT/DeFTLayer.h"

/// Gaudi namespace declaration
namespace gaudi   {

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT static detector element 
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTStationStaticObject : public detail::DeStaticObject  {
    public:
      enum { type = FT::STATION };
      /// Reference to the static information of the layers
      std::array<DeFTLayerStatic,4> layers;
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTStationStaticObject);
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT static detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTStationStaticElement
    : public dd4hep::Handle<detail::DeFTStationStaticObject>
  {
    DE_CONDITIONS_TYPEDEFS;
    typedef Object static_t;    /// This is needed to properly forward requests.
  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTStationStaticElement,Base);
  };
  /// For the full station object, we have to combine it with the geometry stuff:
  typedef  DetectorStaticElement<DeFTStationStaticElement>  DeFTStationStatic;
  
  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT iov dependent detector element of a FT station
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTStationObject : public DeIOVObject  {
      DE_CONDITIONS_TYPEDEFS;
      typedef DeFTStationStatic::Object static_t;
      enum { type = FT::STATION };

      /// The static part of the detector element
      DeFTStationStatic concrete_static;
      /// Reference to the static information of the layers
      std::array<DeFTLayer,4> layers;
      /// Reference to parent element
      DeIOV parent;
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTStationObject);
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT IOV dependent data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTStationElement : public dd4hep::Handle<detail::DeFTStationObject>   {
    DE_CONDITIONS_TYPEDEFS;
    typedef Object::static_t static_t;
    typedef Object           iov_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTStationElement,Base);
    /// Access to the static data. Does this need to be optionized???
    static_t& staticData()  const  {  return access()->concrete_static;  }
    /// Station identifier
    int       stationID() const    {  return staticData().id;            }
    /// Access the layer array
    const std::array<DeFTLayer,4>& layers() const
    {   return access()->layers;                                         }
    /// Reference to the static information of layer X1
    DeFTLayer layerX1() const {  return access()->layers[0];             }
    /// Reference to the static information of layer U
    DeFTLayer layerU() const  {  return access()->layers[1];             }
    /// Reference to the static information of layer V
    DeFTLayer layerV() const  {  return access()->layers[2];             }
    /// Reference to the static information of layer X2
    DeFTLayer layerX2() const {  return access()->layers[3];             }
  };
  /// For the full station object, we have to combine it with the geometry stuff:
  typedef  DetectorElement<DeFTStationElement>  DeFTStation;

}      // End namespace gaudi
#endif // DETECTOR_DEFTSTATION_H
