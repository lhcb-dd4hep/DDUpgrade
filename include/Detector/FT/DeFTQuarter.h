//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEFTQUARTER_H 
#define DETECTOR_DEFTQUARTER_H 1

// Framework include files
#include "Detector/FT/DeFTModule.h"

/// Gaudi namespace declaration
namespace gaudi   {

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT static detector element 
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTQuarterStaticObject : public detail::DeStaticObject  {
    public:
      enum { type = FT::QUARTER };
      /// Reference to the static information of modules
      std::vector<DeFTModuleStatic>  modules;
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTQuarterStaticObject);
      
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT static detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTQuarterStaticElement
    : public dd4hep::Handle<detail::DeFTQuarterStaticObject>
  {
    DE_CONDITIONS_TYPEDEFS;
    /// This is needed by the DetectorElement<TYPE> to properly forward requests.
    typedef Object static_t;
  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTQuarterStaticElement,Base);
    /// The number of modules: 5 or 6
    size_t numModules()  const  {  return access()->modules.size();        }
  };
  /// For the full quarter object, we have to combine it with the geometry stuff:
  typedef  DetectorStaticElement<DeFTQuarterStaticElement>  DeFTQuarterStatic;


  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT iov dependent detector element of a FT quarter
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTQuarterObject : public DeIOVObject  {
      DE_CONDITIONS_TYPEDEFS;
      typedef DeFTQuarterStatic::Object static_t;
      enum { type = FT::QUARTER };

      /// Reference to the static information of modules
      std::vector<DeFTModule>  modules;
      /// The static part of the detector element
      DeFTQuarterStatic concrete_static;
      /// Reference to parent element
      DeIOV parent;

    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTQuarterObject);
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT IOV dependent data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTQuarterElement : public dd4hep::Handle<detail::DeFTQuarterObject>   {
    DE_CONDITIONS_TYPEDEFS;
    typedef Object::static_t static_t;
    typedef Object           iov_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTQuarterElement,Base);
    /// Access to the static data. Does this need to be optionized???
    static_t& staticData()  const  {  return access()->concrete_static;    }
    /// Access the quarter ID
    int quarterID() const          {  return staticData().id;              }
    /// The number of modules: 5 or 6
    size_t numModules()  const  {  return access()->modules.size();        }
    /// Reference to the static information of module 0
    DeFTModule module0()  const {  return access()->modules[0];            }
    /// Reference to the static information of module 0
    DeFTModule module1()  const {  return access()->modules[1];            }
    /// Reference to the static information of module 0
    DeFTModule module2()  const {  return access()->modules[2];            }
    /// Reference to the static information of module 0
    DeFTModule module3()  const {  return access()->modules[3];            }
    /// Reference to the static information of module 0
    DeFTModule module4()  const {  return access()->modules[4];            }
    /// Reference to the static information of module 0
    DeFTModule module5()  const {  return access()->modules[5];            }
  };

  /// For the full quarter object, we have to combine it with the geometry stuff:
  typedef  DetectorElement<DeFTQuarterElement>  DeFTQuarter;

}      // End namespace gaudi
#endif // DETECTOR_DEFTQUARTER_H
