//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_FT_FTCONSTANTS_H 
#define DETECTOR_FT_FTCONSTANTS_H 1

// C/C++ include files

// Framework include files

/// Gaudi namespace declaration
namespace gaudi   {
  namespace FT  {
    enum DetElementTypes  {
      TOP     = 1<<0,
      STATION = 1<<1,
      LAYER   = 1<<2,
      QUARTER = 1<<3,
      MODULE  = 1<<4,
      MAT     = 1<<5
    };
  }

}      // End namespace gaudi
#endif // DETECTOR_FT_FTCONSTANTS_H
