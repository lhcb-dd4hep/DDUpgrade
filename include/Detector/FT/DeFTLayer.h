//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEFTLAYER_H 
#define DETECTOR_DEFTLAYER_H 1

// C/C++ include files
#include <array>

// Framework include files
#include "Detector/FT/DeFTQuarter.h"

/// Gaudi namespace declaration
namespace gaudi   {

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT static detector element 
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTLayerStaticObject : public detail::DeStaticObject  {
    public:
      enum { type = FT::LAYER };
      /// Reference to the static information of the quarters
      std::array<DeFTQuarterStatic, 4> quarters;
      /// stereo angle
      double                           stereoAngle = 0;
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTLayerStaticObject);
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT static detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTLayerStaticElement
    : public dd4hep::Handle<detail::DeFTLayerStaticObject>
  {
    DE_CONDITIONS_TYPEDEFS;
    /// This is needed by the DetectorElement<TYPE> to properly forward requests.
    typedef Object static_t;
  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTLayerStaticElement,Base);
  };
  
  /// For the full layer object, we have to combine it with the geometry stuff:
  typedef  DetectorStaticElement<DeFTLayerStaticElement>  DeFTLayerStatic;

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT iov dependent detector element of a FT layer
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTLayerObject : public DeIOVObject  {
      DE_CONDITIONS_TYPEDEFS;
      typedef DeFTLayerStatic::Object static_t;

      enum { type = FT::LAYER };
      /// The static part of the detector element
      DeFTLayerStatic concrete_static;
      /// Reference to the static information of the quarters
      std::array<DeFTQuarter, 4> quarters;
      /// Reference to parent element
      DeIOV parent;

    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTLayerObject);
    };
  }    // End namespace detail

  
  /// Handle defintiion to an instance of FT IOV dependent data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTLayerElement : public dd4hep::Handle<detail::DeFTLayerObject>   {
    DE_CONDITIONS_TYPEDEFS;
    typedef Object::static_t static_t;
    typedef Object           iov_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTLayerElement,Base);
    /// Access to the static data. Does this need to be optionized???
    static_t& staticData()  const  {  return access()->concrete_static;  }
    /// layer identifier
    int layerID() const            {  return staticData().id;            }
    /// Reference to the static information of quarter 0
    DeFTQuarter quarter0()   const {  return access()->quarters[0];      }
    /// Reference to the static information of quarter 1
    DeFTQuarter quarter1()   const {  return access()->quarters[0];      }
    /// Reference to the static information of quarter 2
    DeFTQuarter quarter2()   const {  return access()->quarters[0];      }
    /// Reference to the static information of quarter 3
    DeFTQuarter quarter3()   const {  return access()->quarters[0];      }
  };

  /// For the full layer object, we have to combine it with the geometry stuff:
  typedef  DetectorElement<DeFTLayerElement>  DeFTLayer;

}      // End namespace gaudi
#endif // DETECTOR_DEFTLAYER_H
