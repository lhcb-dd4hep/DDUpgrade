//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEFTMODULE_H 
#define DETECTOR_DEFTMODULE_H 1

// C/C++ include files
#include <array>

// Framework include files
#include "Detector/FT/DeFTMat.h"

/// Gaudi namespace declaration
namespace gaudi   {

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT static detector element 
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTModuleStaticObject : public DeStaticObject  {
    public:
      enum { type = FT::MODULE };
      /// Reference to the static information of mats
      std::array<DeFTMatStatic,4> mats;
      /// Number of channels in the module
      int                         nChannelsInModule = 0;
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTModuleStaticObject);
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT static detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTModuleStaticElement
    : public dd4hep::Handle<detail::DeFTModuleStaticObject>
  {
    DE_CONDITIONS_TYPEDEFS;
    /// This is needed by the DetectorElement<TYPE> to properly forward requests.
    typedef Object static_t;
  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTModuleStaticElement,Base);
  };
  /// For the full module object, we have to combine it with the geometry stuff:
  typedef  DetectorStaticElement<DeFTModuleStaticElement>  DeFTModuleStatic;

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT iov dependent detector element of type FT module
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTModuleObject : public DeIOVObject  {
      DE_CONDITIONS_TYPEDEFS;
      typedef DeFTModuleStatic::Object static_t;
      enum { type = FT::MODULE };

      /// The static part of the detector element
      DeFTModuleStatic      concrete_static;
      /// Reference to the static information of mats
      std::array<DeFTMat,4> mats;
      /// Reference to parent element
      DeIOV                 parent;

    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTModuleObject);
    };
  }    // End namespace detail
  
  /// Handle defintiion to an instance of FT IOV dependent data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTModuleElement : public dd4hep::Handle<detail::DeFTModuleObject>   {
    DE_CONDITIONS_TYPEDEFS;
    typedef Object::static_t static_t;
    typedef Object           iov_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTModuleElement,Base);
    /// Access to the static data. Does this need to be optionized???
    static_t& staticData()  const  {  return access()->concrete_static;  }
    /// Access the module ID
    int moduleID() const         { return staticData().id;               }
    /// Number of channels in the module
    int nChannelsInModule() const{ return staticData().nChannelsInModule;}
    
    /// Reference to the static information of mat 0
    const std::array<DeFTMat,4>& mats()  const {  return access()->mats; }
    /// Reference to the static information of mat 0
    DeFTMat    mat0()  const  { return access()->mats[0];           }
    /// Reference to the static information of mat 1
    DeFTMat    mat1()  const  { return access()->mats[1];           }
    /// Reference to the static information of mat 2
    DeFTMat    mat2()  const  { return access()->mats[2];           }
    /// Reference to the static information of mat 3
    DeFTMat    mat3()  const  { return access()->mats[3];           }
  };
  /// For the full module object, we have to combine it with the geometry stuff:
  typedef  DetectorElement<DeFTModuleElement>  DeFTModule;

}      // End namespace gaudi
#endif // DETECTOR_DEFTMODULE_H
