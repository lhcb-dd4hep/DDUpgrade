//==============================================================================
//  AIDA Detector description implementation for LHCb
//------------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author   Markus Frank
//  \date     2018-03-08
//  \version  1.0
//
//==============================================================================
#ifndef DETECTOR_DEFTMAT_H 
#define DETECTOR_DEFTMAT_H 1

// C/C++ include files
#include <array>
#include <vector>

// Framework include files
#include "Detector/FT/FTConstants.h"
#include "Detector/Common/DetectorElement.h"

/// Gaudi namespace declaration
namespace gaudi   {

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT static detector element 
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTMatStaticObject : public detail::DeStaticObject  {
    public:
      enum { type = FT::MAT };
      double airGap           = 0e0;
      double deadRegion       = 0e0;
      double channelPitch     = 0e0;
      double dieGap           = 0e0;
      int    nChannelsInSiPM  = 0;
      int    nSiPMsInMat      = 0;
      int    nDiesInSiPM      = 0;
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTMatStaticObject);
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT static detector element data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTMatStaticElement
    : public dd4hep::Handle<detail::DeFTMatStaticObject>
  {
    DE_CONDITIONS_TYPEDEFS;
    /// This is needed by the DetectorElement<TYPE> to properly forward requests.
    typedef Object static_t;
  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTMatStaticElement,Base);
  };
  /// For the full mat object, we have to combine it with the geometry stuff:
  typedef  DetectorStaticElement<DeFTMatStaticElement>  DeFTMatStatic;

  /// Gaudi::detail namespace declaration
  namespace detail   {

    /// Generic FT iov dependent detector element 
    /**
     *
     *  \author  Markus Frank
     *  \date    2018-03-08
     *  \version  1.0
     */
    class DeFTMatObject : public DeIOVObject  {
      DE_CONDITIONS_TYPEDEFS;
      typedef DeFTMatStaticObject static_t;
      enum { type = FT::MAT };
      /// The static part of the detector element
      DeFTMatStatic concrete_static;
      /// Reference to parent element
      DeIOV parent;
    public:
      /// Standard constructors and assignment
      DE_CTORS_DEFAULT(DeFTMatObject);
    };
  }    // End namespace detail

  /// Handle defintiion to an instance of FT IOV dependent data
  /**
   *  This object defines the behaviour of the objects's data
   *
   *  \author  Markus Frank
   *  \date    2018-03-08
   *  \version  1.0
   */
  class DeFTMatElement : public dd4hep::Handle<detail::DeFTMatObject>   {
    DE_CONDITIONS_TYPEDEFS;
    typedef Object::static_t static_t;
    typedef Object           iov_t;

  public:
    /// Standard constructors and assignment
    DE_CTORS_HANDLE(DeFTMatElement,Base);
    /// Access to the static data. Does this need to be optiomized???
    static_t& staticData()  const    {  return access()->concrete_static;  }
    int    matID()  const            {  return staticData().id;            }
    double airGap()  const           {  return staticData().airGap;        }
    double deadRegion()  const       {  return staticData().deadRegion;    }
    double channelPitch()  const     {  return staticData().channelPitch;  }
    double dieGap()  const           {  return staticData().dieGap;        }
    int    nChannelsInSiPM()  const  {  return staticData().dieGap;        }
    int    nSiPMsInMat()  const      {  return staticData().nSiPMsInMat;   }
    int    nDiesInSiPM()  const      {  return staticData().nDiesInSiPM;   }
  };
  /// For the full mat object, we have to combine it with the geometry stuff:
  typedef  DetectorElement<DeFTMatElement>  DeFTMat;

}      // End namespace gaudi
#endif // DETECTOR_DEFTMATIOV_H
