//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================
//
// Specialized generic detector constructor
// 
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "DD4hep/InstanceCount.h"
#include "DD4hep/ConditionDerived.h"
#include "DDCond/ConditionsSlice.h"
#include "DDCond/ConditionsContent.h"
#include "Detector/Common/DetectorElement.h"
#include "Detector/Common/DeConditionCall.h"
#include "Detector/Common/DeAlignmentCall.h"
#include "Conditions/ConditionsRepository.h"
#include "TTimeStamp.h"

#include <memory>

using namespace std;
using namespace dd4hep;

static long test_conditions_recipes(Detector& description, int argc, char** argv)  {
  typedef shared_ptr<gaudi::ConditionsRepository> CONTENT;
  struct Dumper   {
    Detector& description;
    Dumper(Detector& desc) : description(desc) {
    }
    void dump(int level,
              gaudi::RequestSelector& sel,
              DetElement de,
              std::set<Condition::key_type>& needed)   const
    {
      Condition::key_type key = ConditionKey(de,gaudi::Keys::deKey).hash;
      TTimeStamp start;
      for( const auto& c : de.children() )
        dump(level+1, sel, c.second, needed);
      size_t num_req_cond    = sel.required.conditions().size();
      size_t num_req_derived = sel.required.derived().size();
      // Do the key intersection .... this is the result:
      needed.insert(key);
      sel.select(needed, false);
      TTimeStamp stop;
      printout(INFO,"TEST","Selected %2ld c:%2ld d:%2ld] total:%5ld c:%5ld d:%5ld conditions for %s [%7.5f seconds]",
               (sel.required.conditions().size()+sel.required.derived().size()-num_req_cond-num_req_derived),
               sel.required.conditions().size()-num_req_cond,sel.required.derived().size()-num_req_derived,
               (sel.required.conditions().size()+sel.required.derived().size()),
               sel.required.conditions().size(),sel.required.derived().size(),
               de.path().c_str(), stop.AsDouble()-start.AsDouble());
    }
    void operator()(const string& det_name)  const  {
      DetElement de = description.detector(det_name);
      CONTENT                  existing = *de.extension<CONTENT>();
      CONTENT                  required(new gaudi::ConditionsRepository());
      gaudi::RequestSelector    sel(*existing, *required);
      Condition::key_type      key = ConditionKey(de,gaudi::Keys::deKey).hash;
      set<Condition::key_type> needed;
      // ----------------------------------------------------------------------------
      // Let's test the thing:
      // ----------------------------------------------------------------------------
      printout(INFO,"TEST","Got a total of %5ld [%5ld,%5ld] conditions for %s",
               existing->conditions().size()+existing->derived().size(),
               existing->conditions().size(),existing->derived().size(),
               de.name());

      for( const auto& e : de.children() )  {
        DetElement child = e.second;
        needed.clear();
        dump(0, sel, child, needed);
      }
      required->clear();
      // Now the full detector child
      for( const auto& c : de.children() )  {
        TTimeStamp start;
        DetElement child = c.second;
        key = ConditionKey(child,gaudi::Keys::deKey).hash;
        needed.clear();
        needed.insert(key);
        required->clear();
        sel.select(needed, true);
        TTimeStamp stop;
        printout(INFO,"TEST","Selected %5ld [%5ld,%5ld] conditions for %s [%7.5f seconds]",
                 (required->conditions().size()+required->derived().size()),
                 required->conditions().size(),required->derived().size(),
                 child.name(), stop.AsDouble()-start.AsDouble());
      }
      // Now the full detector
      key = ConditionKey(de,gaudi::Keys::deKey).hash;
      required->clear();
      needed.clear();
      TTimeStamp start;
      needed.insert(key);
      sel.select(needed, true);
      TTimeStamp stop;
      printout(INFO,"TEST","Selected %5ld [%5ld,%5ld] conditions for %s [%7.5f seconds]",
               (required->conditions().size()+required->derived().size()),
               required->conditions().size(),required->derived().size(),
               de.name(), stop.AsDouble()-start.AsDouble());
    }
  };
  
  bool help = false;
  vector<string> detectors;
  for(int i=0; i<argc && argv[i]; ++i)  {
    if ( argv[i][0] == '-' || argv[i][0] == '/' )     {
      if ( 0 == ::strncmp("-help",argv[i],4) )
        help = true;
      else if ( 0 == ::strncmp("-detector",argv[i],4) )
        detectors.push_back(argv[++i]);
      else
        help = true;
    }
  }
  if ( help )   {
    /// Help printout describing the basic command line interface
    cout <<
      "Usage: -plugin <name> -arg [-arg]                                   \n"
      "     name:   factory name     LHCb_TEST_cond_content                \n"
      "     -detector <name>         Name of the sub-detector to analyze.  \n"
      "     -help                    Ahow this help.                       \n"
      "\tArguments given: " << arguments(argc,argv) << endl << flush;
    ::exit(EINVAL);
  }
  Dumper dmp(description);
  for(const auto& det_name : detectors )
    dmp(det_name);
  return 1;
}
DECLARE_APPLY(LHCb_TEST_cond_content,test_conditions_recipes)


static long test_conditions_sd_content(Detector& description, int argc, char** argv)  {
  typedef shared_ptr<gaudi::ConditionsRepository> CONTENT;
  bool help = false;
  string detector;
  for(int i=0; i<argc && argv[i]; ++i)  {
    if ( argv[i][0] == '-' || argv[i][0] == '/' )     {
      if ( 0 == ::strncmp("-help",argv[i],4) )
        help = true;
      else if ( 0 == ::strncmp("-detector",argv[i],4) )
        detector = argv[++i];
      else
        help = true;
    }
  }
  if ( help )   {
    /// Help printout describing the basic command line interface
    cout <<
      "Usage: -plugin <name> -arg [-arg]                                   \n"
      "     name:   factory name     LHCb_TEST_cond_content                \n"
      "     -detector <name>         Name of the sub-detector to analyze.  \n"
      "     -help                    Ahow this help.                       \n"
      "\tArguments given: " << arguments(argc,argv) << endl << flush;
    ::exit(EINVAL);
  }
  TTimeStamp start;
  DetElement de = description.detector(detector);
  const CONTENT&           existing = *de.extension<CONTENT>();
  CONTENT                  required(new gaudi::ConditionsRepository());
  gaudi::RequestSelector    sel(*existing, *required);
  Condition::key_type      key = ConditionKey(de,gaudi::Keys::deKey).hash;
  set<Condition::key_type> needed;
  needed.insert(key);
  sel.select(needed, true);
  TTimeStamp stop;
  printout(INFO,"TEST","\tSelected %5ld [%5ld,%5ld] conditions for %s  [%7.5f seconds]",
           (required->conditions().size()+required->derived().size()),
           required->conditions().size(),required->derived().size(),
           de.path().c_str(), stop.AsDouble()-start.AsDouble());
  return 1;
}
DECLARE_APPLY(LHCb_TEST_cond_sd_content,test_conditions_sd_content)

#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/ConditionDerived.h"

#include "DDCond/ConditionsSlice.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsManagerObject.h"
#include "Detector/VP/DeVP.h"

static long test_conditions_slice(Detector& description, int argc, char** argv)  {
  typedef shared_ptr<gaudi::ConditionsRepository> CONTENT;
  size_t loads = 1, turns = 1;
  bool help = false;
  string conditions;
  vector<string> detectorNames;
  for(int i=0; i<argc && argv[i]; ++i)  {
    if ( argv[i][0] == '-' || argv[i][0] == '/' )     {
      if ( 0 == ::strncmp("-help",argv[i],4) )
        help = true;
      else if ( 0 == ::strncmp("-detector",argv[i],4) )
        detectorNames.push_back(argv[++i]);
      else if ( 0 == ::strncmp("-conditions",argv[i],4) )
        conditions = argv[++i];
      else if ( 0 == ::strncmp("-loads",argv[i],4) )
        loads = ::atol(argv[++i]);
      else if ( 0 == ::strncmp("-turns",argv[i],4) )
        turns = ::atol(argv[++i]);
      else
        help = true;
    }
  }
  if ( help || conditions.empty() )   {
    /// Help printout describing the basic command line interface
    cout <<
      "Usage: -plugin <name> -arg [-arg]                                   \n"
      "     name:   factory name     LHCb_TEST_cond_content                \n"
      "     -detector   <name>       Name of the sub-detector to analyze.  \n"
      "     -conditions <directory>  Top-directory with conditions files.  \n"
      "                              Fully qualified: <protocol>://<path>  \n"
      "     -help                    Show this help.                       \n"
      "\tArguments given: " << arguments(argc,argv) << endl << flush;
    ::exit(EINVAL);
  }
  if ( conditions.rfind('/') != conditions.length() )   {
    conditions += '/';
  }
  dd4hep::setPrintFormat("%-24s %5s %s");
  /******************** Initialize the conditions manager *****************/
  // Now we instantiate the conditions manager
  cond::ConditionsManager manager(description, "DD4hep_ConditionsManager_Type1");
  manager["PoolType"]       = "DD4hep_ConditionsLinearPool";
  manager["UserPoolType"]   = "DD4hep_ConditionsMapUserPool";
  manager["UpdatePoolType"] = "DD4hep_ConditionsLinearUpdatePool";
  manager["LoaderType"]     = "LHCb_ConditionsLoader";
  manager["OutputUnloadedConditions"] = true;
  manager["LoadConditions"] = true;
  manager.initialize();
  const IOVType*    iov_typ = manager.registerIOVType(0,"run").second;
  if ( 0 == iov_typ )  {
    except("ConditionsPrepare","++ Unknown IOV type supplied.");
  }
  
  cond::ConditionsDataLoader& loader = manager.loader();
  loader["ReaderType"] = "LHCb_ConditionsFileReader";
  loader["Directory"]  = conditions;
  loader["Match"]      = conditions.substr(0, conditions.find('/'));
  loader["IOVType"]    = "run";
  loader.initialize();

  CONTENT all_conditions(new gaudi::ConditionsRepository());
  std::set<DetElement> detectors;
  for(const auto& det : detectorNames )    {
    DetElement     de = description.detector(det);
    const CONTENT& de_conds = *de.extension<CONTENT>();
    all_conditions->merge(*de_conds);
    if ( de.path() == "/world" ) continue;
    detectors.insert(de);
  }

  for(const auto& de : detectors )    {
    CONTENT                  required(new gaudi::ConditionsRepository());
    gaudi::RequestSelector   sel(*all_conditions, *required);
    Condition::key_type      key = ConditionKey(de,gaudi::Keys::deKey).hash;
    TTimeStamp               start;
    set<Condition::key_type> needed;
    needed.insert(key);
    sel.select(needed, true);
    TTimeStamp               stop;
    printout(INFO,"TEST","\tSelected %5ld [%5ld,%5ld] conditions requests for %s  [%7.5f seconds]",
             (required->conditions().size()+required->derived().size()),
             required->conditions().size(),required->derived().size(),
             de.path().c_str(), stop.AsDouble()-start.AsDouble());
  }
  //TRandom3 random;
  //unsigned int rndm = 1+random.Integer(num_iov*10);

  /// Seclection procedure: This selects all subdetector conditions specified
  CONTENT content(new gaudi::ConditionsRepository());
  gaudi::RequestSelector sel(*all_conditions, *content);
  std::set<Condition::key_type> needed;
  for(const auto& de : detectors )
    needed.insert(ConditionKey(de,gaudi::Keys::deKey).hash);
  needed.insert(gaudi::Keys::alignmentsComputedKey);
  sel.select(needed, true);

  for(size_t i=1; i<loads+1; ++i)   {
    IOV req_iov(iov_typ,i*100);
    for(size_t j=0; j<turns; ++j)   {
      shared_ptr<cond::ConditionsSlice>   slice(new cond::ConditionsSlice(manager,content));
      TTimeStamp start;
      /// Load the conditions
      //cond::ConditionsManager::Result total = manager.prepare(req_iov,*slice);
      cond::ConditionsManager::Result total = manager.load(req_iov,*slice);
      TTimeStamp comp;
      total += manager.compute(req_iov,*slice);
      TTimeStamp stop;
      cond::ConditionsContent::Conditions& missing = slice->missingConditions();
      for ( const auto& m : missing )   {
        printout (ERROR, "TEST", "Failed to load condition [%016llX]: %s",
                  m.first, m.second->toString().c_str());
      }
      gaudi::DeVP devp = slice->get(description.detector("VP"),gaudi::Keys::deKey);
      int imod = 0, isens = 0;
      for (const auto& m : devp.modules())   {
        cout << "    Module: " << ++imod << "  " << m->detector.path() << endl;
        for( auto sensor : m->sensors )  {
          cout << "        Sensor:   " <<  ++isens << "  " << sensor->detector.path() << endl;
        }
      }
      isens = 0;
      for( auto sensor : devp.sensors() )  {
        cout << "    DEVP-Sensor:   " <<  ++isens << "  " << sensor->detector.path() << endl;
      }
      printout(INFO,"Statistics",
               "+  Created/Accessed a total of %ld conditions "
               "(S:%6ld,L:%6ld,C:%6ld,M:%ld)  Load:%7.5f sec Compute:%7.5f sec",
               total.total(), total.selected, total.loaded, total.computed, total.missing,
               comp.AsDouble()-start.AsDouble(),
               stop.AsDouble()-comp.AsDouble());
      slice.reset();
    }
  }
  /// Clear it
  manager.clear();
  // Let's do the cleanup
  all_conditions.reset();
  content.reset();
  manager.destroy();
  return 1;
}
DECLARE_APPLY(LHCb_TEST_cond_slice,test_conditions_slice)
