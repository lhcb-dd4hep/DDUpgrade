//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
//  \author  Markus Frank
//  \date    2016-02-02
//  \version 1.0
//
//==========================================================================

// Framework include files
#include "Conditions/ConditionsLoader.h"
#include "Conditions/ConditionsRepository.h"
#include "Conditions/ConditionsReaderContext.h"

// Other dd4hep includes
#include "XML/XML.h"
#include "DD4hep/Printout.h"
#include "DD4hep/Operators.h"
#include "DD4hep/PluginCreators.h"
#include "DD4hep/detail/ConditionsInterna.h"
#include "DDCond/ConditionsTags.h"
#include "DDCond/ConditionsManagerObject.h"
#include "Conditions/UpgradeTags.h"

#include "TTimeStamp.h"

// Forward declartions
using namespace std;
using namespace dd4hep;

using cond::ConditionsSlice;
using cond::ConditionsListener;
using cond::ConditionsDescriptor;
using cond::ConditionsManagerObject;
using cond::ConditionsLoadInfo;

/// Standard constructor, initializes variables
gaudi::ConditionsLoader::ConditionsLoader(Detector& description, cond::ConditionsManager mgr, const string& nam) 
  : ConditionsDataLoader(description, mgr, nam)
{
  declareProperty("ReaderType", m_readerType);
  declareProperty("Directory", m_directory);
  declareProperty("Match", m_match);
  declareProperty("IOVType", m_iovTypeName);
}

/// Default Destructor
gaudi::ConditionsLoader::~ConditionsLoader() {
} 

/// Access a conditions loader for a particular class ID
gaudi::ConditionConverter& gaudi::ConditionsLoader::conditionConverter(int class_id)    {
  auto icnv = m_converters.find(class_id);
  if ( icnv == m_converters.end() )  {
    stringstream str;
    str << "LHCb_ConditionsConverter_" << class_id;
    ConditionConverter* cnv = createPlugin<ConditionConverter>(str.str(),m_detector,0,0);
    if ( cnv ) {
      m_converters.insert(make_pair(class_id, cnv));
      return *cnv;
    }
    except("ConditionsLoader","Failed to create ConditionConverter for class ID:%d",class_id);
  }
  return *((*icnv).second);
}

/// Initialize loader according to user information
void gaudi::ConditionsLoader::initialize()   {
  string typ = m_readerType;
  const void* argv[] = {"ConditionsReader", this, 0};
  m_reader.reset(createPlugin<ConditionsReader>(typ,m_detector,2,argv));
  (*m_reader)["Directory"] = m_directory;
  (*m_reader)["Match"] = m_match;
  m_iovType = manager().iovType(m_iovTypeName);
  if ( !m_iovType ) {
    // Error
  }
}

/// Load single conditions document
void gaudi::ConditionsLoader::loadDocument(xml::UriContextReader& /* rdr */,
                                           const string&          /* sys_id */)
//RequiredItems&         work,
//LoadedItems&           loaded,
//IOV&                   conditions_validity)
{
}

/// Load  a condition set given a Detector Element and the conditions name according to their validity
size_t gaudi::ConditionsLoader::load_range(key_type         /* key */,
                                           const IOV&       /* req_iov */,
                                           RangeConditions& /* conditions */)   {
  return 0;
}

/// Access single conditions from the persistent medium
size_t gaudi::ConditionsLoader::load_single(key_type         /* key */,
                                            const IOV&       /* req_iov */,
                                            RangeConditions& /* conditions */)   {
  return 0;
}

/// Optimized update using conditions slice data
size_t gaudi::ConditionsLoader::load_many(const IOV&      req_iov,
                                          RequiredItems&  work,
                                          LoadedItems&    loaded,
                                          IOV&            conditions_validity)
{
  ConditionsReaderContext  local;
  size_t len = loaded.size();

  local.event_time  = req_iov.keyData.first;
  local.valid_since = 0;
  local.valid_until = 0;

  xml::UriContextReader local_reader(m_reader.get(), &local);

  // First collect all required URIs which need loading.
  // Since one file contains many conditions, we have
  // to create a unique set
  string buffer;
  size_t loaded_len    = loaded.size();
  bool   print_results = isActivePrintLevel(DEBUG);

  set<string> urls;
  map<Condition::key_type,pair<bool,gaudi::ConditionIdentifier*> > todo;
  loaded.clear();
  for( const auto& i : work )
    todo.insert(make_pair(i.first, make_pair(false,(gaudi::ConditionIdentifier*)i.second->ptr())));
  try   {  
    for( auto i = todo.begin(); i != todo.end(); ++i )   {     // O[N]
      bool done = i->second.first;
      if ( done )   {
        continue;
      }
      gaudi::ConditionIdentifier* info = i->second.second;
      if ( !info )  {
        printout(INFO,"ConditionsLoader","++ CANNOT update condition: [%16llX] [No load info]",i->first);
        continue;
      }
      auto file_info = urls.find(info->sys_id);
      if ( file_info != urls.end() )   {
        continue;
      }
      urls.insert(info->sys_id);
      TTimeStamp start;
      bool result = m_reader->load(info->sys_id, &local, buffer);
      if ( result )    {
        ConditionKey::KeyMaker kmaker(info->sys_id_hash,0);
        const auto* repo = info->repository;
        xml_doc_holder_t doc(xml_handler_t().parse(buffer.c_str(),
                                                   buffer.length(),
                                                   info->sys_id.c_str(),
                                                   &local_reader));
        xml_h e = doc.root();
        vector<Condition> entity_conditons;
        entity_conditons.reserve(1000);
        // We implicitly assume that one file may only contain
        for(xml_coll_t cond(e,_UC(condition)); cond; ++cond)   {
          xml_h   x_c = cond;
          int     cls = x_c.attr<int>(_LBU(classID));
          string  nam = x_c.attr<string>(_U(name));
          kmaker.values.det_key  = info->sys_id_hash;
          kmaker.values.item_key = detail::hash32(nam);
          auto cond_ident  = repo->locations.find(kmaker.hash);  // O[log(N)]
          if ( cond_ident == repo->locations.end() )   {
            printout(ERROR,"ConditionsLoader","++ Got stray condition %s from %s",
                     nam.c_str(), info->sys_id.c_str());
            continue;
          }
          Condition::key_type cond_key = cond_ident->second->hash;
          Condition c = conditionConverter(cls)(m_detector, cond_ident->second, x_c);
          if ( !c.isValid() )   {
            // Will eventually have to raise an exception
            printout(ERROR,"ConditionsLoader","++ Failed to load condition %s from %s",
                     nam.c_str(), info->sys_id.c_str());
            continue;
          }
          c->hash = cond_ident->second->hash;
          loaded[cond_key] = c;
          entity_conditons.push_back(c);
          if ( cond_key == i->first )   {
            i->second.first = true;
            continue;
          }
          auto cond_itr = todo.find(cond_key);                    // O[log(N)]
          if ( cond_itr != todo.end() )  {
            cond_itr->second.first = true;
            continue;
          }
          printout(WARNING,"ConditionsLoader","++ Got stray but valid condition %s from %s",
                   nam.c_str(), info->sys_id.c_str());
        }
        IOV::Key iov_key(local.valid_since, local.valid_until);
        cond::ConditionsPool* pool = manager().registerIOV(*m_iovType, iov_key);
        size_t ret = manager().blockRegister(*pool, entity_conditons);
        if ( ret != entity_conditons.size() )   {
          // Error!
        }

        TTimeStamp stop;
        printout(INFO,"ConditionsLoader","++ Loaded %4ld conditions from %-48s [%7.3f sec]",
                 loaded.size()-loaded_len,info->sys_id.c_str(),
                 stop.AsDouble()-start.AsDouble());
        loaded_len = loaded.size();
        if ( !print_results ) continue;
      }
      if ( print_results )  {
        for( const auto& e : loaded )  {
          const Condition& cond = e.second;
          printout(INFO,"ConditionsLoader","++ %16llX: %s -> %s",
                   cond.key(),cond->value.c_str(),cond.name());
        }
      }
    }
  }
  catch( const exception& e )  {
    printout(ERROR,"ConditionsLoader","+++ Load exception: %s",e.what());
    throw;
  }
  catch( ... )  {
    printout(ERROR,"ConditionsLoader","+++ UNKNWON Load exception.");
    throw;    
  }
  return loaded.size()-len;
}

#include "DD4hep/Factories.h"

//==========================================================================
/// Plugin function
static void* create_loader(Detector& description, int argc, char** argv)   {
  const char* name = argc>0 ? argv[0] : "ConditionsLoader";
  cond::ConditionsManagerObject* m = (cond::ConditionsManagerObject*)(argc>0 ? argv[1] : 0);
  return new gaudi::ConditionsLoader(description,m,name);
}
DECLARE_DD4HEP_CONSTRUCTOR(LHCb_ConditionsLoader,create_loader)
