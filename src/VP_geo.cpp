//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================
//
// Specialized generic detector constructor
// 
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "XML/Utilities.h"
#include "Conditions/UpgradeTags.h"

using namespace std;
using namespace dd4hep;
using namespace dd4hep::detail;

namespace   {

  /// Helper class to build the VP detector of LHCb
  struct VPBuild : public xml::tools::VolumeBuilder  {
    /// Debug flags
    Material   silicon;
    /// Reference to the module detector element
    DetElement de_module_with_support;
    /// Distance of closest pixel cell to beam line. Used often, so take it global
    double     ClosestPixel           = 0.0;
    /// Flag to disable building supports
    bool build_mod_support = true;
    /// Flag to disable building the VP half stations
    bool build_sides = true;
    /// Flag to disable building the wakefield cones
    bool build_wake_cones = true;
    /// Flag to disable building the detector support in the vacuum tank
    bool build_det_support = true;
    /// Flag to disable building the vakuum tank
    bool build_vacuum_tank = true;
    /// Flag to disable building the RF box
    bool build_rf_box = true;
    /// Flag to disable building the RF foil
    bool build_rf_foil = true;
    string select_volume{"lvVP"};
    /// Initializing constructor
    VPBuild(Detector& description, xml_elt_t e, SensitiveDetector sens);
    void build_groups();    
    void build_module();
    void build_detector();
  };

  /// Initializing constructor
  VPBuild::VPBuild(Detector& dsc, xml_elt_t e, SensitiveDetector sens)
    : xml::tools::VolumeBuilder(dsc, e, sens)
  {
    // Process debug flags
    xml_comp_t x_dbg  = x_det.child(_U(debug), false);
    if ( x_dbg )   {
      for(xml_coll_t i(x_dbg,_U(item)); i; ++i)  {
        xml_comp_t c(i);
        string n = c.nameStr();
        if      ( n == "debug" )
          debug = c.attr<int>(_U(value)) != 0;
        else if ( n == "build_mod_support" )
          build_mod_support = c.attr<int>(_U(value)) != 0;
        else if ( n == "build_sides" )
          build_sides = c.attr<int>(_U(value)) != 0;
        else if ( n == "build_det_support" )
          build_det_support = c.attr<int>(_U(value)) != 0;
        else if ( n == "build_vacuum_tank" )
          build_vacuum_tank = c.attr<int>(_U(value)) != 0;
        else if ( n == "build_wake_cones" )
          build_wake_cones = c.attr<int>(_U(value)) != 0;
        else if ( n == "build_rf_box" )
          build_rf_box = c.attr<int>(_U(value)) != 0;
        else if ( n == "build_rf_foil" )
          build_rf_foil = c.attr<int>(_U(value)) != 0;
        else if ( n == "select_volume" )
          select_volume = c.attr<string>(_U(value));
      }
    }
    silicon = description.material("Silicon");
    // Distance of closest pixel cell to beam line
    ClosestPixel           = _toDouble("VP:ClosestPixel");
  }

  void VPBuild::build_module()   {
    //double       eps    = _toDouble("VP:Epsilon");
    double       vp_eps   = _toDouble("VP:VPEpsilon");
    double       vp_rot_z = _toDouble("VP:Rotation");
    Position     pos;
    PlacedVolume pv;
    
    // Distance between two readout chips
    double Chip2ChipDist     = _toDouble("VP:Chip2ChipDist");
    // Chip dimensions
    double ChipThick         = _toDouble("VP:ChipThick");
    double ChipSizeX         = _toDouble("VP:ChipSizeX");
    double ChipSizeY         = _toDouble("VP:ChipSizeY");
    // Thickness of glue layer
    double GlueThick         = _toDouble("VP:GlueThick");
    // Sensor dimensions (active area)
    double SiThick           = _toDouble("VP:SiThick");
    double SiSizeX           = _toDouble("VP:SiSizeX");
    double SiSizeY           = _toDouble("VP:SiSizeY");
    // Width of inactive edge
    double SiEdge            = _toDouble("VP:SiEdge");
    // Distance between the two adjacent (same-side) sensors
    double Sensor2SensorDist = _toDouble("VP:Sensor2SensorDist");
    // Shift between ladders on opposite sides
    double LadderShift       = _toDouble("VP:LadderShift");
    double SubstrateThick    = _toDouble("VP:SubstrateThick");

    
    // Description of single Chip
    Box    boxChip(ChipSizeX/2.0, ChipSizeY/2.0, ChipThick/2.0);
    Volume lvChip("lvChip", boxChip, silicon);
    lvChip.setVisAttributes(description, "VP:Chip");
    // Glue layer between substrate and chip
    Box    boxGlue(ChipSizeX/2.0, ChipSizeY/2.0, GlueThick/2.0);
    Volume lvGlue("lvGlue", boxGlue, description.material("VP:Epoxy"));
    lvGlue.setVisAttributes(description, "VP:Glue");
    // Chip array
    Assembly lvChips("lvChips");
    double pos_X = SiSizeX/2.0 - ChipSizeX - Chip2ChipDist;
    double pos_Y = SiSizeY - ChipSizeY/2.0;
    lvChips.placeVolume(lvChip, Position(pos_X, pos_Y,  ChipThick/2.0));
    lvChips.placeVolume(lvGlue, Position(pos_X, pos_Y, -GlueThick/2.0));
    pos_X = SiSizeX/2.0;
    lvChips.placeVolume(lvChip, Position(pos_X, pos_Y,  ChipThick/2.0));
    lvChips.placeVolume(lvGlue, Position(pos_X, pos_Y, -GlueThick/2.0));
    pos_X = SiSizeX/2.0 + ChipSizeX + Chip2ChipDist;
    lvChips.placeVolume(lvChip, Position(pos_X, pos_Y,  ChipThick/2.0));
    lvChips.placeVolume(lvGlue, Position(pos_X, pos_Y, -GlueThick/2.0));

    // Silicon sensor
    Box detBox      (SiSizeX,     SiSizeY,     SiThick/2.0);
    Box detCutLeft  (SiSizeX/2.0, 1.5*SiSizeY, SiThick);
    Box detCutRight (SiSizeX/2.0, 1.5*SiSizeY, SiThick);
    Box detCutBottom(SiSizeX*1.5, SiSizeY/2.0, SiThick);
    Box detCutTop   (SiSizeX*1.5, SiSizeY/2.0, SiThick);
    SubtractionSolid detSolid(detBox, detCutLeft, Position(-SiSizeX-SiEdge, 0, 0));
    detSolid = SubtractionSolid(detSolid, detCutRight,  Position(SiSizeX+SiEdge, 0, 0));
    detSolid = SubtractionSolid(detSolid, detCutBottom, Position(0,-SiSizeY-SiEdge, 0));
    detSolid = SubtractionSolid(detSolid, detCutTop,    Position(0, SiSizeY+SiEdge, 0));
    Volume lvDet("lvDet", detSolid, silicon);
    lvDet.setVisAttributes(description, "VP:Sensor");

    Assembly lvSensor("lvSensor");
    PlacedVolume pvSensor = lvSensor.placeVolume(lvDet, Position(SiSizeX/2.0, SiSizeY/2.0, 0));
    registerVolume(lvSensor.name(), lvSensor);

    Assembly lvLadder("lvLadder");
    lvLadder.placeVolume(lvSensor, Position(0, 0, ChipThick+SiThick/2.0+vp_eps));
    lvLadder.placeVolume(lvChips);
    registerVolume(lvLadder.name(), lvLadder);

    DetElement deLadder, deSensor;
    Assembly lvModule("lvModule");
    registerVolume(lvModule.name(), lvModule);
    /// Module structural hierarchy
    de_module_with_support = DetElement("module",id);
    // vertical, near foil
    pos = Position(-ClosestPixel - SiSizeY,
                    ClosestPixel  - SiSizeX,
                   -SubstrateThick/2.0 - GlueThick);
    pv = lvModule.placeVolume(lvLadder, Transform3D(RotationZYX(-M_PI/2.0, 0, M_PI), pos));
    pv->SetName("pvLadder_0");
    pv.addPhysVolID("ladder",0);
    deLadder = DetElement(de_module_with_support,"ladder_0", 0);
    deLadder.setPlacement(pv);
    deSensor = DetElement(deLadder,"sensor", 0);
    deSensor.setPlacement(pvSensor);
    // vertical, away from foil 
    pos = Position(-ClosestPixel - 2.0*SiSizeY + LadderShift,
                    ClosestPixel  - 2.0*SiEdge - Sensor2SensorDist,
                    SubstrateThick/2.0 + GlueThick);
    pv = lvModule.placeVolume(lvLadder, Transform3D(RotationZYX(-M_PI/2.0, 0, 0), pos));
    pv->SetName("pvLadder_1");
    pv.addPhysVolID("ladder",1);
    deLadder = DetElement(de_module_with_support,"ladder_1", 0);
    deLadder.setPlacement(pv);
    deSensor = DetElement(deLadder,"sensor", 1);
    deSensor.setPlacement(pvSensor);
    // horizontal, near foil
    pos = Position(ClosestPixel,
                   ClosestPixel + SiSizeY,
                   SubstrateThick/2.0 + GlueThick);
    pv = lvModule.placeVolume(lvLadder, Transform3D(RotationZYX(M_PI, 0, 0), pos));
    pv->SetName("pvLadder_2");
    pv.addPhysVolID("ladder",2);
    deLadder = DetElement(de_module_with_support,"ladder_2", 0);
    deLadder.setPlacement(pv);
    deSensor = DetElement(deLadder,"sensor", 2);
    deSensor.setPlacement(pvSensor);
    // horizontal, away from foi
    pos = Position( ClosestPixel - SiSizeX,
                    ClosestPixel + 2.0*SiSizeY-LadderShift,
                   -SubstrateThick/2.0 - GlueThick);
    pv = lvModule.placeVolume(lvLadder, Transform3D(RotationZYX(M_PI, M_PI, 0), pos));
    pv->SetName("pvLadder_3");
    pv.addPhysVolID("ladder",3);
    deLadder = DetElement(de_module_with_support,"ladder_3", 0);
    deLadder.setPlacement(pv);
    deSensor = DetElement(deLadder,"sensor", 3);
    deSensor.setPlacement(pvSensor);

    // Hybrid
    if ( build_mod_support )
      lvModule.placeVolume(volume("lvHybrid"), RotationZYX(-vp_rot_z, 0, 0));
  
    Assembly lvModuleWithSupport("lvModuleWithSupport");
    if ( build_mod_support )  
      lvModuleWithSupport.placeVolume(volume("lvSupport"));
    lvModuleWithSupport.placeVolume(lvModule, RotationZYX(vp_rot_z,0,0));
    registerVolume(lvModuleWithSupport.name(), lvModuleWithSupport);

  }

  void VPBuild::build_detector()    {
    PlacedVolume pv;
    double       Right2LeftDist = _toDouble("VP:Right2LeftDist");
    Assembly     lvLeft, lvRight;
    DetElement   deOpt, deLeft, deRight;
    if ( build_sides )   {
      RotationZYX rot_left  (M_PI, 0, 0);
      Volume      lvModule = volume("lvModuleWithSupport");
      lvLeft  = Assembly("lvVPLeft");
      lvRight = Assembly("lvVPRight");
      deLeft  = DetElement(detector, "VPLeft",0);
      deRight = DetElement(detector, "VPRight",1);
      
      for(int i=0; i<=25; ++i)   { // One or two loops -- what would be more efficient in tracking?
        double station_z = _toDouble(_toString(i,"VP:Station%02dZ"));
        pv = lvRight.placeVolume(lvModule, Position(0, 0, station_z-Right2LeftDist/2.0));
        pv->SetName(_toString(i*2,"pvModule%02dWithSupport").c_str());    // Optional
        pv.addPhysVolID("module", i*2);
        deOpt = de_module_with_support.clone(_toString(i*2,"Module%02d"),i*2);
        deOpt.setPlacement(pv);
        deRight.add(deOpt);
        pv = lvLeft.placeVolume(lvModule, Transform3D(rot_left,Position(0, 0, station_z+Right2LeftDist/2.0)));
        pv->SetName(_toString(1+i*2,"pvModule%02dWithSupport").c_str());  // Optional
        pv.addPhysVolID("module", i*2+1);
        deOpt = de_module_with_support.clone(_toString(i*2+1,"Module%02d"),i*2+1);
        deOpt.setPlacement(pv);
        deLeft.add(deOpt);
      }
      if ( build_rf_box )   {
        // This looks correct:
        printout(WARNING,"VP","+++ Placing RF box at an ASSUMED position! [not found in XML]");
        //lvLeft.placeVolume( volume("lvRFBoxLeft"),  Position(0,0,_toDouble("-243*mm")));
        //lvRight.placeVolume(volume("lvRFBoxRight"), Position(0,0,_toDouble("-243*mm")));
        // However, this is what is in the XML:
        pv = lvLeft.placeVolume( volume("lvRFBoxLeft"));
        deOpt = DetElement(deLeft,"RFBox",deLeft.id());
        deOpt.setPlacement(pv);
        pv = lvRight.placeVolume(volume("lvRFBoxRight"));
        deOpt = DetElement(deRight,"RFBox",deLeft.id());
        deOpt.setPlacement(pv);
      }
      if ( build_rf_foil )   {
        Volume   vol = volume("lvRFFoil");
        Position pos_left( 0,0,_toDouble("VP:RFFoilLeftPosZ"));
        Position pos_right(0,0,_toDouble("VP:RFFoilRightPosZ"));
        pv = lvLeft.placeVolume( vol, pos_left);
        deOpt = DetElement(deLeft,"RFFoilLeft",deLeft.id());
        deOpt.setPlacement(pv);
        pv = lvRight.placeVolume(vol, Transform3D(RotationZYX(M_PI,0,0),pos_right));
        deOpt = DetElement(deRight,"RFFoilRight",deRight.id());
        deOpt.setPlacement(pv);
      }
    }
    if ( build_vacuum_tank && build_det_support )  {
      Volume vol = volume("lvVeloDetSup");
      Position pos_left (_toDouble("VP:DetSupLeftPosX"), 0,_toDouble("VP:DetSupLeftPosZ"));
      Position pos_right(_toDouble("VP:DetSupRightPosX"),0,_toDouble("VP:DetSupRightPosZ"));
      pv = lvLeft.placeVolume( vol, Transform3D(RotationZYX(M_PI,0,0), pos_left));
      deOpt = DetElement(deLeft,"Support",deLeft.id());
      deOpt.setPlacement(pv);
      pv = lvRight.placeVolume(vol, pos_right);
      deOpt = DetElement(deRight,"Support",deRight.id());
      deOpt.setPlacement(pv);
    }
    
    // Now we build the main velo shape
    double zpos        = _toDouble("VP:VeloZ");
    double radius      = _toDouble("VP:VeloRad");
    double DSEndStartZ = _toDouble("VP:VeloDSEndStartZ");
    double DSEndDeltaZ = _toDouble("VP:VeloDSEndDeltaZ");
    double EWFlangeZExcess = _toDouble("VP:VeloEWFlangeZExcess");
    UnionSolid vp_shape(Tube(0, radius, zpos/2.0),
                        Tube(0, radius, DSEndDeltaZ/2.0),
                        Position(0,0,DSEndStartZ+DSEndDeltaZ/2.0));
    vp_shape = UnionSolid(vp_shape,
                          Tube("VP:vTankDownEWFlangeIR-2*mm",
                               "VP:vTankDownEWFlangeOR+2*mm",
                               EWFlangeZExcess/2.0+1.*dd4hep::mm),
                          Position(0,0,DSEndStartZ+DSEndDeltaZ+(EWFlangeZExcess+2.0*dd4hep::mm)/2.0));

    Volume lvVP("lvVP", vp_shape, description.vacuum());
    lvVP.setAttributes(description, x_det.regionStr(), x_det.limitsStr(), x_det.visStr());
    if ( build_sides )   {
      pv = lvVP.placeVolume(lvLeft);
      pv.addPhysVolID("side",0);
      deLeft.setPlacement(pv);
      pv = lvVP.placeVolume(lvRight);
      pv.addPhysVolID("side",1);
      deRight.setPlacement(pv);
    }
    if ( build_vacuum_tank )  {
      DetElement de(detector, "VacuumTank", id);
      Position pos(0,0,_toDouble("VP:vTankPosZ"));
      pv = lvVP.placeVolume(volume("lvVacTank"), pos);
      de.setPlacement(pv);
    }
    if ( build_wake_cones )  {
      // We have them as volumes, but we dop not enter them into the hierarchy.
      Position pos_up(0,0,_toDouble("VP:UpStrWakeFieldConePosZ"));
      lvVP.placeVolume(volume("lvUpstreamWakeFieldCone"), pos_up);
      Position pos_down(0,0,_toDouble("VP:DownStrWakeFieldConePosZ"));
      lvVP.placeVolume(volume("lvDownstreamWakeFieldCone"), pos_down);
    }
    registerVolume(lvRight.name(), lvRight);
    registerVolume(lvLeft.name(), lvRight);
    registerVolume(lvVP.name(), lvVP);
  } 


  void VPBuild::build_groups()   {
    sensitive.setType("tracker");
    load(x_det, "include");
    buildVolumes(x_det);
    placeDaughters(detector, Volume(), x_det);
    if ( build_sides )   {
      build_module();
    }
    build_detector();
    Volume vol = volume(select_volume);
    PlacedVolume pv = placeDetector(vol);
    pv.addPhysVolID("system",  id);
    destroyHandle(de_module_with_support);
  }

}

static Ref_t create_element(Detector& description, xml_h e, SensitiveDetector sens_det)  {
  VPBuild       builder(description, e, sens_det);
  builder.build_groups();
  return builder.detector;
}
DECLARE_DETELEMENT(LHCb_VP,create_element)
