//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================
//
// Specialized generic detector constructor
// 
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "DD4hep/ExtensionEntry.h"
#include "DD4hep/ConditionDerived.h"
#include "DDCond/ConditionsTags.h"
#include "DDCond/ConditionsContent.h"
#include "Detector/VP/DeVPConditionCalls.h"
#include "Conditions/ConditionIdentifier.h"
#include "Conditions/ConditionsRepository.h"

using namespace dd4hep;

static long create_conditions_recipes(Detector& description, xml_h e)  {
  using namespace std;
  using namespace gaudi;
  auto requests = shared_ptr<gaudi::ConditionsRepository>(new gaudi::ConditionsRepository());
  map<string, string> alignment_locations;
  map<string, string> condition_locations;
  string location;

  // First read from the XML element the file "locations"
  for(xml_coll_t i(e, _U(alignments)); i; ++i)  {
    xml_comp_t c = i;
    alignment_locations[c.attr<string>(_U(type))] = c.attr<string>(_U(ref));
  }
  for(xml_coll_t i(e, _UC(conditions)); i; ++i)  {
    xml_comp_t c = i;
    condition_locations[c.attr<string>(_U(type))] = c.attr<string>(_U(ref));
  }
  // All information we need to create the new conditions we will receive during the callbacks.
  // It is not necessary to cache information such as detector elements etc.
  DeConditionCall* vp_iovUpdate        = new DeVPConditionCall<DeVP>              (requests);
  DeConditionCall* vp_staticUpdate     = new DeVPConditionCall<DeVPStatic>        (requests);
  DeConditionCall* gen_iovUpdate       = new DeVPConditionCall<DeVPGeneric>       (requests);
  DeConditionCall* gen_staticUpdate    = new DeVPConditionCall<DeVPGenericStatic> (requests);
  DeConditionCall* sensor_iovUpdate    = new DeVPConditionCall<DeVPSensor>        (requests);
  DeConditionCall* sensor_staticUpdate = new DeVPConditionCall<DeVPSensorStatic>  (requests);
  //DeConditionCall* default_delta       = new DeDefaultDeltaCall                   (requests);
  // --------------------------------------------------------------------------
  // 1) Setup the addresses for the raw conditions (call: addLocation())
  // 2) Setup the callbacks and their dependencies:
  // 2.1) Static detector elements depend typically on nothing.
  //      These parameters normally were read during
  //      the detector construction in the XML define section.
  //      The callback ensures the creation of the objects
  // 2.2) IOV dependent detector elements depend on
  //      - the raw conditions data (aka the time dependent conditions) (see 1)
  //      - and the static detector element
  // 2.3) Typically parents have references to the next level children
  //      - Add reference to the child detector elements
  // --------------------------------------------------------------------------
  auto deVP     = description.detector("VP");
  auto vpAddr   = requests->addLocation(deVP, Keys::deltaKey, alignment_locations["system"], "VPSystem");
  auto vpStatic = requests->addDependency (deVP, Keys::staticKey, vp_staticUpdate->addRef());
  auto vpIOV    = requests->addDependency (deVP, Keys::deKey,     vp_iovUpdate->addRef());
  vpStatic.second->dependencies.push_back(vpAddr.first);
  vpIOV.second->dependencies.push_back(vpStatic.first);
  for (auto& side : deVP.children() )  {
    auto deSide = side.second;
    string side_nam  = deSide.name();
    // Ignore all which does not start with "VP...."
    if ( side_nam.empty() || !(side_nam[0] == 'V' && side_nam[1] == 'P') ) continue;
    auto sideAddr    = requests->addLocation(deSide, Keys::deltaKey, alignment_locations["system"], side_nam);
    auto sideStatic  = requests->addDependency (deSide, Keys::staticKey, gen_staticUpdate->addRef());
    auto sideIOV     = requests->addDependency (deSide, Keys::deKey,     gen_iovUpdate->addRef());
    sideStatic.second->dependencies.push_back(sideAddr.first);
    sideIOV.second->dependencies.push_back(sideStatic.first);
    vpIOV.second->dependencies.push_back(sideIOV.first);
    for (auto& module : deSide.children() )  {
      auto   deMod   = module.second;
      string mod_nam = deMod.name();  // ModuleXY
      // Ignore all which is not ModuleXY
      if ( mod_nam.empty() || mod_nam[0] != 'M' ) continue;
      auto modStatic = requests->addDependency (deMod, Keys::staticKey, gen_staticUpdate->addRef());
      auto modIOV    = requests->addDependency (deMod, Keys::deKey,     gen_iovUpdate->addRef());
      auto modAddr   = requests->addLocation(deMod,    Keys::deltaKey, alignment_locations["module"], mod_nam);
      modStatic.second->dependencies.push_back(modAddr.first);
      modIOV.second->dependencies.push_back(modStatic.first);
      sideIOV.second->dependencies.push_back(modIOV.first);
      for (auto& assembly : deMod.children() )  {
        auto   deAsm   = assembly.second;
        auto asmStatic = requests->addDependency (deAsm, Keys::staticKey, gen_staticUpdate->addRef());
        auto asmIOV    = requests->addDependency (deAsm, Keys::deKey,     gen_iovUpdate->addRef());
        asmIOV.second->dependencies.push_back(asmStatic.first);
        modIOV.second->dependencies.push_back(asmIOV.first);
        // Assembly supports have no alignment constants
        // string asm_nam = mod_nam + "/" + deAsm.name();
        //auto asmAddr   = requests->addLocation(deAsm, Keys::deltaKey, alignment_locations["module"], asm_nam);
        //asmStatic.second->dependencies.push_back(asmAddr.first);
        for (auto& sensor : deAsm.children() )  {
          auto deSensor     = sensor.second;
          auto sensorStatic = requests->addDependency (deSensor, Keys::staticKey, sensor_staticUpdate->addRef());
          auto sensorIOV    = requests->addDependency (deSensor, Keys::deKey,     sensor_iovUpdate->addRef());
          sensorIOV.second->dependencies.push_back(sensorStatic.first);
          asmIOV.second->dependencies.push_back(sensorIOV.first);
          // Sensors have no alignment constants
          // string sensor_nam = asm_nam + "/" + deSensor.name();
          //auto sensorAddr   = requests->addLocation(deSensor, Keys::deltaKey, alignment_locations["sensors"], sensor_nam);
          //auto sensorAddr   = requests->addDependency(deSensor, Keys::deltaKey, default_delta->addRef());
          //sensorStatic.second->dependencies.push_back(sensorAddr.first);
        }
      }
    }
  }
  typedef shared_ptr<gaudi::ConditionsRepository> Ext_t;
  deVP.addExtension(new dd4hep::detail::DeleteExtension<Ext_t, Ext_t>(new Ext_t(requests)));
  vp_iovUpdate->release();
  vp_staticUpdate->release();
  gen_iovUpdate->release();
  gen_staticUpdate->release();
  sensor_iovUpdate->release();
  sensor_staticUpdate->release();
  return 1;
}
DECLARE_XML_DOC_READER(LHCb_VP_cond,create_conditions_recipes)



