#include <iostream>
#include <memory>
#include <map>
#include <typeinfo>

#include "TTimeStamp.h"

#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "DD4hep/InstanceCount.h"
#include "DD4hep/ConditionDerived.h"

#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/ConditionDerived.h"

#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsSlice.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsManagerObject.h"

#include "Detector/Common/DetectorElement.h"
#include "Detector/Common/DeConditionCall.h"
#include "Detector/Common/DeAlignmentCall.h"
#include "Conditions/ConditionsRepository.h"
#include "Detector/Common/ConditionService.h"

#include "Detector/VP/DeVP.h"

namespace Gaudi
{
typedef ROOT::Math::Rotation3D Rotation3D;    ///< 3D rotation
typedef ROOT::Math::EulerAngles EulerAngles;  ///< 3D Euler Angles
typedef ROOT::Math::Quaternion Quaternion;    ///< 3D Quaternion
typedef ROOT::Math::AxisAngle AxisAngle;      ///< 3D Axis Angle
typedef ROOT::Math::RotationX RotationX;      ///< Rotation about X axis
typedef ROOT::Math::RotationY RotationY;      ///< Rotation about Y axis
typedef ROOT::Math::RotationZ RotationZ;      ///< Rotation about Z axis
typedef ROOT::Math::Transform3D Transform3D;  ///< General 3D transformation (rotation+translation)
typedef ROOT::Math::XYZVector TranslationXYZ; ///< 3D translation

} // namespace Gaudi

using namespace dd4hep;
using namespace std;

struct GeometryCache
{
  unsigned int m_firstModule;
  unsigned int m_lastModule;
  unsigned int m_maxClusterSize = 999999999;
  float m_ltg[16 * VP::NSensors]; // 16*208 = 16*number of sensors
  std::array<float, VP::NSensorColumns> m_local_x{};
  std::array<float, VP::NSensorColumns> m_x_pitch{};
  float m_pixel_size;
};

GeometryCache rebuildGeometry(const Detector &description,
                              const shared_ptr<cond::ConditionsSlice> &slice)
{

  cout << "Listing conditions in slice" << endl;
  DetElement de = description.detector("VP");
  auto conds = slice->get(de);
  for (const auto &cond : conds)
  {
    cout << "Cond: ";
    cout << cond.ptr();
    cout << " iov: " << cond.iov().str();
    cout << " key: " << cond.key();
    cout << " Type: " << gaudi::get_condition_type(cond).name() << endl;
  }

  // Now looking for a specific condition
  Condition::key_type k = ConditionKey(de, gaudi::Keys::deKey).hash;
  cout << "Look up dynamic cond object - Key: " << k;
  gaudi::DeVP devp = slice->get(de, gaudi::Keys::deKey);
  cout << " type: " << gaudi::get_condition_type(devp).name() << std::endl;
  cout << "VP has " << devp.numberSensors() << " sensors" << endl;

  GeometryCache cache;
  const auto &s = devp.sensors().front();

  std::map<unsigned int, gaudi::XYZPoint> zmap;

  for (const auto &sensor : devp.sensors())
  {
    auto pos = sensor.toGlobal(gaudi::XYZPoint(-sensor.staticData().sizeX / 2.0,
                                               -sensor.staticData().sizeY / 2.0, 0)) *
               10.0;
    cout << "nb:" << sensor.isRight() << "/" << sensor.staticData().moduleNumber << "/"
    << sensor.staticData().sensorNumber << " ID: "<< sensor.sensorNumber() << " POS:" << pos << endl;
    zmap[sensor.sensorNumber()] = pos;
    //cout << "sensorpos[" << sensor.sensorNumber() << "]:" << sensor.toGlobal(gaudi::XYZPoint(0,0,0)) * 10.0  << endl;
  }

  for (int i = 0; i < 208; i++)
  {
    cout << "sensorpos[" << i << "]:" << zmap[i] << endl;
  }

  // we copy the data from the geometry object which holds doubles intor a local float array
  std::copy(s.staticData().local_x.begin(), s.staticData().local_x.end(), cache.m_local_x.begin());
  std::copy(s.staticData().x_pitch.begin(), s.staticData().x_pitch.end(), cache.m_x_pitch.begin());

  float ltg_rot_components[9];
  for (unsigned i = 0; i < 208; ++i)
  {
    // TODO:     // if (!sensor->isReadOut()) continue;
    const auto &sensor = devp.sensors()[i];
    // get the local to global transformation matrix and
    // store it in a flat float array of size 12.
    const TGeoHMatrix &localToGlobal = sensor.detectorAlignment().worldTransformation();
    cout << "=============================" << endl;
    localToGlobal.Print();
    TGeoTranslation t(-sensor.staticData().sizeX / 2.0, -sensor.staticData().sizeY / 2.0, 0.0);
    t.Print();

    TGeoHMatrix finalm = localToGlobal * t;
    finalm.Print();
    cout << "=============================" << endl;

    auto ltg_rot_components = finalm.GetRotationMatrix();
    for (int i=0; i< 9; i++) { cout << "|" << ltg_rot_components[i]; }
    cout << endl;
    auto ltg_trans = finalm.GetTranslation();
    for (int i=0; i< 3; i++) { cout << "|" << ltg_trans[i]; }
    cout << endl;

cout << "=============================" << endl;

    //sensor->geometry()->toGlobalMatrix().GetDecomposition(ltg_rot, ltg_trans);
    //ltg_rot.GetComponents(ltg_rot_components);
    cout << "SENSOR NUMBER:" << sensor.sensorNumber() << endl;
    unsigned idx = 16 * sensor.sensorNumber();
    cout << "idxstart:" << idx << endl;
  
    cache.m_ltg[idx++] = ltg_rot_components[0];
    cache.m_ltg[idx++] = ltg_rot_components[1];
    cache.m_ltg[idx++] = ltg_rot_components[2];
    cache.m_ltg[idx++] = ltg_rot_components[3];
    cache.m_ltg[idx++] = ltg_rot_components[4];
    cache.m_ltg[idx++] = ltg_rot_components[5];
    cache.m_ltg[idx++] = ltg_rot_components[6];
    cache.m_ltg[idx++] = ltg_rot_components[7];
    cache.m_ltg[idx++] = ltg_rot_components[8];
    cache.m_ltg[idx++] = ltg_trans[0] * 10.0; // mm
    cache.m_ltg[idx++] = ltg_trans[1] * 10.0;
    cache.m_ltg[idx++] = ltg_trans[2] * 10.0;
    cache.m_ltg[idx++] = 0;
    cache.m_ltg[idx++] = 0;
    cache.m_ltg[idx++] = 0;
    cache.m_ltg[idx++] = 0;
    cout << "idxend:" << idx << endl;
  
  }

  for (int i = 0; i < 16*208; i++)
  {
    cout << "cache[" << i << "]:" << ((abs(cache.m_ltg[i]) > 1e-15) ? cache.m_ltg[i] : 0.0) << endl;
  }

  // for (const auto &x : cache.m_x_pitch)
  // {
  //   cout << x << ",";
  // }
  // cout << endl;
  //     // {
  //     //   cout << x << ",";
  //     // }
  //cache.m_pixel_size = s->pixelSize(LHCb::VPChannelID(0, 0, 0, 0)).second;

  // int invalid_sensor_count = 0;
  // for (const auto &s : devp.sensors())
  // {
  //   if (s.isValid())
  //   {

  //     cout << "----\nisRight: " << s.isRight() << endl;
  //     cout << "Sensor number" << s.sensorNumber();
  //     cout << " local_x (" << s.staticData().local_x.size()
  //          << " entries):";

  //     // for (const auto &x : s.staticData().local_x)
  //     // {
  //     //   cout << x << ",";
  //     // }
  //     cout << endl;
  //     cout << "Sensor number" << s.sensorNumber();
  //     cout << " x_pitch (" << s.staticData().x_pitch.size()
  //          << " entries):";

  //     // for (const auto &x : s.staticData().x_pitch)
  //     // {
  //     //   cout << x << ",";
  //     // }
  //     cout << endl;
  //   }
  //   else
  //   {
  //     invalid_sensor_count++;
  //   }
  // }
  // cout << "Invalid sensor count:" << invalid_sensor_count << endl;
  // cout << "-------------------------------------------" << endl;
}

static long test_conditions_slice(Detector &description, int argc, char **argv)
{
  bool help = false;
  vector<string> detectorNames;
  for (int i = 0; i < argc && argv[i]; ++i)
  {
    if (argv[i][0] == '-' || argv[i][0] == '/')
    {
      if (0 == ::strncmp("-help", argv[i], 4))
        help = true;
      else if (0 == ::strncmp("-detector", argv[i], 4))
        detectorNames.push_back(argv[++i]);
      else
        help = true;
    }
  }
  if (help)
  {
    /// Help printout describing the basic command line interface
    cout << "Usage: -plugin <name> -arg [-arg]                                   \n"
            "     name:   factory name     LHCb_TEST_cond_content                \n"
            "     -detector <name>         Name of the sub-detector to analyze.  \n"
            "     -help                    Ahow this help.                       \n"
            "\tArguments given: "
         << arguments(argc, argv) << endl
         << flush;
    ::exit(EINVAL);
  }
  dd4hep::setPrintFormat("%-24s %5s %s");

  gaudi::DetectorDataService ds(description, detectorNames);
  cout << "Creation done" << endl;
  ds.initialize();
  cout << "Initialize done" << endl;
  for (size_t i = 1; i < 2; ++i)
  {
    std::cout << "===========================================" << std::endl;
    auto slice = ds.get_slice(i * 100);
    std::cout << ":::::::::::::::::::::::::::::::::::::::::::" << std::endl;
    slice = ds.get_slice(i * 100);
    std::cout << "-------------------------------------------" << std::endl;
    rebuildGeometry(description, slice);
  }

  ds.finalize();
}
DECLARE_APPLY(LHCb_TEST_condserv, test_conditions_slice) // All rights reserved.
