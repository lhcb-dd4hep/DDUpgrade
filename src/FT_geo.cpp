//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================
//
// Specialized generic detector constructor
// 
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/detail/DetectorInterna.h"
#include "DD4hep/Printout.h"
#include "XML/Utilities.h"
#include "Conditions/UpgradeTags.h"
#include "TClass.h"

using namespace std;
using namespace dd4hep;
using namespace dd4hep::detail;

#define HELPER_CLASS_DEFAULTS(x)                \
  x() = default;                                \
  x(x&&) = default;                             \
  x(const x&) = default;                        \
  x& operator=(const x&) = default;             \
  ~x() = default

namespace   {

  /// Helper class to build the FT detector of LHCb
  struct FTBuild : public xml::tools::VolumeBuilder  {
    // Debug flags: To be enables/disbaled by the <debug/> section in the detector.xml file.
    bool   m_build_passive   = true;
    bool   m_build_frames    = true;
    bool   m_build_quarters  = true;
    bool   m_build_layers    = true;
    bool   m_build_stations  = true;
    std::string m_attach_volume;

    // General constants used by several member functions to build the FT geometry
    double mod_size_x      = 0.0;  //  FT:ModuleSizeX
    double mod_size_y      = 0.0;  //  FT:ModuleSizeY
    double mod_size_z      = 0.0;  //  FT:ModuleSizeZ
    double wall_thick      = 0.0;  //  FT:WallThickness
    double mat_pitch_x     = 0.0;  //  FT:MatPitchX
    double mat_size_x      = 0.0;  //  FT:MatSizeX
    double mat_size_y      = 0.0;  //  FT:MatSizeY
    double mat_size_z      = 0.0;  //  FT:MatSizeZ
    double skin_size_x     = 0.0;  //  FT:SkinSizeX
    double skin_size_y     = 0.0;  //  FT:SkinSizeY
    double skin_size_z     = 0.0;  //  FT:SkinSizeZ
    double core_size_x     = 0.0;  //  FT:CoreSizeX
    double core_size_y     = 0.0;  //  FT:CoreSizeY
    double core_size_z     = 0.0;  //  FT:CoreSizeZ
    double station_size_x  = 0.0;  //  FT:StationSizeX
    double station_size_y  = 0.0;  //  FT:StationSizeY
    double station_size_z  = 0.0;  //  FT:StationSizeZ
    double endpiece_size_x = 0.0;  //  FT:EndpieceSizeX
    double endpiece_size_y = 0.0;  //  FT:EndpieceSizeY
    double endpiece_size_z = 0.0;  //  FT:EndpieceSizeZ
    double eps             = 0.0;  //  FT:eps
    double mod_pitch_x     = 0.0;  //  FT:ModulePitchX
    double layer_pitch_z   = 0.0;  //  FT:LayerPitchZ
    double cframe_pitch_z  = 0.0;  //  FT:CFramePitchZ
    double cframe_shift_y  = 0.0;  //  FT:CFrameShiftY
    double hole_size_x     = 0.0;  //  FT:HoleSizeX
    double dead_size_y     = 0.0;  //  FT:DeadSizeY
    double endplug_size_y  = 0.0;  //  FT:EndplugSizeY"
    double hole_leftx_size_y = 0.0;//  FT:HoleLeftXSizeY"
    double beam_hole_radius  = 0.0;//  FT:BeamHoleRadius
    
    struct Module  {
      double sign = 1.0, leftHoleSizeY = 0, rightHoleSizeY = 0;
      std::string left, right;
      Module(double sgn, double hl, double hr, const std::string& l, const std::string& r)
        : sign(sgn), leftHoleSizeY(hl), rightHoleSizeY(hr), left(l), right(r) {}
      HELPER_CLASS_DEFAULTS(Module);
    };
    map<string, Module>     m_modules;
    map<string, DetElement> m_detElements;

    /// Utility function to register detector elements for cloning. Will all be deleted!
    void registerDetElement(const std::string& nam, DetElement de);
    /// Utility function to access detector elements for cloning. Will all be deleted!
    DetElement detElement(const std::string& nam)  const;

    /// Initializing constructor
    FTBuild(Detector& description, xml_elt_t e, SensitiveDetector sens);
    FTBuild() = delete;
    FTBuild(FTBuild&&) = delete;
    FTBuild(const FTBuild&) = delete;
    FTBuild& operator=(const FTBuild&) = delete;
    
    /// Default destructor
    virtual ~FTBuild();
    void build_mats();
    void build_modules();
    void build_quarters();
    void build_layers();
    void build_stations();
    void build_detector();
  };

  void FTBuild::registerDetElement(const std::string& nam, DetElement de)   {
    m_detElements[nam] = de;
  }
  DetElement FTBuild::detElement(const std::string& nam)  const   {
    auto i = m_detElements.find(nam);
    if ( i == m_detElements.end() )   {
      printout(ERROR,"FT geometry","Attempt to access non-existing detector element %s",nam.c_str());
    }
    return (*i).second;
  }
  
  /// Initializing constructor
  FTBuild::FTBuild(Detector& dsc, xml_elt_t e, SensitiveDetector sens)
    : xml::tools::VolumeBuilder(dsc, e, sens)
  {
    // Process debug flags
    xml_comp_t x_dbg  = x_det.child(_U(debug), false);
    if ( x_dbg )   {
      for(xml_coll_t i(x_dbg,_U(item)); i; ++i)  {
        xml_comp_t c(i);
        string n = c.nameStr();
        if ( n == "build_passive"  ) m_build_passive   = c.attr<bool>(_U(value));
        if ( n == "build_frames"   ) m_build_frames    = c.attr<bool>(_U(value));
        if ( n == "build_quarters" ) m_build_quarters  = c.attr<bool>(_U(value));
        if ( n == "build_layers"   ) m_build_layers    = c.attr<bool>(_U(value));
        if ( n == "build_stations" ) m_build_stations  = c.attr<bool>(_U(value));
        if ( n == "attach_volume"  ) m_attach_volume   = c.attr<string>(_U(value));
        if ( n == "debug"          ) debug             = c.attr<bool>(_U(value));
        //cout << "Dbg:" << n << " " << c.attr<bool>(_U(value)) << endl;
      }
    }
    eps             = _toDouble("FT:eps");
    mod_pitch_x     = _toDouble("FT:ModulePitchX");
    mod_size_x      = _toDouble("FT:ModuleSizeX");
    mod_size_y      = _toDouble("FT:ModuleSizeY");
    mod_size_z      = _toDouble("FT:ModuleSizeZ");
    wall_thick      = _toDouble("FT:WallThickness");
    mat_pitch_x     = _toDouble("FT:MatPitchX");
    mat_size_x      = _toDouble("FT:MatSizeX");
    mat_size_y      = _toDouble("FT:MatSizeY");
    mat_size_z      = _toDouble("FT:MatSizeZ");
    skin_size_x     = _toDouble("FT:SkinSizeX");
    skin_size_y     = _toDouble("FT:SkinSizeY");
    skin_size_z     = _toDouble("FT:SkinSizeZ");
    core_size_x     = _toDouble("FT:CoreSizeX");
    core_size_y     = _toDouble("FT:CoreSizeY");
    core_size_z     = _toDouble("FT:CoreSizeZ");
    station_size_x  = _toDouble("FT:StationSizeX");
    station_size_y  = _toDouble("FT:StationSizeY");
    station_size_z  = _toDouble("FT:StationSizeZ");
    endpiece_size_x = _toDouble("FT:EndpieceSizeX");
    endpiece_size_y = _toDouble("FT:EndpieceSizeY");
    endpiece_size_z = _toDouble("FT:EndpieceSizeZ");
    layer_pitch_z   = _toDouble("FT:LayerPitchZ");
    cframe_pitch_z  = _toDouble("FT:CFramePitchZ");
    cframe_shift_y  = _toDouble("FT:CFrameShiftY");
    hole_size_x     = _toDouble("FT:HoleSizeX");
    dead_size_y     = _toDouble("FT:DeadSizeY");
    endplug_size_y  = _toDouble("FT:EndplugSizeY");
    hole_leftx_size_y = _toDouble("FT:HoleLeftXSizeY");
    beam_hole_radius  = _toDouble("T:BeamHoleRadius");
    m_modules["LeftV"]  = Module(-1., _toDouble("FT:HoleLeftVSizeY"),  0, "lvFibreShortMatLeftV",  "lvFibreMat");
    m_modules["RightV"] = Module( 1., 0, _toDouble("FT:HoleRightVSizeY"), "lvFibreMat", "lvFibreShortMatRightV");

    
    m_modules["LeftU"]  = Module(-1., _toDouble("FT:HoleLeftUSizeY"),  0, "lvFibreShortMatLeftU",  "lvFibreMat");
    m_modules["LeftX"]  = Module(-1., _toDouble("FT:HoleLeftXSizeY"),  0, "lvFibreShortMatLeftX",  "lvFibreMat");
    m_modules["RightU"] = Module( 1., 0, _toDouble("FT:HoleRightUSizeY"), "lvFibreMat", "lvFibreShortMatRightU");
    m_modules["RightX"] = Module( 1., 0, _toDouble("FT:HoleRightXSizeY"), "lvFibreMat", "lvFibreShortMatRightX");
  }

  FTBuild::~FTBuild()   {
    for(auto& d : m_detElements)
      destroyHandle(d.second);
  }
  
  void FTBuild::build_mats()   {
    PlacedVolume pv;
    Position pos;

    Box    mod_box(mod_size_x/2.0, mod_size_y/2.0, mod_size_z/2.0);
    Volume mod_vol("lvFTModuleFull", mod_box, description.air());
    mod_vol.setVisAttributes(description,"FT:Module");
    registerVolume(mod_vol.name(), mod_vol);

    Tube     mod_tube (0, hole_leftx_size_y, (mod_size_z+eps)/2.0);
    Material skin_mat = description.material("FT:Skin");
    Box      skin_box (skin_size_x/2.0, skin_size_y/2.0, skin_size_z/2.0);
    Tube     skin_tube(0, hole_leftx_size_y+wall_thick, skin_size_z/2.0 + eps);
    Material core_mat = description.material("FT:Honeycomb");
    Box      core_box (core_size_x/2.0, core_size_y/2.0, core_size_z/2.0);
    Tube     core_tube(0, hole_leftx_size_y+wall_thick, core_size_z/2.0 + eps);
    double   delta_angle = _toDouble("FT:HoleDeltaAngle");
    Material fibre_mat = description.material("FT:SciFibre");

    for(const auto& m : m_modules )   {
      const auto& p = m.second;
      double hole_size_y = _toDouble("FT:Hole"+m.first+"SizeY");
      Solid  mod_solid;
      if ( m_build_passive )  {
        double dy = -0.5*mod_size_y+hole_size_y-hole_leftx_size_y;
        pos = Position(p.sign*mod_pitch_x/2.0,dy,0);
        mod_solid = SubtractionSolid(mod_box, mod_tube, pos);
        mod_vol = Volume("lvFTModuleHole"+m.first, mod_solid, description.air());
      }
      else {
        mod_vol = Assembly("lvFTModuleHole"+m.first);
        mod_solid = mod_vol.solid();
      }
      mod_vol.setVisAttributes(description,"FT:Module");
      registerVolume(mod_vol.name(), mod_vol);
      printout(debug ? ALWAYS : DEBUG, "FT geometry", "+++ Building volume %-24s solid: %-24s material:%s",
               mod_vol.name(), mod_solid->IsA()->GetName(), mod_vol.material().name());
      
      if ( m_build_passive )  {
        pos.SetY(-0.5*skin_size_y + hole_size_y - hole_leftx_size_y);
        SubtractionSolid skin_solid(skin_box, skin_tube, pos);
        Volume           skin_vol("lvSkinHole"+m.first, skin_solid, skin_mat);
        skin_vol.setVisAttributes(description, "FT:Support");
        registerVolume(skin_vol.name(), skin_vol);
        printout(debug ? ALWAYS : DEBUG, "FT geometry", "+++ Building volume %-24s solid: %-24s material:%s",
                 skin_vol.name(), skin_solid->IsA()->GetName(), skin_mat.name());

        pos.SetY(-0.5*core_size_y + hole_size_y - hole_leftx_size_y);
        SubtractionSolid core_front_solid(core_box, core_tube, pos);
        Volume           core_front_vol("lvCoreHoleFront"+m.first, core_front_solid, core_mat);
        core_front_vol.setVisAttributes(description, "FT:Core");
        core_front_vol.placeVolume(volume("lvEndplug"), Position(0,0.5*(core_size_y-endplug_size_y),0));
        pos = Position(-0.5*p.sign*hole_size_x,
                       -0.5*(core_size_y-endpiece_size_y)+dead_size_y,
                       0.5*(core_size_z-endpiece_size_z));
        core_front_vol.placeVolume(volume("lvEndpiece3"), pos);
        pos = Position(0.5*p.sign*(core_size_x-hole_size_x),
                       -0.5*(core_size_y-endpiece_size_y)+hole_size_y+dead_size_y,
                       0.5*(core_size_z-endpiece_size_z));
        core_front_vol.placeVolume(volume("lvEndpiece1"), pos);
        registerVolume(core_front_vol.name(), core_front_vol);
        printout(debug ? ALWAYS : DEBUG, "FT geometry", "+++ Building volume %-24s solid: %-24s material:%s",
                 core_front_vol.name(), core_front_solid->IsA()->GetName(), core_mat.name());

        pos = Position(p.sign*0.5*mod_pitch_x, -0.5*core_size_y + hole_size_y - hole_leftx_size_y, 0.0);
        SubtractionSolid core_back_solid(core_box, core_tube, pos);
        Volume           core_back_vol("lvCoreHoleBack"+m.first, core_back_solid, core_mat);
        core_back_vol.setVisAttributes(description, "FT:Core");
        core_back_vol.placeVolume(volume("lvEndplug"), Position(0,0.5*(core_size_y-endplug_size_y),0));
        pos = Position(-0.5*p.sign*hole_size_x,
                       -0.5*(core_size_y-endpiece_size_y)+dead_size_y,
                       -0.5*(core_size_z-endpiece_size_z));
        core_back_vol.placeVolume(volume("lvEndpiece3"), pos);
        pos = Position( 0.5*p.sign*(core_size_x-hole_size_x),
                        -0.5*(core_size_y-endpiece_size_y)+hole_size_y+dead_size_y,
                        -0.5*(core_size_z-endpiece_size_z));
        core_back_vol.placeVolume(volume("lvEndpiece1"), pos);
        registerVolume(core_back_vol.name(), core_back_vol);
        printout(debug ? ALWAYS : DEBUG, "FT geometry", "+++ Building volume %-24s solid: %-24s material:%s",
                 core_back_vol.name(), core_back_solid->IsA()->GetName(), core_mat.name());

        Box    side_wall_solid(wall_thick/2.0, (skin_size_y-hole_size_y-wall_thick)/2.0, mod_size_z/2.0);
        Volume side_wall_vol("lvSideWall"+m.first, side_wall_solid, skin_mat);
        side_wall_vol.setVisAttributes(description,"FT:Support");
        registerVolume(side_wall_vol.name(), side_wall_vol);
        printout(debug ? ALWAYS : DEBUG, "FT geometry", "+++ Building volume %-24s solid: %-24s material:%s",
                 side_wall_vol.name(), side_wall_solid->IsA()->GetName(), skin_mat.name());
      
        double angle = std::asin((hole_size_y - hole_leftx_size_y)/hole_leftx_size_y);
        Tube   hole_wall_solid(hole_leftx_size_y, hole_leftx_size_y+wall_thick, mod_size_z/2.0,
                               0.5*(1.0+p.sign)*(M_PI/2.0 + delta_angle + angle),
                               M_PI/2.0 - delta_angle + angle);
        Volume hole_wall_vol("lvHoleWall"+m.first, hole_wall_solid, skin_mat);
        hole_wall_vol.setVisAttributes(description,"FT:Support");
        registerVolume(hole_wall_vol.name(), hole_wall_vol);
        printout(debug ? ALWAYS : DEBUG, "FT geometry", "+++ Building volume %-24s solid: %-24s material:%s",
                 hole_wall_vol.name(), hole_wall_solid->IsA()->GetName(), skin_mat.name());
      }
      
      cout << m.first << " : " << "FT:Hole"+m.first+"SizeY" << " = "
           << _toDouble("FT:Hole"+m.first+"SizeY") << "  "
           << (mat_size_y-hole_size_y)/2.0 << endl;
      if ( p.left != "lvFibreMat" )   {
        Box mat0_solid(mat_size_x/2.0, (mat_size_y-hole_size_y)/2.0, mat_size_z/2.0);
        Volume mat0_vol(p.left, mat0_solid, fibre_mat);
        mat0_vol.setVisAttributes(description, "FT:Fibre");
        mat0_vol.setSensitiveDetector(sensitive);
        registerVolume(mat0_vol.name(), mat0_vol);
        printout(debug ? ALWAYS : DEBUG, "FT geometry", "+++ Building volume %-24s solid: %-24s material:%s",
                 mat0_vol.name(), mat0_solid->IsA()->GetName(), fibre_mat.name());
      }
      if ( p.right != "lvFibreMat" )   {
        Box mat3_solid(mat_size_x/2.0, (mat_size_y-hole_size_y)/2.0, mat_size_z/2.0);
        Volume mat3_vol(p.right, mat3_solid, fibre_mat);
        mat3_vol.setVisAttributes(description, "FT:Fibre");
        mat3_vol.setSensitiveDetector(sensitive);
        registerVolume(mat3_vol.name(), mat3_vol);
        printout(debug ? ALWAYS : DEBUG, "FT geometry", "+++ Building volume %-24s solid: %-24s material:%s",
                 mat3_vol.name(), mat3_solid->IsA()->GetName(), fibre_mat.name());
      }
    }
  }
  
  void FTBuild::build_modules()   {
    Position     pos;
    PlacedVolume pv;
    RotationZYX  rot_Y(0,M_PI,0);
    Volume       mod_vol = volume("lvFTModuleFull");
    DetElement   de_mod("module", id);
    DetElement   de_mat;

    registerDetElement(mod_vol.name(), de_mod);
    if ( m_build_passive )  {
      pos = Position(0,-0.5*(mod_size_y-skin_size_y),-0.5*(mat_size_z+skin_size_z)-core_size_z);
      pv = mod_vol.placeVolume(volume("lvSkin"), pos);
      pos = Position(0,-0.5*(mod_size_y-core_size_y),-0.5*(mat_size_z+core_size_z));
      pv = mod_vol.placeVolume(volume("lvCore"), pos);
    }
    pos = Position(-1.5*mat_pitch_x, 0.5*(mod_size_y-mat_size_y), 0.0);
    pv = mod_vol.placeVolume(volume("lvFibreMat"), pos);
    pv.addPhysVolID("mat",0);
    de_mat = DetElement(de_mod, "Mat0", 0);
    de_mat.setPlacement(pv);

    pos = Position(-0.5*mat_pitch_x, 0.5*(mod_size_y-mat_size_y), 0.0);
    pv = mod_vol.placeVolume(volume("lvFibreMat"), pos);
    pv.addPhysVolID("mat",1);
    de_mat = DetElement(de_mod, "Mat1", 1);
    de_mat.setPlacement(pv);

    pos = Position( 0.5*mat_pitch_x, 0.5*(mod_size_y-mat_size_y), 0.0);
    pv = mod_vol.placeVolume(volume("lvFibreMat"), pos);
    pv.addPhysVolID("mat",2);
    de_mat = DetElement(de_mod, "Mat2", 2);
    de_mat.setPlacement(pv);

    pos = Position( 1.5*mat_pitch_x, 0.5*(mod_size_y-mat_size_y), 0.0);
    pv = mod_vol.placeVolume(volume("lvFibreMat"), pos);
    pv.addPhysVolID("mat",3);
    de_mat = DetElement(de_mod, "Mat3", 3);
    de_mat.setPlacement(pv);

    if ( m_build_passive )  {
      pos = Position( 0,-0.5*(mod_size_y-core_size_y), 0.5*(mat_size_z+core_size_z));
      pv = mod_vol.placeVolume(volume("lvCore"), Transform3D(rot_Y,pos));
      pos = Position( 0,-0.5*(mod_size_y-skin_size_y), 0.5*(mat_size_z+skin_size_z) + core_size_z);
      pv = mod_vol.placeVolume(volume("lvSkin"), Transform3D(rot_Y,pos));
      
      pos = Position( -0.5*(mod_size_x-wall_thick),-0.5*(mod_size_y-skin_size_y), 0.0);
      pv = mod_vol.placeVolume(volume("lvSideWall"), pos);
      pos = Position(  0.5*(mod_size_x-wall_thick),-0.5*(mod_size_y-skin_size_y), 0.0);
      pv = mod_vol.placeVolume(volume("lvSideWall"), pos);
    }
    for(const auto& m : m_modules )   {
      const auto& p = m.second;
      double      hole_size_y = _toDouble("FT:Hole"+m.first+"SizeY");

      de_mod  = DetElement("module",id);
      mod_vol = volume("lvFTModuleHole"+m.first);
      registerDetElement(mod_vol.name(), de_mod);
      
      if ( m_build_passive )  {
        pos = Position(0.0, -0.5*(mod_size_y-skin_size_y), -0.5*(mat_size_z+skin_size_z)-core_size_z);
        pv = mod_vol.placeVolume(volume("lvSkinHole"+m.first), pos);

        pos = Position(0.0, -0.5*(mod_size_y-core_size_y), -0.5*(mat_size_z+core_size_z));
        pv = mod_vol.placeVolume(volume("lvCoreHoleFront"+m.first), pos);
      }
      pos = Position(-1.5*mat_pitch_x, 0.5*(mod_size_y-mat_size_y+p.leftHoleSizeY), 0.0);
      pv = mod_vol.placeVolume(volume(p.left), pos);
      pv.addPhysVolID("mat",0);
      de_mat = DetElement(de_mod, "Mat0",0);
      de_mat.setPlacement(pv);

      pos = Position(-0.5*mat_pitch_x, 0.5*(mod_size_y-mat_size_y), 0.0);
      pv = mod_vol.placeVolume(volume("lvFibreMat"), pos);
      pv.addPhysVolID("mat",1);
      de_mat = DetElement(de_mod, "Mat1",1);
      de_mat.setPlacement(pv);

      pos = Position( 0.5*mat_pitch_x, 0.5*(mod_size_y-mat_size_y), 0.0);
      pv = mod_vol.placeVolume(volume("lvFibreMat"), pos);
      pv.addPhysVolID("mat",2);
      de_mat = DetElement(de_mod, "Mat2",2);
      de_mat.setPlacement(pv);

      pos = Position( 1.5*mat_pitch_x, 0.5*(mod_size_y-mat_size_y+p.rightHoleSizeY), 0.0);
      pv = mod_vol.placeVolume(volume(p.right), pos);
      pv.addPhysVolID("mat",3);
      de_mat = DetElement(de_mod, "Mat3",3);
      de_mat.setPlacement(pv);

      if ( m_build_passive )  {
        pos = Position(0.0, -0.5*(mod_size_y-core_size_y),  0.5*(mat_size_z+core_size_z));
        pv = mod_vol.placeVolume(volume("lvCoreHoleBack"+m.first), pos);

        pos = Position(0.0, -0.5*(mod_size_y-skin_size_y),  0.5*(mat_size_z+skin_size_z)+core_size_z);
        pv = mod_vol.placeVolume(volume("lvSkinHole"+m.first), pos);

        pos = Position(0.5*p.sign*(mod_size_x-wall_thick), -0.5*(mod_size_y - skin_size_y - hole_leftx_size_y - wall_thick), 0.0);
        pv = mod_vol.placeVolume(volume("lvSideWall"+m.first), pos);

        pos = Position(0.5*p.sign*mod_pitch_x, -0.5*mod_size_y + hole_size_y - hole_leftx_size_y, 0.0);
        pv = mod_vol.placeVolume(volume("lvHoleWall"+m.first), pos);
      }
    }
  }
  
  void FTBuild::build_quarters()   {
    struct Quarter  {
      double sign = 0;
      double stereo = 0;
      std::string special;
      Quarter(double sgn, double st, const std::string& s) : sign(sgn), stereo(st), special(s) {}
      HELPER_CLASS_DEFAULTS(Quarter);
    };
    Position pos;
    PlacedVolume pv;
    Volume     vol_full = volume("lvFTModuleFull");
    DetElement de_full = detElement("lvFTModuleFull");
    map<string, Quarter> quarters;
    double stereo_u   = _toDouble("FT:StereoAngleU");
    double stereo_v   = _toDouble("FT:StereoAngleV");
    double stereo_x   = _toDouble("FT:StereoAngleX");
    quarters["UNeg"]  = Quarter(-1., stereo_u, "RightU");
    quarters["UPos"]  = Quarter( 1., stereo_u, "LeftU");
    quarters["VNeg"]  = Quarter(-1., stereo_v, "RightV");
    quarters["VPos"]  = Quarter( 1., stereo_v, "LeftV");
    quarters["XNeg"]  = Quarter(-1., stereo_x, "RightX");
    quarters["XPos"]  = Quarter( 1., stereo_x, "LeftX");
    for(int i=5, nmod=6; i<=nmod; ++i)  {
      for(const auto& q : quarters )   {
        const auto& p = q.second;
        Assembly vol_quarter(_toString(i,"lvQuarter%d")+q.first);
        double sin_stereo = std::sin(p.stereo);
        double cos_stereo = std::cos(p.stereo);
        RotationZYX rot(p.stereo,0,0);
        DetElement de_quarter("quarter",id);
        DetElement de, de_module;

        registerVolume(vol_quarter.name(), vol_quarter);
        registerDetElement(vol_quarter.name(), de_quarter);
        vol_quarter.setVisAttributes(description, "FT:Quarter");

        printout(debug ? ALWAYS : DEBUG,
                 "FT geometry","+++ Building quarter: %s sign:%c stereo:%.3f special:%s",
                 vol_quarter.name(), p.sign ? '+' : '-', p.stereo, p.special.c_str());
        pos = Position(p.sign*0.5*mod_pitch_x/cos_stereo-0.5*mod_size_y*sin_stereo, mod_size_y*cos_stereo/2.0, 0.0);
        pv  = vol_quarter.placeVolume(volume("lvFTModuleHole"+p.special), Transform3D(rot,pos));
        de  = detElement(pv.volume().name());
        pv.addPhysVolID("module",0);
        de_module = de.clone("M0",0);
        de_module.setPlacement(pv);
        de_quarter.add(de_module);

        pos.SetX(pos.X() + p.sign*mod_pitch_x/cos_stereo);
        pv = vol_quarter.placeVolume(vol_full, Transform3D(rot,pos));
        pv.addPhysVolID("module",1);
        de_module = de_full.clone("M1",1);
        de_module.setPlacement(pv);
        de_quarter.add(de_module);

        pos.SetX(pos.X() + p.sign*mod_pitch_x/cos_stereo);
        pv = vol_quarter.placeVolume(vol_full, Transform3D(rot,pos));
        pv.addPhysVolID("module",2);
        de_module = de_full.clone("M2",2);
        de_module.setPlacement(pv);
        de_quarter.add(de_module);

        pos.SetX(pos.X() + p.sign*mod_pitch_x/cos_stereo);
        pv = vol_quarter.placeVolume(vol_full, Transform3D(rot,pos));
        pv.addPhysVolID("module",3);
        de_module = de_full.clone("M3",3);
        de_module.setPlacement(pv);
        de_quarter.add(de_module);

        pos.SetX(pos.X() + p.sign*mod_pitch_x/cos_stereo);
        pv = vol_quarter.placeVolume(vol_full, Transform3D(rot,pos));
        pv.addPhysVolID("module",4);
        de_module = de_full.clone("M4",4);
        de_module.setPlacement(pv);
        de_quarter.add(de_module);

        if ( i == 5 ) continue;  // Distinguish between the quarters with 5 and 6 modules
        pos.SetX(pos.X() + p.sign*mod_pitch_x/cos_stereo);
        pv = vol_quarter.placeVolume(vol_full, Transform3D(rot,pos));
        pv.addPhysVolID("module",5);
        de_module = de_full.clone("M5",5);
        de_module.setPlacement(pv);
        de_quarter.add(de_module);
      }
    }
  }

  void FTBuild::build_layers()   {
    struct LayerType  {
      double rotY = 0;
      std::string A, B;
      LayerType(double rot, const std::string& a, const std::string& b) : rotY(rot), A(a), B(b) {}
      HELPER_CLASS_DEFAULTS(LayerType);
    };
    PlacedVolume pv;
    SubtractionSolid layer_solid; {
      // Box  box(station_size_x/2.0, station_size_y/2.0, station_size_z/2.0);
      // Tube tub(0, beam_hole_radius, station_size_z/2.0+eps);
      Box  box(station_size_x/2.0, station_size_y/2.0, mod_size_z/2.0+eps);
      Tube tub(0, beam_hole_radius, mod_size_z+eps);
      layer_solid = SubtractionSolid(box, tub);
    }
    map<string, LayerType> layers;

    layers["5U"]  = LayerType(0,   "lvQuarter5UPos","lvQuarter5UNeg");
    layers["5V"]  = LayerType(M_PI,"lvQuarter5VNeg","lvQuarter5VPos");
    layers["5X1"] = LayerType(M_PI,"lvQuarter5XNeg","lvQuarter5XPos");
    layers["5X2"] = LayerType(0,   "lvQuarter5XPos","lvQuarter5XNeg");
    layers["6U"]  = LayerType(0,   "lvQuarter6UPos","lvQuarter6UNeg");
    layers["6V"]  = LayerType(M_PI,"lvQuarter6VNeg","lvQuarter6VPos");
    layers["6X1"] = LayerType(M_PI,"lvQuarter6XNeg","lvQuarter6XPos");
    layers["6X2"] = LayerType(0,   "lvQuarter6XPos","lvQuarter6XNeg");
    Volume vol;
    for ( const auto& l : layers )   {
      const auto& p = l.second;
      // This volume declaration makes Geant4 extremely unhappy!!!!
      Volume     vol_layer("lvLayer"+l.first, layer_solid, description.air());
      //Assembly   vol_layer("lvLayer"+l.first);
      DetElement de_layer(l.first,id);
      DetElement de_quarter;
      registerVolume(vol_layer.name(), vol_layer);
      registerDetElement(vol_layer.name(), de_layer);
      vol_layer.setVisAttributes(description, "FT:Layer");
      printout(debug ? ALWAYS : DEBUG,
               "FT geometry", "+++ Building layer:   %-24s   A-type: %s   B-type: %s",
               vol_layer.name(), p.A.c_str(), p.B.c_str());
      pv = vol_layer.placeVolume(vol=volume(p.A),RotationZYX(M_PI,p.rotY,0));
      pv.addPhysVolID("quarter",0);
      de_quarter = detElement(vol.name()).clone("Q0",0);
      de_quarter.setPlacement(pv);
      de_layer.add(de_quarter);

      pv = vol_layer.placeVolume(vol=volume(p.B),RotationZYX(M_PI,p.rotY,0));
      pv.addPhysVolID("quarter",1);
      de_quarter = detElement(vol.name()).clone("Q1",1);
      de_quarter.setPlacement(pv);
      de_layer.add(de_quarter);

      pv = vol_layer.placeVolume(vol=volume(p.B),RotationZYX(   0,p.rotY,0));
      pv.addPhysVolID("quarter",2);
      de_quarter = detElement(vol.name()).clone("Q2",2);
      de_quarter.setPlacement(pv);
      de_layer.add(de_quarter);

      pv = vol_layer.placeVolume(vol=volume(p.A),RotationZYX(   0,p.rotY,0));
      pv.addPhysVolID("quarter",3);
      de_quarter = detElement(vol.name()).clone("Q3",3);
      de_quarter.setPlacement(pv);
      de_layer.add(de_quarter);
    }
  }

  void FTBuild::build_stations()   {
    Box  box(station_size_x/2.0, station_size_y/2.0, station_size_z/2.0);
    Tube tub(0.0, beam_hole_radius, station_size_z/2.0 + eps);
    SubtractionSolid solid(box,tub);
    PlacedVolume pv;
    Position pos;
    Volume vol;
    for(int i=5; i<=6; ++i)   {
      Volume vol_station(_toString(i,"lvStation%d"), solid, description.air());
      //Assembly vol_station(_toString(i,"lvStation%d"));
      DetElement de_station("station", id);
      registerDetElement(vol_station.name(), de_station);
      registerVolume(vol_station.name(), vol_station);

      printout(debug ? ALWAYS : DEBUG,
               "FT geometry", "+++ Building station type: %s",vol_station.name());

      if ( m_build_frames )   {      /// Need to place CFrame here
        vol_station.placeVolume(volume("lvCFramePair"),
                                Position(0, 0.5*cframe_shift_y, -0.5*cframe_pitch_z));
        vol_station.placeVolume(volume("lvCFramePair"),
                                Position(0, 0.5*cframe_shift_y,  0.5*cframe_pitch_z));
      }
      pos = Position(0, 0, -0.5*(cframe_pitch_z+layer_pitch_z));
      pv = vol_station.placeVolume(vol=volume(_toString(i,"lvLayer%dX1")),pos);
      pv.addPhysVolID("layer",0);
      DetElement de_layer0 = detElement(vol.name()).clone("X1",0);
      de_layer0.setPlacement(pv);
      de_station.add(de_layer0);
      printout(INFO,"FT","Layer 0 z=%.3f -z=%.3f +z=%.3f [cm]", pos.Z(),
               pos.Z()-pv.volume().boundingBox().z(),
               pos.Z()+pv.volume().boundingBox().z());

      pos = Position(0, 0, -0.5*(cframe_pitch_z-layer_pitch_z));
      pv = vol_station.placeVolume(vol=volume(_toString(i,"lvLayer%dU")),pos);
      pv.addPhysVolID("layer",1);
      DetElement de_layer1 = detElement(vol.name()).clone("U",1);
      de_layer1.setPlacement(pv);
      de_station.add(de_layer1);
      printout(INFO,"FT","Layer 1 z=%.3f -z=%.3f +z=%.3f [cm]", pos.Z(),
               pos.Z()-pv.volume().boundingBox().z(),
               pos.Z()+pv.volume().boundingBox().z());

      pos = Position(0, 0, +0.5*(cframe_pitch_z-layer_pitch_z));
      pv = vol_station.placeVolume(vol=volume(_toString(i,"lvLayer%dV")),pos);
      pv.addPhysVolID("layer",2);
      DetElement de_layer2 = detElement(vol.name()).clone("V",2);
      de_layer2.setPlacement(pv);
      de_station.add(de_layer2);
      printout(INFO,"FT","Layer 2 z=%.3f -z=%.3f +z=%.3f [cm]", pos.Z(),
               pos.Z()-pv.volume().boundingBox().z(),
               pos.Z()+pv.volume().boundingBox().z());

      pos = Position(0, 0, +0.5*(cframe_pitch_z+layer_pitch_z));
      pv = vol_station.placeVolume(vol=volume(_toString(i,"lvLayer%dX2")),pos);
      pv.addPhysVolID("layer",3);
      DetElement de_layer3 = detElement(vol.name()).clone("X2",3);
      de_layer3.setPlacement(pv);
      de_station.add(de_layer3);
      printout(INFO,"FT","Layer 3 z=%.3f -z=%.3f +z=%.3f [cm]", pos.Z(),
               pos.Z()-pv.volume().boundingBox().z(),
               pos.Z()+pv.volume().boundingBox().z());
    }      
  }
  
  void FTBuild::build_detector()   {
    PlacedVolume     pv;
    double           tm_size_x  = _toDouble("T:TMSizeX");
    double           tm_size_y  = _toDouble("T:TMSizeY");
    double           tm_size_z  = _toDouble("T:TMSizeZ");
    double           beam_angle = _toDouble("T:BeamAngle");
    Box              ft_box1(tm_size_x/2.0, tm_size_y/2.0, tm_size_z/2.0);
    Box              ft_box2(tm_size_x/2.0 + eps, tm_size_y/2.0 + eps, "20*mm");
    Tube             ft_tube(0, beam_hole_radius, tm_size_z/2.0 + eps, 2.0*M_PI);
    SubtractionSolid ft_solid(ft_box1, ft_tube);
    RotationZYX      rot(0,0,beam_angle);
    Position         pos(0,-beam_hole_radius, (tm_size_z+20.0*mm)/2.0);
    Volume           ft_vol("lvFT", SubtractionSolid(ft_solid,ft_box2,Transform3D(rot,pos)), description.air());
    //Assembly ft_vol("lvFT");
    Volume           vol;
    DetElement       de;

    ft_vol.setVisAttributes(description, xml_det_t(x_det).visStr());
    sensitive.setType("tracker");
    buildVolumes(x_det);
    placeDaughters(detector, ft_vol, x_det);
    build_mats();
    build_modules();
    if ( m_build_quarters )   {
      build_quarters();
      if ( m_build_layers )   {
        build_layers();
        if ( m_build_stations )   {
          build_stations();
        }
      }
    }
    if ( m_attach_volume.empty() && m_build_stations )   {
      Volume vol_station5 = volume("lvStation5");
      Volume vol_station6 = volume("lvStation6");
      DetElement  de_station;
      
      pos = Position(0, 0,_toDouble("FT:T1ZPos"));
      rot = RotationZYX(0, 0, beam_angle);
      pv = ft_vol.placeVolume(vol_station5, Transform3D(rot,pos));
      pv.addPhysVolID("station",0);
      de_station = detElement(vol_station5.name()).clone("T1",0);
      detector.add(de_station);
      printout(debug ? ALWAYS : DEBUG,
               "FT geometry", "+++ Position station T1 of type %s",vol_station5.name());
      
      pos = Position(0, 0,_toDouble("FT:T2ZPos"));
      pv = ft_vol.placeVolume(vol_station5, Transform3D(rot,pos));
      pv.addPhysVolID("station",1);
      de_station = detElement(vol_station5.name()).clone("T2",1);
      detector.add(de_station);
      printout(debug ? ALWAYS : DEBUG,
               "FT geometry", "+++ Position station T2 of type %s",vol_station5.name());
      
      pos = Position(0, 0,_toDouble("FT:T3ZPos"));
      pv = ft_vol.placeVolume(vol_station6, Transform3D(rot,pos));
      pv.addPhysVolID("station",2);
      de_station = detElement(vol_station6.name()).clone("T3",2);
      detector.add(de_station);
      printout(debug ? ALWAYS : DEBUG,
               "FT geometry", "+++ Position station T3 of type %s",vol_station6.name());
    }
    else if ( !m_attach_volume.empty() )   {
      // Test display of individual entities
      ft_vol.placeVolume(vol=volume(m_attach_volume));
      de = detElement(vol.name());
      detector.add(de.clone(m_attach_volume));
    }
    else   {
      except("FT geometry","+++ Inconsistent build flags. "
             "If no specific volume should be ejected, the stations must be built!");
    }
    xml_h x_tr = x_det.child(_U(transformation));
    pv = placeDetector(ft_vol, x_tr);
    pv.addPhysVolID("system", id);
  }
}

static Ref_t create_element(Detector& description, xml_h e, SensitiveDetector sens_det)  {
  FTBuild builder(description, e, sens_det);
  builder.build_detector();
  return builder.detector;
}
DECLARE_DETELEMENT(LHCb_FT_geo,create_element)
