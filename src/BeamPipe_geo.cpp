#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"

using namespace std;
using namespace dd4hep;
using namespace dd4hep::detail;

// static Volume add_vaccuum(Cone c, Material mat, std::string name, Detector &description)
// {
//   Material vaccuum(description.material("Vacuum"));
//   Cone ext_cone(c->GetDz(),
//                 0, c->GetRmax1(),
//                 0, c->GetRmax2());
//   Volume ext_cone_vol(name + "_vaccuum", ext_cone, vaccuum);
//   ext_cone_vol.setVisAttributes(description, "VacuumVis");
//   Volume int_cone_vol(name, c, mat);
//   int_cone_vol.setVisAttributes(description, "BeamPipeVis");
//   Transform3D trb(Position(0.0, 0.0, 0.0));
//   ext_cone_vol.placeVolume(int_cone_vol, trb);
//   return ext_cone_vol;
// }

static Volume add_vaccuum(Cone c, Material mat, std::string name, Detector &description)
{
  Material vaccuum(description.material("Vacuum"));
  Cone int_cone(c->GetDz(),
                0, c->GetRmin1(),
                0, c->GetRmin2());
  Volume int_cone_vol(name + "_vaccuum", int_cone, mat);
  int_cone_vol.setVisAttributes(description, "VacuumVis");
  
  Volume ext_cone_vol(name, c, mat);
  ext_cone_vol.setVisAttributes(description, "BeamPipeVis");
  Transform3D trb(Position(0.0, 0.0, 0.0));
  ext_cone_vol.placeVolume(int_cone_vol, trb);
  return ext_cone_vol;
}

static Ref_t create_section1(Detector &description)
{
  // Now creating a cone with Vacuum inside
  Material be(description.material("Be"));
  Cone section1(400.0, 10.0, 20.0, 100.0, 110.0);
  Volume section1_vol = add_vaccuum(section1, be, "section1", description);
  return section1_vol;
}

static Ref_t create_element(Detector &description, xml_h e, Ref_t /* sens */)
{
  // Preparing the detector element based on the XML received
  xml_det_t x_det(e);
  int det_id = x_det.id();
  string det_name = x_det.nameStr();
  DetElement det(det_name, det_id);
  Volume mother = description.pickMotherVolume(det);

  // The Beam pipe is n aembly of sections
  //Assembly env_vol(det_name);
  //env_vol.setVisAttributes(description, x_det.visStr());

  // Adding the first section
  auto section1 = create_section1(description);
  
  xml_dim_t pos = x_det.position();
  Transform3D tr(Position(pos.x(), pos.y(), pos.z()));
  PlacedVolume pv = mother.placeVolume(section1, tr);
  det.setPlacement(pv);
  return det;
}

DECLARE_DETELEMENT(DD4hep_BeamPipe_detector, create_element)
