//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================
//
// Specialized generic detector constructor
// 
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "DD4hep/ExtensionEntry.h"
#include "DD4hep/ConditionDerived.h"
#include "Conditions/ConditionsRepository.h"
#include "Detector/Common/DetectorElement.h"
#include "Detector/Common/DeAlignmentCall.h"

// C/C++ include files
#include <memory>

static long create_conditions_recipe(dd4hep::Detector& description, xml_h /* e */)  {
  typedef std::shared_ptr<gaudi::ConditionsRepository> Ext_t;
  auto world = description.world();
  auto req   = world.extension<Ext_t>(false);
  if ( !req )   {
    req = new Ext_t(new gaudi::ConditionsRepository());
    world.addExtension(new dd4hep::detail::DeleteExtension<Ext_t, Ext_t>(req));
  }
  dd4hep::cond::DependencyBuilder builder(world,
                                          gaudi::Keys::alignmentsComputedKey,
                                          new gaudi::DeAlignmentCall(world));
  auto* dep = builder.release();
  dep->target.hash = gaudi::Keys::alignmentsComputedKey;
  (*req)->addGlobal(dep);
  dd4hep::printout(dd4hep::INFO,"Alignments",
                   "++ Add dependency: alignmentsComputedKey: %s %016llX",
                   world.path().c_str(), dep->target.hash);
  return 1;
}
DECLARE_XML_DOC_READER(LHCb_Align_cond,create_conditions_recipe)
