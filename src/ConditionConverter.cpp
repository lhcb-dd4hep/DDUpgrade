//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================

// Framework includes
#include "DD4hep/Factories.h"
#include "Conditions/ConditionConverter.h"

using namespace std;

namespace dd4hep  {

  /// Conversion Utilities 
  string clean_cond_data(char pre, const string& data, char post)  {
    string d = pre+data+" ";
    size_t idx, idq;
    for(idx = d.find_first_not_of(' ',1); idx != string::npos;)  {
      if ( ::isspace(d[idx]) ) d[idx] = ' ';
      idx = d.find_first_not_of(' ',++idx);
    }
    for(idx = d.find_first_not_of(' ',1); idx != string::npos; ++idx)  {
      if ( d[idx] != ' ' && ::isspace(d[idx]) ) d[idx] = ' ';
      idq = d.find_first_of(' ',idx);
      if ( idq != string::npos )  {
        idx = d.find_first_not_of(' ',idq);
        if ( idx == string::npos ) break;
        if ( d[idx] != ' ' && ::isspace(d[idx]) ) d[idx] = ' ';
        d[idq] = ',';
        continue;
      }
      break;
    }
    d[d.length()-1] = post;
    return d;
  }

  /// Specialized conversion of <param/> and <paramVector> entities in alignments
  template <> void Converter<Delta>::operator()(xml_h element) const {
    string        nam = element.attr<string>(_U(name));
    string       data = clean_cond_data('(',element.text(),')');
    Delta* a = _option<Delta>();
    Position pos;
    const BasicGrammar& g = BasicGrammar::instance<Position>();

    if ( !g.fromString(&pos,data) ) g.invalidConversion(data, g.type());
    if ( nam == "dPosXYZ" )  {
      a->translation = pos/10.0;
      a->flags |= Delta::HAVE_TRANSLATION;
    }
    else if ( nam == "dRotXYZ" )   {
      a->rotation = RotationZYX(pos.z(),pos.y(),pos.x());
      a->flags |= Delta::HAVE_ROTATION;
    }
    else if ( nam == "pivotXYZ" )   {
      a->pivot = Translation3D(pos.x(),pos.y(),pos.z());
      a->flags |= Delta::HAVE_PIVOT;
    }
    else   {
      printout(ERROR,"Delta","++ Unknown alignment conditions tag: %s",nam.c_str());
    }
  }
    
  /// Specialized conversion of <posXYZ/> entities
  template <> void Converter<Position>::operator()(xml_h element) const {
    xml_dim_t  dim = element;
    Position*  pos = _option<Position>();
    pos->SetXYZ(dim.x(0.0), dim.y(0.0), dim.z(0.0));
  }
#if 0
  /// Specialized conversion of <posRPhiZ/> entities
  template <> void Converter<PositionRPhiZ>::operator()(xml_h element) const {
    xml_dim_t  dim = element;
    ROOT::Math::Polar2D<double> dim2(dim.r(0.0), dim.phi(0.0));
    Position*  pos = _option<Position>();
    pos->SetXYZ(dim2.X(), dim2.Y(), dim.z(0.0));
  }
  /// Specialized conversion of <rotXYZ/> entities
  template <> void Converter<RotationZYX>::operator()(xml_h element) const {
    xml_dim_t    dim = element;
    RotationZYX* rot = _option<RotationZYX>();
    rot->SetComponents(dim.rotZ(0.0), dim.rotY(0.0), dim.rotX(0.0));
  }
#endif
  namespace {
  }
}

/// Namespace for the AIDA detector description toolkit
namespace gaudi {

  using namespace dd4hep;
  
  /// Alignment conditions
  /**
   *  Form:
   *
   *    <condition classID="6" name="FTSystem">
   *      <paramVector name="dPosXYZ" type="double">0 0 0</paramVector>
   *      <paramVector name="dRotXYZ" type="double">0 0 0</paramVector>
   *    </condition>
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup LHCb_CONDITIONS
   */
  template <> Condition
  ConcreteConditionConverter<6>::operator()(Detector& description, ConditionIdentifier*, xml_h e)  const {
    xml_dim_t element = e;
    Condition cond(element.nameStr(),"alignment");
    Converter<Delta> conv(description,0,&cond.bind<Delta>());
    xml_coll_t(element,_LBU(paramVector)).for_each(conv);
    cond->setFlag(Condition::ALIGNMENT_DELTA);
    return cond;
  }

  /// TH2D conditions
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup LHCb_CONDITIONS
   */
  template <> Condition
  ConcreteConditionConverter<1008188>::operator()(Detector& description, ConditionIdentifier*, xml_h e)  const {
    xml_dim_t element = e;
    Condition cond(element.nameStr(),"alignment");
    Converter<Delta> conv(description,0,&cond.bind<Delta>());
    xml_coll_t(element,_LBU(paramVector)).for_each(conv);
    cond->setFlag(Condition::ALIGNMENT_DELTA);
    return cond;
  }
}
//==========================================================================
/// Plugin function
template <int TYPE>
static void* create_conditions_converter(dd4hep::Detector&, int, char**)   {
  gaudi::ConditionConverter* o = new gaudi::ConcreteConditionConverter<TYPE>();
  return o;
}
// These 2 are the same
DECLARE_DD4HEP_CONSTRUCTOR(LHCb_ConditionsConverter_6,create_conditions_converter<6>)
DECLARE_DD4HEP_CONSTRUCTOR(LHCb_ConditionsConverter_1008106,create_conditions_converter<6>)

