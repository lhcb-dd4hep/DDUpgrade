//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================
//
// Specialized generic detector constructor
// 
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "XML/Utilities.h"
#include "Conditions/UpgradeTags.h"

using namespace std;
using namespace dd4hep;
using namespace dd4hep::detail;

namespace   {

  /// Helper class to build the UT detector of LHCb
  struct UTBuild : public xml::tools::VolumeBuilder  {
    // Debug flags
    bool              m_build_uta    = true;
    bool              m_build_utaU   = true;
    bool              m_build_utaX   = true;
    bool              m_build_utb    = true;
    bool              m_build_utbV   = true;
    bool              m_build_utbX   = true;
    bool              m_build_frame  = true;
    bool              m_build_box    = true;
    bool              m_build_jacket = true;
    bool              m_build_layers = true;
    bool              m_build_cxx    = true;

    /// Initializing constructor
    UTBuild(Detector& description, xml_elt_t e, SensitiveDetector sens);
    void build_environ(DetElement parent, Volume sdet_vol);
    void build_groups();
    void build_hybrids();
    void build_jacket();
    
    void build_modules();
    void build_layers(DetElement parent, Volume mother);
    Volume build_layer(DetElement parent,
                       const std::string& nam,
                       double angle,
                       int num_mod,
                       map<string, double>& positions,
                       double offsets[2],
                       double steps[2]);
  };

  /// Initializing constructor
  UTBuild::UTBuild(Detector& dsc, xml_elt_t e, SensitiveDetector sens)
    : xml::tools::VolumeBuilder(dsc, e, sens)
  {
    // Process debug flags
    xml_comp_t x_dbg  = x_det.child(_U(debug), false);
    if ( x_dbg )   {
      for(xml_coll_t i(x_dbg,_U(item)); i; ++i)  {
        xml_comp_t c(i);
        string n = c.nameStr();
        if      ( n == "Build_UTA"     ) m_build_uta     = c.attr<bool>(_U(value));
        else if ( n == "Build_UTA_U"   ) m_build_utaU    = c.attr<bool>(_U(value));
        else if ( n == "Build_UTA_X"   ) m_build_utaX    = c.attr<bool>(_U(value));
        else if ( n == "Build_UTB"     ) m_build_utb     = c.attr<bool>(_U(value));
        else if ( n == "Build_UTB_V"   ) m_build_utbV    = c.attr<bool>(_U(value));
        else if ( n == "Build_UTB_X"   ) m_build_utbX    = c.attr<bool>(_U(value));
        else if ( n == "Build_Jacket"  ) m_build_jacket  = c.attr<bool>(_U(value));
        else if ( n == "Build_Layers"  ) m_build_layers  = c.attr<bool>(_U(value));
        else if ( n == "Build_Frame"   ) m_build_frame   = c.attr<bool>(_U(value));
        else if ( n == "Build_Box"     ) m_build_box     = c.attr<bool>(_U(value));
        else if ( n == "debug"         ) debug           = c.attr<bool>(_U(value));
        //cout << "Dbg:" << n << " " << c.attr<bool>(_U(value)) << endl;
      }
    }
  }

  void UTBuild::build_groups()   {
    Assembly group;
    PlacedVolume pv;
    Position pos_metal(0, 0,      _toDouble("UTSensorSiThickness")/2.0);
    Position pos_guard(0, 0, -1.0*_toDouble("UTSensorAlThickness")/2.0);
    double   sens_width  = _toDouble("UTSensorWidthInner")/4.0;
    double   sens_height = _toDouble("UTSensorHeightInner")/4.0;
    double   sens_thick  = _toDouble("UTSensorAlThickness")/2.0;

    group = Assembly("lvGroupNorm");
    group.setVisAttributes(description, "UT:SensorGroup");
    group.placeVolume(volume("lvMetalLayer"), pos_metal);
    group.placeVolume(volume("lvGuardRing"),  pos_guard);
    pv = group.placeVolume(volume("lvSectorNorm"),  Position(0,0,-sens_thick));
    pv.addPhysVolID("sensor",1);
    registerVolume(group.name(),group);

    group = Assembly("lvGroupDual");
    group.setVisAttributes(description, "UT:SensorGroup");
    group.placeVolume(volume("lvMetalLayer"), pos_metal);
    group.placeVolume(volume("lvGuardRing"),  pos_guard);
    pv = group.placeVolume(volume("lvSectorDual"), Position(0,-sens_height,-sens_thick));
    pv.addPhysVolID("sensor",1);
    pv = group.placeVolume(volume("lvSectorDual"), Position(0, sens_height,-sens_thick));
    pv.addPhysVolID("sensor",2);
    registerVolume(group.name(),group);

    group = Assembly("lvGroupQuad");
    group.setVisAttributes(description, "UT:SensorGroup");
    group.placeVolume(volume("lvMetalLayer"), pos_metal);
    group.placeVolume(volume("lvGuardRing"),  pos_guard);
    pv = group.placeVolume(volume("lvSectorQuad"), Position(-sens_width,-sens_height,-sens_thick));
    pv.addPhysVolID("sensor",1);
    pv = group.placeVolume(volume("lvSectorQuad"), Position( sens_width,-sens_height,-sens_thick));
    pv.addPhysVolID("sensor",2);
    pv = group.placeVolume(volume("lvSectorQuad"), Position(-sens_width, sens_height,-sens_thick));
    pv.addPhysVolID("sensor",3);
    pv = group.placeVolume(volume("lvSectorQuad"), Position( sens_width, sens_height,-sens_thick));
    pv.addPhysVolID("sensor",4);
    registerVolume(group.name(),group);

    group = Assembly("lvGroupNormSlim");
    group.setVisAttributes(description, "UT:SensorGroup");
    group.placeVolume(volume("lvMetalLayerSlim"), pos_metal);
    group.placeVolume(volume("lvGuardRingSlim"),  pos_guard);
    pv = group.placeVolume(volume("lvSectorNormSlim"), Position(0,0,-sens_thick));
    pv.addPhysVolID("sensor",1);
    registerVolume(group.name(),group);

    group = Assembly("lvGroupDualSlim");
    group.setVisAttributes(description, "UT:SensorGroup");
    group.placeVolume(volume("lvMetalLayerSlim"), pos_metal);
    group.placeVolume(volume("lvGuardRingSlim"),  pos_guard);
    pv = group.placeVolume(volume("lvSectorDualSlim"), Position(0,-sens_height,-sens_thick));
    pv.addPhysVolID("sensor",1);
    pv = group.placeVolume(volume("lvSectorDualSlim"), Position(0, sens_height,-sens_thick));
    pv.addPhysVolID("sensor",2);
    registerVolume(group.name(),group);

    group = Assembly("lvGroupQuadSlim");
    group.setVisAttributes(description, "UT:SensorGroup");
    group.placeVolume(volume("lvMetalLayerSlimCut"), pos_metal);
    group.placeVolume(volume("lvGuardRingSlimCut"),  pos_guard);
    group.placeVolume(volume("lvSectorQuadSlim"),      Position(-sens_width,-sens_height,-sens_thick));
    pv.addPhysVolID("sensor",1);
    pv = group.placeVolume(volume("lvSectorQuadSlim"), Position( sens_width,-sens_height,-sens_thick));
    pv.addPhysVolID("sensor",2);
    pv = group.placeVolume(volume("lvSectorQuadSlim"), Position(-sens_width, sens_height,-sens_thick));
    pv.addPhysVolID("sensor",3);
    pv = group.placeVolume(volume("lvSectorQuadSlim"), Position( sens_width, sens_height,-sens_thick));
    pv.addPhysVolID("sensor",4);
    registerVolume(group.name(),group);
  }
  
  void UTBuild::build_hybrids()   {
    double hybrid_dx = _toDouble("UTHybridWidth/2");
    double hybrid_dy = _toDouble("UTHybridHeight/2");
    double hybrid_dz = _toDouble("UTHybridThickness/2");
    double asic_y    = _toDouble("UTHybridAsicY");
    Position pos_substrate(0,0,_toDouble("UTHybridSubstrateZ"));
    Position pos_sensor(0,_toDouble("UTHybridSensorY"),_toDouble("UTHybridSensorZ"));
    Box box_normal(hybrid_dx,hybrid_dy,hybrid_dz);
    Box box_slim(hybrid_dx,hybrid_dy,hybrid_dz);
    Volume vol;
    vol = Volume("lvHybridNorm", box_normal, description.air());
    vol.placeVolume(volume("lvSubstrate"), pos_substrate);
    vol.placeVolume(volume("lvGroupNorm"), pos_sensor);
    vol.placeVolume(volume("lvAsic4"), Position(0,asic_y,_toDouble("UTHybridAsicZbase-UTAsicThicknessAve*4/2")));
    registerVolume(vol.name(),vol);

    vol = Volume("lvHybridDual", box_normal, description.air());
    vol.placeVolume(volume("lvSubstrate"), pos_substrate);
    vol.placeVolume(volume("lvGroupDual"), pos_sensor);
    vol.placeVolume(volume("lvAsic8"), Position(0,asic_y,_toDouble("UTHybridAsicZbase-UTAsicThicknessAve*8/2")));
    registerVolume(vol.name(),vol);

    vol = Volume("lvHybridQuad", box_normal, description.air());
    vol.placeVolume(volume("lvSubstrate"), pos_substrate);
    vol.placeVolume(volume("lvGroupQuad"), pos_sensor);
    vol.placeVolume(volume("lvAsic16"), Position(0,asic_y,_toDouble("UTHybridAsicZbase-UTAsicThicknessAve*16/2")));
    registerVolume(vol.name(),vol);

    vol = Volume("lvHybridNormSlim", box_slim, description.air());
    vol.placeVolume(volume("lvSubstrateSlim"), pos_substrate);
    vol.placeVolume(volume("lvGroupNormSlim"), pos_sensor);
    vol.placeVolume(volume("lvAsic3Slim"), Position(0,asic_y,_toDouble("UTHybridAsicZbase-UTAsicThicknessAveSlim*3/2")));
    registerVolume(vol.name(),vol);

    vol = Volume("lvHybridDualSlim", box_slim, description.air());
    vol.placeVolume(volume("lvSubstrateSlim"), pos_substrate);
    vol.placeVolume(volume("lvGroupDualSlim"), pos_sensor);
    vol.placeVolume(volume("lvAsic6Slim"), Position(0,asic_y,_toDouble("UTHybridAsicZbase-UTAsicThicknessAveSlim*6/2")));
    registerVolume(vol.name(),vol);

    Tube tub(0,_toDouble("UTBeamHoleRadius"),box_slim->GetDZ()/2);
    SubtractionSolid solid(box_slim, tub, Position(0,_toDouble("UTHybridHeight/2+UTSensorMiddleDistY-UTHybridSensorEdge1"),0));
    vol = Volume("lvHybridQuadSlim", solid, description.air());
    vol.placeVolume(volume("lvSubstrateSlim"), pos_substrate);
    vol.placeVolume(volume("lvGroupDualSlim"), pos_sensor);
    vol.placeVolume(volume("lvAsic11Slim"), Position(0,asic_y,_toDouble("UTHybridAsicZbase-UTAsicThicknessAveSlim*11/2")));
    registerVolume(vol.name(),vol);
  }
  
  void UTBuild::build_modules()   {
    Box box_module("UTModuleWidth/2", "UTModuleHeight/2" ,"UTModuleThickness/2");
    Box b1("UTModuleWidthSlim/2","UTModuleHeightSlim/2","UTModuleThickness/2");
    Tube t1(0.0,"UTBeamHoleRadius","UTModuleThickness",2.0*M_PI);
    SubtractionSolid shap_mod_D(b1, t1, Position(0,_toDouble("UTModuleHeightSlim/2+UTSensorMiddleDistY-UTHybridSensorEdge1"),0));
    RotationZYX rot_Y(0,M_PI,0);
    RotationZYX rot_Z(0,0,M_PI);
    RotationZYX rot_YZ(0,M_PI,M_PI);
    double hybridSensorY      = _toDouble("UTHybridSensorY");
    double moduleHybridStepY0 = _toDouble("UTModuleHybridStepY0");
    double moduleHybridStepY1 = _toDouble("UTModuleHybridStepY1");
    double moduleHybridZ      = _toDouble("UTModuleHybridZ");
    double moduleSlimY7       = _toDouble("UTModuleSlimY7");
    double kapton_foil_z      = _toDouble("UTModuleKaptonFoilZ");
    Volume vol, dau;
    PlacedVolume pv;
    Position pos;
    stringstream str;
    char text[32];
    for(size_t ivol=0; ivol<4; ++ivol)   {
      switch(ivol)  {
      case 0:
        vol = Volume("lvModuleA", box_module, description.air());
        vol.placeVolume(volume("lvModuleSupport"));
        vol.placeVolume(volume("lvKaptonFoil"),Position(0,0, kapton_foil_z));
        vol.placeVolume(volume("lvKaptonFoil"),Position(0,0,-kapton_foil_z));
        break;
      case 1:
        vol = Volume("lvModuleB", box_module, description.air());
        vol.placeVolume(volume("lvModuleSupport"));
        vol.placeVolume(volume("lvKaptonFoil"),Position(0,0, kapton_foil_z));
        vol.placeVolume(volume("lvKaptonFoil"),Position(0,0,-kapton_foil_z));
        break;
      case 2:
        vol = Volume("lvModuleC", box_module, description.air());
        vol.placeVolume(volume("lvModuleSupport"));
        vol.placeVolume(volume("lvKaptonFoil"),Position(0,0, kapton_foil_z));
        vol.placeVolume(volume("lvKaptonFoil"),Position(0,0,-kapton_foil_z));
        break;
      case 3:
        vol = Volume("lvModuleD", shap_mod_D, description.air());
        vol.placeVolume(volume("lvModuleSupportSlim"));
        vol.placeVolume(volume("lvKaptonFoilSlim"),Position(0,0, kapton_foil_z));
        vol.placeVolume(volume("lvKaptonFoilSlim"),Position(0,0,-kapton_foil_z));
        break;
      default:
        except("UT","+++ Unknown Volume type:%d",ivol);
        break;
      }
      vol.setVisAttributes(description, "UT:Module");
      registerVolume(vol.name(), vol);
      if ( debug )  {
        printout(ALWAYS,"UT","+++ Building module: %s",vol.name());
      }
      for(int alt=-1, i = -6, ihybrid=1; i <= 6; ++i, ++ihybrid, alt *= -1)     {
        double hybrid_step_y = 0.0, offset = 0.0;
        if ( ivol == 3 && i == 1 ) break;
        switch(ivol)  {
        case 0:
        case 1:
        case 2:
          hybrid_step_y = alt > 0.0 ? moduleHybridStepY1 : moduleHybridStepY0;
          pos = Position(0,double(i)*hybrid_step_y + hybridSensorY, double(alt)*moduleHybridZ);
          break;
        case 3:
          hybrid_step_y = alt > 0.0 ? moduleHybridStepY1 : moduleHybridStepY0;
          offset = (i%2==0) ? _toDouble("UTModuleSlimYOffset1") : 0.0;
          pos = Position(0,double(i)*hybrid_step_y + moduleSlimY7 - offset, double(alt)*moduleHybridZ);
          break;
        default:
          pos = Position();
          break;
        }
        switch(ivol)  {
        case 1:
          if (      std::abs(i) <= 2 ) dau = volume("lvHybridDual");
          else                         dau = volume("lvHybridNorm");
          break;
        case 2:
          if (      std::abs(i) == 2 ) dau = volume("lvHybridDual");
          else if ( std::abs(i) <= 1 ) dau = volume("lvHybridQuad");
          else                         dau = volume("lvHybridNorm");
          break;
        case 3:
          if (      std::abs(i) <= 1 ) dau = volume("lvHybridDualSlim");
          else if (          i  == 0 ) dau = volume("lvHybridQuadSlim");
          else                         dau = volume("lvHybridNormSlim");
          break;
        case 0:
        default:
          dau = volume("lvHybridNorm");
          break; // all are "lvHybridNorm"
        }
        str << pos;
        if ( alt > 0 && i < 0 )   {
          str << " RotY  ";
          pv = vol.placeVolume(dau, Transform3D(rot_Y,pos));
        }
        else if ( alt > 0 )   {
          str << " RotYZ ";
          pv = vol.placeVolume(dau, Transform3D(rot_YZ,pos));
        }
        else if ( i > 0 )  {
          str << " RotZ  ";
          pv = vol.placeVolume(dau, Transform3D(rot_Z,pos));
        }
        else  {
          pv = vol.placeVolume(dau, pos);
        }
        if ( debug )   {
          printout(INFO,"UT","+++ Module: %s placing %s at %-24s (i,alt)=%d %d",
                   vol.name(), dau.name(), str.str().c_str(), i, alt);
        }
        str.str("");
        pv.addPhysVolID("hybrid", ihybrid);
        ::snprintf(text,sizeof(text),"pvHybrid_%c%d",char('A'+ivol),ihybrid);
        pv->SetName(text);
      }
    }
  }
  
  Volume UTBuild::build_layer(DetElement parent,
                              const std::string& nam,
                              double angle,
                              int    num_mod,
                              map<string, double>& position,
                              double offsets[2],
                              double steps[2])   {
    int mod_count = 0;
    Position pos;
    PlacedVolume pv;
    RotationZYX rot_Z1(angle, 0, 0);
    Box  box("UTVirtualBoxWidth/2","UTVirtualBoxHeight/2","UTLayerThickness/2");
    Tube tub(0.0,"UTVirtualHoleRadius","UTLayerThickness*1.1/2",2.0*M_PI);
    SubtractionSolid shape_layer(box, tub);
    Volume dau, vol_layer("lv"+nam,shape_layer,description.air());
    Volume vol_modA = volume("lvModuleA");
    Volume vol_modB = volume("lvModuleB");
    Volume vol_modC = volume("lvModuleC");
    Volume vol_modD = volume("lvModuleD");
    
    double module_dz     = _toDouble("UTModuleDz"); 
    double x_pos_regular = position["x_pos_regular"];
    double alt_z = (num_mod%2 == 0) ? 1.0 : -1.0;
    registerVolume("lv"+nam, vol_layer);

    // <!-- Region 1 at -X and Region2 at -X -->
    for(int i=-num_mod, idx=abs(i%2); i <= 0; ++i, idx=abs(i%2))   {
      DetElement module(parent, _toString(i,"layer-%d"), parent.id());
      if      ( i ==  0 ) dau = vol_modC;
      else if ( i == -1 ) dau = vol_modB;
      else dau = vol_modA;
      pos = Position(-x_pos_regular + i*steps[idx] - offsets[idx], 0, alt_z*module_dz/2.0);
      pv = vol_layer.placeVolume(dau, Transform3D(rot_Z1,pos));
      pv.addPhysVolID("module", ++mod_count);
      module.setPlacement(pv);

      if ( debug )   {
        stringstream str;
        str << pos;
        printout(INFO,"UT","+++ %s: position module %s at %s %.3g %.3g %.3g  %f",
                 vol_layer.name(), dau.name(), str.str().c_str(),  double(i)*steps[idx], offsets[idx],
                 double(i)*steps[idx] - offsets[idx], alt_z);
      }
      alt_z *= -1.0;
    }

    // <!-- Region 2 in the middle -->
    double x_pos_middle = position["x_pos_middle"];
    double y_pos_middle = position["y_pos_middle"];

    DetElement mod_m0(parent, "middle_0", parent.id());
    pos = Position(-x_pos_middle,-y_pos_middle, -module_dz/2.0);
    pv  = vol_layer.placeVolume(vol_modD, Transform3D(rot_Z1,pos));
    pv.addPhysVolID("module", ++mod_count);
    mod_m0.setPlacement(pv);

    DetElement mod_m1(parent, "middle_1", parent.id());
    pos = Position( x_pos_middle, y_pos_middle, -module_dz/2.0);
    pv  = vol_layer.placeVolume(vol_modD, Transform3D(RotationZYX(M_PI+angle,0,0),pos));
    pv.addPhysVolID("module", ++mod_count);
    mod_m1.setPlacement(pv);

    // <!-- Region 2 at +X and Region 3 at +X -->
    alt_z = 1.0;
    for(int i=0, idx=abs(i%2); i<num_mod; ++i, idx=abs(i%2))   {
      DetElement module(parent, _toString(i,"layer+%d"), parent.id());
      if      ( i ==  0 ) dau = vol_modC;
      else if ( i ==  1 ) dau = vol_modB;
      else dau = vol_modA;
      pos = Position(x_pos_regular + i*steps[idx] + offsets[idx], 0, alt_z*module_dz/2.0);
      pv  = vol_layer.placeVolume(dau, Transform3D(rot_Z1,pos));
      pv.addPhysVolID("module", ++mod_count);
      module.setPlacement(pv);
      alt_z *= -1.0;
    }
    return vol_layer;
  }

  void UTBuild::build_layers(DetElement parent, Volume utvol)   {
    Position pos;
    Volume   vol;
    double   angle      = _toDouble("UTAngle");
    double   layer_dz   = _toDouble("UTLayerDz");
    double   station_dz = _toDouble("UTStationDz");
    Box  box("UTVirtualBoxWidth/2","UTVirtualBoxHeight/2","UTStationThickness/2");
    Tube tub(0.0,"UTVirtualHoleRadius","UTStationThickness*1.1/2",2.0*M_PI);
    SubtractionSolid station_shape(box, tub);
    //------- x_pos_regular      x_pos_middle    x_pos_regular
    //                            Rg 2 middle
    // aU -> -UTModuleXposULR , UTModuleXposUTB, UTModuleXposULR
    //-------                    y_pos_middle
    //                0       -+UTModuleYposUTB,         0
    // bV -> -UTModuleXposULR , UTModuleXposUTB, UTModuleXposULR
    //                0       -+UTModuleYposUTB,         0
    // aX -> -UTModuleXposXLR , UTModuleYposXTB, UTModuleXposXLR
    //                0       -+UTModuleYposXTB,         0
    // bX -> -UTModuleXposXLR , UTModuleYposXTB, UTModuleXposXLR
    //                0       -+UTModuleYposXTB,         0
    std::map<string, double> positions;
    if ( m_build_uta )   {
      DetElement uta(parent,"A",parent.id());
      Volume     uta_vol("lvUTa", station_shape, description.air());
      PlacedVolume pv = utvol.placeVolume(uta_vol,Position(0,0,-0.5*station_dz));
      pv.addPhysVolID("station", 1);
      uta.setPlacement(pv);
      registerVolume(uta_vol.name(), uta_vol);
      if ( m_build_utaU )   { // UTaU
        double steps[2]   = { _toDouble("UTModuleStepUTaU1"), _toDouble("UTModuleStepUTaU0") };
        double offsets[2] = { 0.0, _toDouble("UTModuleXposUTaUOffset0") };
        
        DetElement layer(uta, "U", uta.id());
        positions["x_pos_regular"] = _toDouble("UTModuleXposULR");
        positions["x_pos_middle"]  = _toDouble("UTModuleXposUTB");
        positions["y_pos_middle"]  = _toDouble("UTModuleYposUTB");
        vol = build_layer(layer, "UTaULayer", -angle, 7, positions, offsets, steps);
        pv  = uta_vol.placeVolume(vol, Position(0,0,-0.5*layer_dz));
        pv.addPhysVolID("layer", 1);
        layer.setPlacement(pv);
        // Balconies
        pos = Position(_toDouble("UTBalconyYposU*UTSinAngle"), _toDouble("UTBalconyYposU"), 0);
        vol.placeVolume(volume("lvUTaUBalcony"), pos);
        pos = Position(_toDouble("-UTBalconyYposU*UTSinAngle"), _toDouble("-UTBalconyYposU"), 0);
        vol.placeVolume(volume("lvUTaUBalcony"), pos);
      }
      if ( m_build_utaX )   { // UTaX
        DetElement layer(uta, "X", uta.id());
        double steps[2]   = { _toDouble("UTModuleStepUTaX1"), _toDouble("UTModuleStepUTaX0") };
        double offsets[2] = { 0.0, _toDouble("UTModuleXposUTaXOffset0") };
        positions["x_pos_regular"] = _toDouble("UTModuleXposXLR");
        positions["x_pos_middle"]  = 0.0;
        positions["y_pos_middle"]  = _toDouble("UTModuleYposXTB");
        vol = build_layer(layer, "UTaXLayer",  0.0, 7, positions, offsets, steps);
        pv  = uta_vol.placeVolume(vol, Position(0,0, 0.5*layer_dz));
        pv.addPhysVolID("layer", 2);
        layer.setPlacement(pv);
        // Balconies
        vol.placeVolume(volume("lvUTaXBalcony"), Position(0, _toDouble("UTBalconyYposX"), 0));
        vol.placeVolume(volume("lvUTaXBalcony"), Position(0, _toDouble("-UTBalconyYposX"), 0));
      }
    }

    if ( m_build_utb )   {
      DetElement utb(parent,"B",parent.id());
      Volume     utb_vol("lvUTb", station_shape, description.air());
      PlacedVolume pv = utvol.placeVolume(utb_vol,Position(0,0, 0.5*station_dz));
      pv.addPhysVolID("station", 2);
      utb.setPlacement(pv);
      registerVolume(utb_vol.name(), utb_vol);
      
      if ( m_build_utbV )   { // UTbV
        DetElement layer(utb, "V", utb.id());
        double steps[2]   = { _toDouble("UTModuleStepUTbV1"), _toDouble("UTModuleStepUTbV0") };
        double offsets[2] = { 0.0, _toDouble("UTModuleXposUTbVOffset0") };
        positions["x_pos_regular"] = _toDouble("UTModuleXposULR");
        positions["x_pos_middle"]  = _toDouble("UTModuleXposUTB");
        positions["y_pos_middle"]  = _toDouble("UTModuleYposUTB");
        vol = build_layer(layer, "UTbVLayer",  angle, 8, positions, offsets, steps);
        // Balconies  ...... posU (??)
        pos = Position(_toDouble("UTBalconyYposU*UTSinAngle"),  _toDouble("UTBalconyYposU"), 0);
        vol.placeVolume(volume("lvUTaUBalcony"), pos);
        pos = Position(_toDouble("-UTBalconyYposU*UTSinAngle"), _toDouble("-UTBalconyYposU"), 0);
        vol.placeVolume(volume("lvUTaUBalcony"), pos);
        pv = utb_vol.placeVolume(vol, Position(0,0,-0.5*layer_dz));
        pv.addPhysVolID("layer", 1);
        layer.setPlacement(pv);
      }
      if ( m_build_utbX )   { // UTbX
        DetElement layer(utb, "X", utb.id());
        double steps[2]   = { _toDouble("UTModuleStepUTbX1"), _toDouble("UTModuleStepUTbX0") };
        double offsets[2] = { 0.0, _toDouble("UTModuleXposUTbXOffset0") };
        positions["x_pos_regular"] = _toDouble("UTModuleXposXLR");
        positions["x_pos_middle"]  = 0.0;
        positions["y_pos_middle"]  = _toDouble("UTModuleYposXTB");
        vol = build_layer(layer, "UTbXLayer",  0.0, 8, positions, offsets, steps);
        // Balconies
        vol.placeVolume(volume("lvUTbXBalcony"), Position(0, _toDouble( "UTBalconyYposX"), 0));
        vol.placeVolume(volume("lvUTbXBalcony"), Position(0, _toDouble("-UTBalconyYposX"), 0));
        pv = utb_vol.placeVolume(vol, Position(0,0, 0.5*layer_dz));
        pv.addPhysVolID("layer", 2);
        layer.setPlacement(pv);
      }
    }
  }

  void UTBuild::build_jacket()   {
    Material mat = description.material("UT:Kapton");
    Tube   cylSolid("UTCylJacketRadius","UTCylJacketRadius+UTCylJacketThickness","UTCylJacketLength/2",2.0*M_PI);
    Volume cylVol("lvUTCylJacket", cylSolid, mat);
    double con_len   = _toDouble("UTConeJacketLength")/2.0;
    double con_thick = _toDouble("UTConeJacketThickness");
    double con_rmin1 = _toDouble("UTConeJacketRadiusZmin");
    double con_rmin2 = _toDouble("UTConeJacketRadiusZmax");
    ConeSegment conSolid(con_len, con_rmin1, con_rmin1+con_thick, con_rmin2, con_rmin2+con_thick);
    Volume conVol("lvUTConeJacket", conSolid, mat);
    Assembly jacketVol("lvUTJacket");
    jacketVol.placeVolume(cylVol, Position(0,0,_toDouble("UTCylJacketZpos")));
    jacketVol.placeVolume(conVol, Position(0,0,_toDouble("UTConeJacketZpos")));
    registerVolume(jacketVol.name(), jacketVol);
  }
  
  void UTBuild::build_environ(DetElement parent, Volume parent_vol)   {
    //   See DDDB/UT/geometry.xml for the structure
    PlacedVolume pv;
    if ( m_build_frame ) {   // Build  lvUTFrame
      DetElement frame(parent,"frame",parent.id());
      pv = parent_vol.placeVolume(volume("lvUTFrame"));
      frame.setPlacement(pv);
    }
    if ( m_build_box )  {    // Build  lvUTBox
      Position pos;
      parent_vol.placeVolume(volume("lvUTBox"), pos);
    }
    if ( m_build_jacket )  { // Build  lvUTJacket
      Volume jacketVol = volume("lvUTJacket");
      DetElement jacket(parent,"jacket",parent.id());
      Position pos(0,0,_toDouble("UX851Rich1TTSplitZposIP-UTSystemZ"));
      pv = parent_vol.placeVolume(jacketVol, pos);
      jacket.setPlacement(pv);
    }
    { // Build  lvUX851InUT
      printout(WARNING,"UT","+++ The sub volume lvUX851InUT is NOT constructed.");
    }
  }
}

static Ref_t create_element(Detector& description, xml_h e, SensitiveDetector sens_det)  {
  xml_det_t     x_det = e;
  UTBuild       builder(description, x_det, sens_det);
  DetElement    ut_det = builder.detector;
  string        name    = ut_det.name();
  PlacedVolume  pv;
  Box      ut_box("UTFrameOuterWidth/2","UTFrameOuterHeight/2","UTBoxOuterThickness/2");
  Volume   ut_vol("lvUT", ut_box, description.air());
  
  sens_det.setType("tracker");
  builder.buildVolumes(e);
  builder.placeDaughters(ut_det, ut_vol, e);
  if ( builder.m_build_cxx )   {
    builder.build_groups();
    builder.build_hybrids();
    builder.build_jacket();
  }
  builder.build_modules();
  builder.build_layers(ut_det, ut_vol);
  builder.build_environ(ut_det, ut_vol);
  pv = builder.placeDetector(ut_vol);
  //pv.addPhysVolID("station", x_det.station());
  pv.addPhysVolID("system",  x_det.id());
  return ut_det;
}
DECLARE_DETELEMENT(LHCb_UT,create_element)
