//==========================================================================
//  AIDA Detector description implementation 
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see $DD4hepINSTALL/LICENSE.
// For the list of contributors see $DD4hepINSTALL/doc/CREDITS.
//
// Author     : M.Frank
//
//==========================================================================
//
// Specialized generic detector constructor
// 
//==========================================================================
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/Printout.h"
#include "DD4hep/ExtensionEntry.h"
#include "DD4hep/ConditionDerived.h"
#include "DDCond/ConditionsTags.h"
#include "DDCond/ConditionsContent.h"
#include "Detector/FT/DeFTConditionCalls.h"
#include "Conditions/ConditionIdentifier.h"
#include "Conditions/ConditionsRepository.h"

using namespace dd4hep;

static long create_conditions_recipes(Detector& description, xml_h e)  {
  using namespace std;
  using namespace gaudi;
  auto requests = shared_ptr<gaudi::ConditionsRepository>(new gaudi::ConditionsRepository());
  map<string, string> alignment_locations;
  map<string, string> condition_locations;
  string location;
  for(xml_coll_t i(e, _U(alignments)); i; ++i)  {
    xml_comp_t c = i;
    alignment_locations[c.attr<string>(_U(type))] = c.attr<string>(_U(ref));
  }
  for(xml_coll_t i(e, _UC(conditions)); i; ++i)  {
    xml_comp_t c = i;
    condition_locations[c.attr<string>(_U(type))] = c.attr<string>(_U(ref));
  }
  // All information we need to create the new conditions we will receive during the callbacks.
  // It is not necessary to cache information such as detector elements etc.
  DeConditionCall* ft_iovUpdate         = new DeFTConditionCall<DeFT>              (requests);
  DeConditionCall* ft_staticUpdate      = new DeFTConditionCall<DeFTStatic>        (requests);
  DeConditionCall* station_iovUpdate    = new DeFTConditionCall<DeFTStation>       (requests);
  DeConditionCall* station_staticUpdate = new DeFTConditionCall<DeFTStationStatic> (requests);
  DeConditionCall* layer_iovUpdate      = new DeFTConditionCall<DeFTLayer>         (requests);
  DeConditionCall* layer_staticUpdate   = new DeFTConditionCall<DeFTLayerStatic>   (requests);
  DeConditionCall* mod_iovUpdate        = new DeFTConditionCall<DeFTModule>        (requests);
  DeConditionCall* mod_staticUpdate     = new DeFTConditionCall<DeFTModuleStatic>  (requests);
  DeConditionCall* quad_iovUpdate       = new DeFTConditionCall<DeFTQuarter>       (requests);
  DeConditionCall* quad_staticUpdate    = new DeFTConditionCall<DeFTQuarterStatic> (requests);
  DeConditionCall* mat_iovUpdate        = new DeFTConditionCall<DeFTMat>           (requests);
  DeConditionCall* mat_staticUpdate     = new DeFTConditionCall<DeFTMatStatic>     (requests);
  
  // --------------------------------------------------------------------------
  // 1) Setup the addresses for the raw conditions (call: addLocation())
  // 2) Setup the callbacks and their dependencies:
  // 2.1) Static detector elements depend typically on nothing.
  //      These parameters normally were read during
  //      the detector construction in the XML define section.
  //      The callback ensures the creation of the objects
  // 2.2) IOV dependent detector elements depend on
  //      - the raw conditions data (aka the time dependent conditions) (see 1)
  //      - and the static detector element
  // 2.3) Typically parents have references to the next level children
  //      - Add reference to the child detector elements
  // --------------------------------------------------------------------------
  auto deFT     = description.detector("FT");
  auto ftAddr   = requests->addLocation(deFT, Keys::deltaKey, alignment_locations["system"],"FTSystem");
  auto ftStatic = requests->addDependency (deFT, Keys::staticKey, ft_staticUpdate->addRef());
  auto ftIOV    = requests->addDependency (deFT, Keys::deKey,     ft_iovUpdate->addRef());
  ftStatic.second->dependencies.push_back(ftAddr.first);
  ftIOV.second->dependencies.push_back(ftStatic.first);
  for (auto& station : deFT.children() )  {
    auto deStation = station.second;
    string st_nam  = deStation.name();
    auto stAddr    = requests->addLocation(deStation, Keys::deltaKey, alignment_locations["system"], st_nam);
    auto stStatic  = requests->addDependency (deStation, Keys::staticKey, station_staticUpdate->addRef());
    auto stIOV     = requests->addDependency (deStation, Keys::deKey,     station_iovUpdate->addRef());
    stStatic.second->dependencies.push_back(stAddr.first);
    stIOV.second->dependencies.push_back(stStatic.first);
    ftIOV.second->dependencies.push_back(stIOV.first);
    for (auto& layer : deStation.children() )  {
      auto deLay     = layer.second;
      string lay_nam = st_nam + deLay.name();
      auto layAddr   = requests->addLocation(deLay, Keys::deltaKey, alignment_locations["system"], lay_nam);
      auto layStatic = requests->addDependency (deLay, Keys::staticKey, layer_staticUpdate->addRef());
      auto layIOV    = requests->addDependency (deLay, Keys::deKey,     layer_iovUpdate->addRef());
      layStatic.second->dependencies.push_back(layAddr.first);
      layIOV.second->dependencies.push_back(layStatic.first);
      stIOV.second->dependencies.push_back(layIOV.first);
      for (auto& quad : deLay.children() )  {
        auto deQuad     = quad.second;
        string quad_nam = lay_nam + deQuad.name();
        auto quadAddr   = requests->addLocation(deQuad, Keys::deltaKey, alignment_locations["system"], quad_nam);
        auto quadStatic = requests->addDependency (deQuad, Keys::staticKey, quad_staticUpdate->addRef());
        auto quadIOV    = requests->addDependency (deQuad, Keys::deKey,     quad_iovUpdate->addRef());
        quadStatic.second->dependencies.push_back(quadAddr.first);
        quadIOV.second->dependencies.push_back(quadStatic.first);
        layIOV.second->dependencies.push_back(quadIOV.first);
        for (auto& module : deQuad.children() )  {          
          auto deMod     = module.second;
          string mod_nam = quad_nam + deMod.name();
          auto modAddr   = requests->addLocation(deMod, Keys::deltaKey, alignment_locations["modules"], mod_nam);
          auto modStatic = requests->addDependency (deMod, Keys::staticKey, mod_staticUpdate->addRef());
          auto modIOV    = requests->addDependency (deMod, Keys::deKey,     mod_iovUpdate->addRef());
          modStatic.second->dependencies.push_back(modAddr.first);
          modIOV.second->dependencies.push_back(modStatic.first);
          quadIOV.second->dependencies.push_back(modIOV.first);
          for (auto& mat : deMod.children() )  {
            auto deMat     = mat.second;
            string mat_nam = mod_nam + deMat.name();
            auto matAddr   = requests->addLocation(deMat, Keys::deltaKey, alignment_locations["mats"], mat_nam);
            auto matStatic = requests->addDependency (deMat, Keys::staticKey, mat_staticUpdate->addRef());
            auto matIOV    = requests->addDependency (deMat, Keys::deKey,     mat_iovUpdate->addRef());
            matStatic.second->dependencies.push_back(matAddr.first);
            matIOV.second->dependencies.push_back(matStatic.first);
            modIOV.second->dependencies.push_back(matIOV.first);
          }
        }
      }
    }
  }
  typedef shared_ptr<gaudi::ConditionsRepository> Ext_t;
  deFT.addExtension(new dd4hep::detail::DeleteExtension<Ext_t, Ext_t>(new Ext_t(requests)));
  ft_iovUpdate->release();
  ft_staticUpdate->release();
  station_iovUpdate->release();
  station_staticUpdate->release();
  layer_iovUpdate->release();
  layer_staticUpdate->release();
  mod_iovUpdate->release();
  mod_staticUpdate->release();
  quad_iovUpdate->release();
  quad_staticUpdate->release();
  mat_iovUpdate->release();
  mat_staticUpdate->release();
  return 1;
}
DECLARE_XML_DOC_READER(LHCb_FT_cond,create_conditions_recipes)
